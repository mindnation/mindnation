<?php

return [
    
    "allowed_metas" => [
        "psych" => [
            "full_name" => "Full Name",
            "contact_number" => "Contact Number",
            "short_background" => "Short Background",
            "timeslots" => "Timeslots",
            "doxyme_url" => "Doxy Me URL",
            "expertises" => "Expertises",
        ],

        "member" => [
            "birth_date" => "Birth Date",
            "gender" => "Gender",
            "contact_number" => "Contact Number",
            "settings_email_notifications" => "Settings Email Notifications",
            "emergency_contact_name" => "Name",
            "emergency_contact_relationship" => "Relationship",
            "emergency_contact_number" => "Contact Number",
            "issues" => "Issues",

        ],
    
    ],

    "gender_options" => [
        "male" => "Male",
        "female" => "Female",
        "nonbinary" => "Nonbinary",
        "prefer_not_to_say" => "Prefer not to say",
    ],


];