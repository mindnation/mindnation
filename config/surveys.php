<?php

return [
    
    "onboarding" => [
        "title" => "Onboarding Quiz",
        "categories" => [
            [
                "title" => "Tell us a bit about yourself.",
                "questions" => [
                    [
                        "title" => "How would you describe your romantic or sexual attraction to others?",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Straight/heterosexual",
                                "Gay/homosexual man",
                                "Lesbian/homosexual woman",
                                "Bisexual/attracted to both men and women",
                                "Questioning or I don’t know",
                                "Asexual/not attracted to others",
                                "Others",
                            ],
                    ],
                    [
                        "title" => "Are you current in a relationship?",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Single/never married",
                                "Seriously dating or committed relationship",
                                "Married",
                                "Civil union, domestic partnership or equivalent",
                                "Divorced/annulled/separated",
                                "Widowed",
                                "Other",
                            ],
                    ],
                    [
                        "title" => "Do you have kids?",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Yes",
                                "No",
                            ],
                    ],
                    [
                        "title" => "Have you ever been in counseling or therapy before?",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Yes",
                                "No",
                            ],
                    ],
                        
                ],
            ],
            [
                "title" => "Over the last 2 weeks, how often have you been bothered by the following problems?",
                "questions" => [
                    [
                        "title" => "Little interest or pleasure in doing things",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Not at all",
                                "Several days",
                                "More than half the days",
                                "Nearly every day",
                            ],
                    ],
                    [
                        "title" => "Feeling down, depressed or hopeless",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Not at all",
                                "Several days",
                                "More than half the days",
                                "Nearly every day",
                            ],
                    ],
                    [
                        "title" => "Feeling nervous, anxious or on edge",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Not at all",
                                "Several days",
                                "More than half the days",
                                "Nearly every day",
                            ],
                    ],
                    [
                        "title" => "Not being able to stop or control worrying",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Not at all",
                                "Several days",
                                "More than half the days",
                                "Nearly every day",
                            ],
                    ],
                    [
                        "title" => "Feeling exhausted or losing energy",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Not at all",
                                "Several days",
                                "More than half the days",
                                "Nearly every day",
                            ],
                    ],
                    [
                        "title" => "Feeling negative or cynical about your job",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Not at all",
                                "Several days",
                                "More than half the days",
                                "Nearly every day",
                            ],
                    ],
                    [
                        "title" => "How difficult have these problems made it for you to do work, take care of things at home, or get along with other people?",
                        "type" => "radio",
                        "answers" => 
                            [
                                "Not at all",
                                "Somewhat difficult",
                                "Very difficult",
                                "Extremely difficult",
                            ],
                    ],
                    [
                        "title" => "Are you experiencing any specific issues you’d like to focus on?",
                        "type" => "issues",
                        "answers" => 
                            [],
                    ],
                    
                ],
            ],
            [
                "title" => "Emergency Contact",
                "questions" => [
                    [
                        "title" => "Your safety matters to us. Please be assured that this will only be used in an actual emergency.",
                        "type" => "emergency_contact",
                        "answers" => 
                            [
                                
                            ],
                    ],
                ],
            ],
        ],
    ],

];