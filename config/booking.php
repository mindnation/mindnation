<?php

return [

    "working_hours" => [
        "start" => "00:00",
        "finish" => "23:59",
    ],
    "appointment" => [
        "max_ongoing" => 6,
        "duration" => 60, //minutes duration of a consultation
        "earliest_allowed" => 12, //hours from now
        "latest_allowed" => 6,  // months from now
        "reminder_notification" => 60, //minutes before the start time
        "waiting_room_link_visible" => 120, //minutes before the start time
        "status_change_notification" => 3, //minutes before sending (delay send)

        "methods" => [
            "live_chat" => "Live Chat",
            "online_audio_call" => "Online Audio Call",
            "online_video_call" => "Online Video Call",
        ],
        "status" => [
            "confirmed" => "Confirmed",
            "completed" => "Completed",
            "canceled" => "Canceled",
            "no-show" => "No show",
        ]
    ],


];
