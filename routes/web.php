<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes(['verify' => true, 'except' => 'register']);
//$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register')->name('register');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');


Route::get('/auth/check',function(){
    return json_encode((Auth::check()) ? true : false);
});

//Botman
// Route::match(['get', 'post'], '/botman', 'BotManController@handle');
// Route::get('/botman/tinker', 'BotManController@tinker');

// FRONT ROUTES
Route::get('/', 'FrontendController@home')->name('front.home');
Route::get('/about-us', 'FrontendController@aboutus')->name('front.about-us');
Route::get('/for-members', 'FrontendController@formembers')->name('front.for-members');
Route::get('/privacy-policy', 'FrontendController@privacypolicy')->name('front.privacy-policy');
Route::get('/terms-of-use', 'FrontendController@terms')->name('front.terms');
Route::get('/mentalresiliencetalk', 'FrontendController@videos')->name('front.videos');

Route::post('/contact', 'ContactEnquiryController@post')->name('contact-enquiry.post');


// ADMIN ROUTES
Route::middleware(['auth', 'role:admin,super_admin'])->group(function () {
    Route::redirect('/admin', '/admin/dashboard');
    Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
    //PSYCH
    Route::get('/admin/psych', 'Admin\PsychController@index')->name('admin.psych.index');
    Route::post('/admin/psych/store', 'Admin\PsychController@store')->name('admin.psych.store');
    Route::get('/admin/psych/datatable', 'Admin\PsychController@datatable')->name('admin.psych.datatable');
    Route::get('/admin/psych/get_data', 'Admin\PsychController@get_data')->name('admin.psych.get_data');
    Route::get('/admin/psych/assignedcompanies', 'Admin\PsychController@assignedcompanies')->name('admin.psych.assignedcompanies');
    Route::post('/admin/psych/delete', 'Admin\PsychController@delete')->name('admin.psych.delete');
    Route::post('/admin/psych/block', 'Admin\PsychController@block')->name('admin.psych.block');
    Route::post('/admin/psych/unblock', 'Admin\PsychController@unblock')->name('admin.psych.unblock');
    

    //Members
    Route::get('/admin/members/allowed', 'Admin\MemberController@allowed')->name('admin.members.allowed');
    Route::get('/admin/members/allowed/datatable', 'Admin\MemberController@datatable_allowed')->name('admin.members.allowed.datatable');

    Route::get('/admin/members/registered', 'Admin\MemberController@registered')->name('admin.members.registered');
    Route::get('/admin/members/registered/datatable', 'Admin\MemberController@datatable_registered')->name('admin.members.registered.datatable');

    Route::post('/admin/members/store', 'Admin\MemberController@store')->name('admin.members.store');
    
    Route::get('/admin/members/get_data', 'Admin\MemberController@get_data')->name('admin.members.get_data');
    Route::get('/admin/members/assignedcompanies', 'Admin\MemberController@assignedcompanies')->name('admin.members.assignedcompanies');
    Route::post('/admin/members/delete', 'Admin\MemberController@delete')->name('admin.members.delete');
    Route::post('/admin/members/block', 'Admin\MemberController@block')->name('admin.members.block');
    Route::post('/admin/members/unblock', 'Admin\MemberController@unblock')->name('admin.members.unblock');
    
    Route::post('/admin/members/import/parse', 'Admin\CsvImportController@parse')->name('admin.members.import.parse');
    Route::post('/admin/members/import/process', 'Admin\CsvImportController@process')->name('admin.members.import.process');

    //Human Resources
    Route::post('/admin/humanresources/store', 'Admin\HumanResourcesController@store')->name('admin.humanresources.store');
    Route::get('/admin/humanresources/registered', 'Admin\HumanResourcesController@registered')->name('admin.humanresources.registered');
    Route::get('/admin/humanresources/registered/datatable', 'Admin\HumanResourcesController@datatable_registered')->name('admin.humanresources.registered.datatable');
    Route::get('/admin/humanresources/get_data', 'Admin\HumanResourcesController@get_data')->name('admin.humanresources.get_data');
    Route::post('/admin/humanresources/delete', 'Admin\HumanResourcesController@delete')->name('admin.humanresources.delete');
    Route::post('/admin/humanresources/block', 'Admin\HumanResourcesController@block')->name('admin.humanresources.block');
    Route::post('/admin/humanresources/unblock', 'Admin\HumanResourcesController@unblock')->name('admin.humanresources.unblock');

    //COMPANY
    Route::get('/admin/companies', 'Admin\CompanyController@index')->name('admin.companies.index');
    Route::post('/admin/companies/store', 'Admin\CompanyController@store')->name('admin.companies.store');
    Route::post('/admin/companies/delete', 'Admin\CompanyController@delete')->name('admin.companies.delete');
    Route::post('/admin/companies/block', 'Admin\CompanyController@block')->name('admin.companies.block');
    Route::post('/admin/companies/unblock', 'Admin\CompanyController@unblock')->name('admin.companies.unblock');
    Route::get('/admin/companies/datatable', 'Admin\CompanyController@datatable')->name('admin.companies.datatable');
    Route::get('/admin/companies/get_data', 'Admin\CompanyController@get_data')->name('admin.companies.get_data');

    Route::get('/admin/companies/autocomplete', 'Admin\CompanyController@autocomplete')->name('admin.companies.autocomplete');

    //Appointments
    Route::get('/admin/consultations', 'Admin\AppointmentController@index')->name('admin.appointments.index');
    Route::get('/admin/consultations/datatable', 'Admin\AppointmentController@datatable')->name('admin.appointments.datatable');

    //Admin Users
    Route::get('/admin/admin-users', 'Admin\AdminUserController@index')->name('admin.admin-users.index');
    Route::post('/admin/admin-users/store', 'Admin\AdminUserController@store')->name('admin.admin-users.store');
    Route::get('/admin/admin-users/datatable', 'Admin\AdminUserController@datatable')->name('admin.admin-users.datatable');
    Route::get('/admin/admin-users/get_data', 'Admin\AdminUserController@get_data')->name('admin.admin-users.get_data');
    Route::post('/admin/admin-users/delete', 'Admin\AdminUserController@delete')->name('admin.admin-users.delete');
    Route::post('/admin/admin-users/block', 'Admin\AdminUserController@block')->name('admin.admin-users.block');
    Route::post('/admin/admin-users/unblock', 'Admin\AdminUserController@unblock')->name('admin.admin-users.unblock');

    //Account

    Route::get('/admin/account/details', 'Admin\AccountController@details')->name('admin.account.details');
    Route::post('/admin/account/details/store', 'Admin\AccountController@store_details')->name('admin.account.store_details');
    Route::get('/admin/account/password', 'Admin\AccountController@password')->name('admin.account.password');
    Route::post('/admin/account/password/store', 'Admin\AccountController@store_password')->name('admin.account.store_password');
    

});

// PSYCHOLOGIST ROUTES
Route::middleware(['auth', 'role:psychologist', 'psych.redirect_if_onboarding_is_done'])->group(function () {
    Route::get('/psych/onboarding', 'Psych\AccountController@onboarding')->name('psych.onboarding');
    Route::post('/psych/store_onboarding', 'Psych\AccountController@store_onboarding')->name('psych.store_onboarding');
});
Route::middleware(['auth', 'role:psychologist', 'psych.redirect_if_onboarding_not_done'])->group(function () {
    Route::redirect('/psych', '/psych/dashboard')->name('psych');;
    Route::get('/psych/dashboard', 'Psych\DashboardController@index')->name('psych.dashboard');
    Route::get('/psych/calendar', 'Psych\CalendarController@index')->name('psych.calendar');

    // Account
    Route::redirect('/psych/account', '/psych/account/profile');
    Route::get('/psych/account/profile', 'Psych\AccountController@profile')->name('psych.account.profile');
    Route::post('/psych/account/profile/store', 'Psych\AccountController@store_profile')->name('psych.account.store_profile');
    Route::get('/psych/account/password', 'Psych\AccountController@password')->name('psych.account.password');
    Route::post('/psych/account/password/store', 'Psych\AccountController@store_password')->name('psych.account.store_password');

    //Working Hours
    Route::get('/psych/working-hours', 'Psych\WorkingHourController@index')->name('psych.working-hours');
    Route::post('/psych/working-hours/store', 'Psych\WorkingHourController@store')->name('psych.working-hours.store');
    Route::get('/psych/working-hours/get_data', 'Psych\WorkingHourController@get_data')->name('psych.working-hours.get_data');
    Route::get('/psych/working-hours/get_input_template', 'Psych\WorkingHourController@get_input_template')->name('psych.working-hours.get_input_template');
    //Appointments
    Route::get('/psych/consultations', 'Psych\AppointmentController@index')->name('psych.appointments');
    Route::get('/psych/consultations/datatable', 'Psych\AppointmentController@datatable')->name('psych.appointments.datatable');
    Route::get('/psych/consultations/get_data', 'Psych\AppointmentController@get_data')->name('psych.appointments.get_data');
    Route::post('/psych/consultations/edit', 'Psych\AppointmentController@edit')->name('psych.appointments.edit');
    //Member
    Route::get('/psych/patient/get_data', 'Psych\MemberController@get_data')->name('psych.patients.get_data');

});

// MEMBER ROUTES

Route::middleware(['auth',  'role:member', 'verified', 'member.redirect_if_agreement_is_signed'])->group(function () {
    Route::get('/member/agreement', 'Member\AccountController@agreement')->name('member.agreement');
    Route::post('/member/agreement', 'Member\AccountController@postagreement')->name('member.agreement');
});

Route::middleware(['auth',  'role:member', 'verified','member.redirect_if_onboarding_is_done', 'member.redirect_if_agreement_not_signed'])->group(function () {
    Route::get('/member/onboarding', 'Member\AccountController@onboarding')->name('member.onboarding');

    Route::get('/member/onboarding/survey', 'Member\SurveyController@onboarding')->name('member.onboarding.survey');
    Route::post('/member/onboarding/store', 'Member\SurveyController@store')->name('member.onboarding.survey.store');
});
Route::middleware(['auth',  'role:member', 'verified','member.redirect_if_onboarding_not_done', 'member.redirect_if_agreement_not_signed'])->group(function () {
    Route::redirect('/member', '/member/dashboard')->name('member');
    Route::get('/member/dashboard', 'Member\DashboardController@index')->name('member.dashboard');
    Route::get('/member/account', 'Member\AccountController@index')->name('member.account.index');
    
    //Booking
    Route::get('/member/booking', 'Member\BookingController@select_pro')->name('member.booking');
    Route::post('/member/booking', 'Member\BookingController@post_select_pro')->name('member.booking.select_pro');
    Route::get('/member/booking/choose-slot', 'Member\BookingController@select_slot')->name('member.booking.select_slot');
    Route::post('/member/booking/choose-slot', 'Member\BookingController@post_select_slot')->name('member.booking.select_slot');
    Route::get('/member/booking/get_slots', 'Member\BookingController@get_slots')->name('member.booking.get_slots');
    Route::get('/member/booking/choose-method', 'Member\BookingController@select_method')->name('member.booking.select_method');
    Route::post('/member/booking/choose-method', 'Member\BookingController@post_select_method')->name('member.booking.select_method');
    Route::get('/member/booking/comment', 'Member\BookingController@comment')->name('member.booking.comment');
    Route::post('/member/booking/comment', 'Member\BookingController@post_comment')->name('member.booking.comment');

    

    // Account
    Route::redirect('/member/account', '/member/account/details')->name('member.account');
    Route::get('/member/account/details', 'Member\AccountController@details')->name('member.account.details');
    Route::post('/member/account/details/store', 'Member\AccountController@store_details')->name('member.account.store_details');
    Route::get('/member/account/password', 'Member\AccountController@password')->name('member.account.password');
    Route::post('/member/account/password/store', 'Member\AccountController@store_password')->name('member.account.store_password');
    Route::get('/member/account/settings', 'Member\AccountController@settings')->name('member.account.settings');
    Route::post('/member/account/settings/store', 'Member\AccountController@store_settings')->name('member.account.store_settings');

    //Appointment
    Route::get('/member/account/consultations', 'Member\AppointmentController@index')->name('member.account.appointments');
    Route::post('/member/account/consultations/cancel', 'Member\AppointmentController@cancel')->name('member.account.appointments.cancel');

    //Appointment
    Route::get('/member/account/assessments', 'Member\SurveyController@list')->name('member.account.surveys');

    //Temp location for HR
    Route::get('/hr/dashboard', 'HR\DashboardController@index')->name('hr.dashboard');
});

Route::middleware(['auth', 'role:human_resources'])->group(function () {
    Route::redirect('/hr', '/hr/dashboard')->name('hr');
    Route::get('/hr/dashboard', 'HR\DashboardController@index')->name('hr.dashboard');
});



