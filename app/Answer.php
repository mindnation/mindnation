<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc')->get();
    }

    public function user_answers()
	{
		return $this->hasMany(UserAnswer::class, 'answer_id');
    }
}
