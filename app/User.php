<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;



use Illuminate\Support\Facades\Password;
//use Mail;
use Str;
//use Illuminate\Support\Facades\Mail;
//use App\Mail\StaffWelcomeMail;
use App\Notifications\VerifyEmailNotification;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\StaffWelcomeNotification;



class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'email', 
        'password', 
        'registration_date',
        'last_login',
        'last_login_ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function __construct()
    {
        parent::boot();
        static::deleting(function($user){
            $relationMethods = ['user_metas','users_added','companies_added','member_appointments','working_hours','psych_appointments','user_surveys','psych_assigned_companies'];
            foreach($relationMethods as $relationMethod){
                if($user->$relationMethod()->count() > 0){
                    throw new \Exception('Exisiting Relationship');
                    return false;
                }
            }
            $user->roles()->detach();
        });
    }


    public function routeNotificationForMail($notification)
    {
        // Return email address only...
        return $this->email;

        // Return name and email address...
       // return [$this->email_address => $this->name];
    }
    
    protected static function boot(){

    }

    public function user_metas()
    {
        return $this->hasMany(UserMeta::class, 'user_id');
    }

    public function getMeta($key)
    {
        $meta = $this->user_metas()->where('key', $key)->first();
        if ($meta) {
            return $meta;
        } else {
            return null;
        }

    }

    public function getMetaValue($key)
    {
        $meta = $this->user_metas()->where('key', $key)->first();
        if ($meta) {
            return $meta->value;
        } else {
            return null;
        }

    }

    public function setMeta($key, $value, $checkAllowed = null)
    {
        $meta = $this->getMeta($key);
        if (!$meta) {
            $meta = new UserMeta();
            $meta->key = $key;
            $meta->user()->associate($this->id);
        }
        $meta->value = $value;
        switch ($checkAllowed) {
            case 'member':
                if ($meta->isMemberAllowed()) {
                    return $meta->save();
                }
                break;
            case 'psych':
                if ($meta->isPsychAllowed()) {
                    return $meta->save();
                }
                break;
            default:
                return $meta->save();
        }

    }

    public function avatar()
    {
        return $this->belongsTo(Media::class, 'avatar_id', 'id');
    }

    // ADMIN

    public function added_by()
    {
        return $this->belongsTo(User::class, 'added_by_id');
    }

    // Get users added by this user
    public function users_added()
    {
        return $this->hasMany(User::class, 'added_by_id');
    }

    public function companies_added()
    {
        return $this->hasMany(Company::class, 'added_by_id');
    }

    public function setAddedBy()
    {
        if (Auth::user() != null) {$user_id = Auth::user()->id;} else { $user_id = 1;}
        $this->added_by()->associate($user_id);
    }

    // ROLES

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    public function isAdminOnly()
    {
        return $this->hasRole('admin') && !$this->hasRole('super_admin');
    }
    public function isAdmin()
    {
        return $this->hasAnyRole(['admin', 'super_admin']);
    }
    public function isPsych()
    {
        return $this->hasAnyRole(['psychologist']);
    }
    public function isMember()
    {
        return $this->hasAnyRole(['member']);
    }
    public function isHR()
    {
        return $this->hasAnyRole(['human_resources']);
    }

    public function getDashboardRoute()
    {

        if ($this->isAdmin()) {
            return route('admin.dashboard');
        } else if ($this->isPsych()) {
            return route('psych.dashboard');
        } else if ($this->isMember()) {
            return route('member.dashboard');
        } else if ($this->isHR()) {
            return route('hr.dashboard');
        } else {
            return route('home');
        }

    }

    public function makeMember()
    {
        $role = Role::where('name', 'member')->first();
        $this->roles()->attach($role);
    }
    public function makePsych()
    {
        $role = Role::where('name', 'psychologist')->first();
        $this->roles()->attach($role);
    }
    public function makeAdmin()
    {
        $role = Role::where('name', 'admin')->first();
        $this->roles()->attach($role);
    }
    public function makeHumanResources()
    {
        $role = Role::where('name', 'human_resources')->first();
        $this->roles()->attach($role);
    }

    // Members
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function member_appointments()
    {
        return $this->hasMany(Appointment::class, 'member_id');
    }

    public function isPatientOf($psych)
    {
        return $this->member_appointments()->where('psych_id', $psych->id)->get();
    }

    public function hasEmailNotifications(){
        if($this->getMetaValue('settings_email_notifications') === '0'){
            return false;
        }
        return true;
    }

    //Psych

    public static function findPsych($id)
    {
        return User::where('id', $id)
            ->whereHas(
                'roles', function ($query) {
                    $query->where('name', 'psychologist');
                }
            )->first();
    }

    public function working_hours()
    {
        return $this->hasMany(WorkingHour::class, 'psych_id');
    }

    public function psych_assigned_companies()
    {
        return $this->belongsToMany(Company::class, 'company_psych');
    }

    public function isAssignedToCompany($company_id)
    {
        return $this->psych_assigned_companies()
            ->where('company_id', $company_id)
            ->get();
    }

    public function psych_list_assigned_companies()
    {
        $html = "";
        $companies = $this->psych_assigned_companies()->get();
        $first = true;
        foreach ($companies as $company) {
            $html .= ((!$first) ? ', ' : '') . $company->name;
            $first = false;
        }
        return $html;
    }
    public function psych_appointments()
    {
        return $this->hasMany(Appointment::class, 'psych_id');
    }

    //SURVEYS
    public function user_surveys()
    {
        return $this->hasMany(UserSurvey::class, 'user_id');
    }

    //Functions
    public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
    public function short_name()
    {
        return $this->first_name . ' ' . substr($this->last_name, 0, 1);
    }

    public function full_name()
    {
        if ($full_name = $this->getMetaValue('full_name')) {
            return $full_name;
        } else {
            return $this->name();
        }

    }

    public function isRegistered()
    {
        if ($this->registration_date) {
            return true;
        }
        return false;
    }

    public function getRegistrationDate()
    {
        if ($this->registration_date) {
            return Carbon::parse($this->registration_date)->format('d/m/Y');
        }
        return null;
    }

    public static function getGenderOptions()
    {
        return config('user_metas.gender_options');
    }

    public function getGender()
    {
        return isset($this->getGenderOptions()[$this->getMetaValue('gender')]) ? $this->getGenderOptions()[$this->getMetaValue('gender')] : null;
    }


    public static function generatePassword()
    {
      // Generate random string and encrypt it. 
      return bcrypt(Str::random(35));
    }

    public function sendStaffWelcomeEmail()
    {
    
      // Generate a new reset password token
      $token = Password::getRepository()->create($this);
      $this->notify(new StaffWelcomeNotification($token));
      
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }


    // Checking is the user or the company is blocked
    public function isActive(){
        if($this->isMember() && $this->company && $this->company->blocked_at)
        {
            return false;
        }

        if($this->blocked_at) return false;

        return true;
    }







}
