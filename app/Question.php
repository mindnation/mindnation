<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    public function question_category()
    {
        return $this->belongsTo(QuestionCategory::class, 'question_category_id');
    }

    public function survey()
    {
        if ($this->question_category) {
            return $this->question_category->survey;
        }
        return null;
    }
    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc')->get();
    }

    public function user_answers()
    {
        return $this->hasMany(UserAnswer::class, 'question_id');
    }

    public function answer_from_user($user_id)
    {
        return $this->user_answers()->where('user_id', $user_id)->latest()->first();
    }

    public function previous()
    {
        $previous = null;

        //we look for the previous question in the current category
        foreach ($this->question_category->questions()->ordered() as $question) {
            if (!empty($previous && $question->id == $this->id)) {
                return $previous;
            }
            $previous = $question;
        }
        // if we dont find it we check the last question of the previous cat  
        $previous_cat = $this->question_category->previous();
        if($previous_cat){
            return $previous_cat->questions()->orderBy('order', 'desc')->first();
        }
        
        return null;
    }


    public function next()
    {
        $current = null;
        foreach ($this->question_category->questions()->ordered() as $question)
        {
            if(!empty($current)){
                return $question;
            }
            if($this->id == $question->id){
                $current = $question;
            }
        }

        $next_cat = $this->question_category->next();
        if($next_cat){
            return $next_cat->questions()->ordered()->first();
        }

        return null;
    }

}
