<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\MemberBirthdayNotification;

//use Carbon\Carbon;
use App\User;

class SendBirthdayNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday_notifications:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send birthday greetings to members';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Not clean but it works. we should store the birthdate as date format
        $birthday_boys = User::whereHas(
            'user_metas', function ($query) {
                $query->where('key', 'birth_date')
                    ->where('value','like', now()->format('d/m').'%');
            }
        )->get();
        
        foreach($birthday_boys as $user){
            $user->notify(new MemberBirthdayNotification());
        }
    }
}
