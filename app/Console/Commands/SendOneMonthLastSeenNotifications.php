<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\MemberOneMonthSinceLastSeen;
use App\User;
use App\Appointment;
use DB;

class SendOneMonthLastSeenNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'longtimenosee_notifications:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for latest appointment = 1 month thenn send notif';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $latestAppointments = Appointment::
                   select('member_id', DB::raw('MAX(date) as last_appointment_date'))
                   ->where('status', 'completed')
                   ->groupBy('member_id');

        $longTimeNoSees = User::joinSub($latestAppointments, 'member_appointments', function ($join) {
                    $join->on('users.id', '=', 'member_appointments.member_id')
                    ->where('last_appointment_date', '=', now()->subMonth()->toDateString());
                })->get();

        
                // dd($longTimeNoSees);
        
        foreach($longTimeNoSees as $user){
            $user->notify(new MemberOneMonthSinceLastSeen());
        }
    }
}
