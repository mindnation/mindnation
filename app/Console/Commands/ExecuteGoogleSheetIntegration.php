<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AuthorizedGoogleSheets;
use App\User;
use App\Role;
use App\Company;
use DB;
use Sheets;


class ExecuteGoogleSheetIntegration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'googlesheetintegration:start {task}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //CEI   = Company Employee Integration
        //CI    = Company Integration

        $task = $this->argument('task');
        if($task == "CEI"){
            $this->executeCompanyEmployeeIntegration();   
            exit;
        }
        if($task == "CI"){
            $this->executeCompanyIntegration();
            exit;
        }
        echo "Invalid task";
    }

    public function executeCompanyIntegration(){

        $bulk_sheets = AuthorizedGoogleSheets::all()
                        ->where('single_execution', true)
                        ->where('integration_type', 'CI');

        if(count($bulk_sheets)==0) {
            echo "No data to be executed";
            exit;
        }

        foreach($bulk_sheets as $s){
            $sheets = Sheets::spreadsheet($s->sheet_id)
                ->range('A2:I')
                ->all();

            if(count($sheets)==0) break;

            foreach($sheets as $row){

                $email_domains = explode(",", $row[8]);
                if(count($email_domains)>0){
                    foreach($email_domains as $email_domain){
                        $company = Company::all()
                            ->where('name', $row[0])
                            ->where('email_domain', $email_domain)
                            ->first();

                        if($company == null || $company->id == null){
                            $company = new Company();
                            $company->name = $row[0];
                        }
                        $company->description = $row[1];
                        $company->email_domain = trim($email_domain);

                        echo "Inserting row with company name : [" . $company->name . "] with email domain of : [" . $email_domain . "]";
                        $company->save();
                    }
                }

            }

            $s->is_executed = true;
            $s->save();

        }
    }

    public function executeCompanyEmployeeIntegration(){

        $bulk_sheets = AuthorizedGoogleSheets::all()
                        ->where('single_execution', true)
                        ->where('integration_type', 'CEI');

        if(count($bulk_sheets)==0) {
            echo "No data to be executed";
            exit;
        }
        
        $uploaded_records = 0;
        foreach($bulk_sheets as $s){
            $sheets = Sheets::spreadsheet($s->sheet_id)
                ->range('A2:F')
                ->all();
            if(count($sheets)==0) break;

            $old_company = "";
            foreach($sheets as $sheet){
                $user = User::all()
                        ->where('email', $sheet[4])
                        ->first();
                if($user != null && $user->id != null) continue;

                $company = Company::all()
                        ->where('name', $sheet[3])
                        ->first();

                if($company == null || $company->id == null){
                    if($old_company != $sheet[3]){
                        echo "Company " . $sheet[3] . " was not yet registered on our system";
                    }
                    $old_company = $sheet[3];
                    continue;
                }

                $role = Role::where('name','member')->first();
                $user = new User();
                $user->first_name = $sheet[0];
                $user->last_name = $sheet[1];
                $user->email = $sheet[4];
                $user->password = bcrypt('$Ovx@!$D4#t7');
                $user->role = $sheet[2];
                $user->country = $sheet[5];
                $user->company_id = $company->id;
                $user->setAddedBy();
                $user->save();
                $user->roles()->attach($role);
                $uploaded_records++;
            }
            $s->is_executed = true;
            $s->save();
        }
        echo "# of successful uploads : " . $uploaded_records;
    }
}
