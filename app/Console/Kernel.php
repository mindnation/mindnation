<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Console\Commands\SendBirthdayNotifications;
use App\Console\Commands\SendOneMonthLastSeenNotifications;
use App\Console\Commands\ExecuteGoogleSheetIntegration;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SendBirthdayNotifications::class,
        SendOneMonthLastSeenNotifications::class,
        ExecuteGoogleSheetIntegration::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('queue:work --tries=3')->cron('* * * * *')->withoutOverlapping();
        //$schedule->command('queue:work --daemon')->everyMinute()->withoutOverlapping();

        $schedule->command('birthday_notifications:send')->dailyAt('07:00')->withoutOverlapping()->emailOutputTo('jordan.mouyal@gmail.com');
        $schedule->command('longtimenosee_notifications:send')->dailyAt('07:00')->withoutOverlapping()->emailOutputTo('jordan.mouyal@gmail.com');
         
        //          ->hourly();->dailyAt('08:00');
        
       // ->environments(['staging', 'production']);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
