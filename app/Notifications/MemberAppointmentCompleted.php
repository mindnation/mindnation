<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;
use App\Mail\MemberAppointmentCompletedMail;
use App\Appointment;
use Carbon\Carbon;

class MemberAppointmentCompleted extends Notification implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $appointment;

    public $firstCompletedAppointment;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment, $firstCompletedAppointment = false)
    {
        $this->appointment = $appointment;
        $this->firstCompletedAppointment = $firstCompletedAppointment;

        $now = Carbon::now();
        $minutes_before_sending = config('booking.appointment.status_change_notification', 1);

        $this->delay($now->addMinutes($minutes_before_sending));
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->dontSend($notifiable)) {
            return [];
        } 
        return ['mail'];
    }

    public function dontSend($notifiable)
    {
        return ($this->appointment->status !== 'completed') || !($notifiable->hasEmailNotifications());

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MemberAppointmentCompletedMail($this->appointment, $this->firstCompletedAppointment))->to($notifiable->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
        ];
    }
}
