<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;
use App\Mail\PsychAppointmentReminderMail;
use App\Appointment;

class PsychAppointmentReminder extends Notification implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $appointment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;

        $minutes_before_start = config('booking.appointment.reminder_notification', 60);
        $carbon_start = $appointment->getCarbonStart()->subMinutes($minutes_before_start);

        $this->delay($carbon_start);
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->dontSend($notifiable)) {
            return [];
        } 
        return ['mail'];
    }

    public function dontSend($notifiable)
    {
        return ($this->appointment->status === 'canceled') || !($notifiable->hasEmailNotifications());

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new PsychAppointmentReminderMail($this->appointment))->to($notifiable->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
