<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;
use App\Mail\MemberAppointmentCanceledMail;
use App\Appointment;
use Carbon\Carbon;

class MemberAppointmentCanceled extends Notification implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $appointment;
    public $isUserCanceling;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment, $isUserCanceling = false)
    {
        $this->appointment = $appointment;
        $this->isUserCanceling = $isUserCanceling;

        $now = Carbon::now();
        $minutes_before_sending = config('booking.appointment.status_change_notification', 1);

        $this->delay($now->addMinutes($minutes_before_sending));
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->dontSend($notifiable)) {
            return [];
        } 
        return ['mail'];
    }

    public function dontSend($notifiable)
    {
        return ($this->appointment->status !== 'canceled') || !($notifiable->hasEmailNotifications());

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MemberAppointmentCanceledMail($this->appointment, $this->isUserCanceling))->to($notifiable->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
