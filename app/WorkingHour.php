<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkingHour extends Model
{
    //use SoftDeletes;

    protected $fillable = ['date', 'start_time', 'finish_time', 'employee_id'];

    public function psych()
    {
        return $this->belongsTo(User::class, 'psych_id');
    }


    static function list_select_options(){
        $options = array();

        $duration = config('booking.appointment.duration', 60);
        $start_hour = self::getCarbonConfigMinTime();
        $finish_hour = self::getCarbonConfigMaxTime();
        $current = $start_hour;

        while(($current->copy()->subMinute())->lessThanOrEqualTo($finish_hour)){ // to handle 2359pm 
            if($current->copy()->subMinute()->equalTo($finish_hour)) $current->subMinute(); // to handle 2359pm 
            
            $options[$current->toTimeString()] = $current->format('g:i A'); //$current->format('H:i');
            $current->addMinutes($duration);
        }
        return $options;
    }

    static function getCarbonConfigMinTime(){
        return Carbon::parse(config('booking.working_hours.start', '00:00'));
    }
    static function getCarbonConfigMaxTime(){
        return Carbon::parse(config('booking.working_hours.finish', '23:59'));
    }

    public function getCarbonStart(){
        $date = Carbon::parse($this->date);
        return $date->setTimeFromTimeString($this->start_time);
    }

    public function getCarbonFinish(){
        $date = Carbon::parse($this->date);
        return $date->setTimeFromTimeString($this->finish_time);
    }

    public function isValid(){
        $allowed_value = $this->list_select_options();

        if(!isset($allowed_value[$this->start_time]) || !isset($allowed_value[$this->finish_time])){
            return false;
        }

        $start = $this->getCarbonStart();
        $finish = $this->getCarbonFinish();

        if($start->greaterThanOrEqualTo($finish)){
            return false;
        }

        return true;
    }

    public function isOverlapping($working_hour){
        // For touching periods we can do that
        // return !start1.after(end2) && !start2.after(end1);

        //Strictly overlapping: period1_start < period2_end and period2_start < period1_end
        
        $s1 = $this->getCarbonStart();
        $f1 = $this->getCarbonFinish();

        $s2 = $working_hour->getCarbonStart();
        $f2 = $working_hour->getCarbonFinish();

        return $s1->lessThan($f2) && $s2->lessThan($f1);
    }


    static function get_available_slots($date, $appointment = null){
        $date = Carbon::parse($date)->toDateString();
        $query = WorkingHour::where('date', $date);
                        //->orderBy('start_time', 'asc') // lets try rand order. 
         
        if(!$appointment){
            return array();
        }
        $company = $appointment->member->company;
        $available_psychs = $company->getAvailablePsychs()->pluck('id');
        
        $query->whereIn('psych_id', $available_psychs);
        if($appointment->psych) $query->where('psych_id', $appointment->psych->id);
        
        $working_hours = $query->inRandomOrder()->get();

       
                    
        $duration = config('booking.appointment.duration', 60);
        $earliest_allowed_from_now = config('booking.appointment.earliest_allowed', 12);

        $earliest_allowed_appointment = Carbon::now()->addHours($earliest_allowed_from_now);

        $available_slots = array();

        foreach($working_hours as $w){
            $start = $w->getCarbonStart();
            $finish = $w->getCarbonFinish();
            
            $current = $start;
            
            while($current->lessThanOrEqualTo($finish)){

                $slot_start_time = $current->copy();
                $slot_finish_time = $current->addMinutes($duration);


                // Check earliest allowed : minimum 12h after now
                if($slot_start_time->greaterThanOrEqualTo($earliest_allowed_appointment)){

                    

                    if($slot_finish_time->copy()->subMinute()->equalTo($finish)) $slot_finish_time->subMinute(); // to handle 2359pm 

                    // Check if this slot doesnt already exist and is valid 
                    if(!isset($available_slots[$slot_start_time->toTimeString()]) && $slot_finish_time->lessThanOrEqualTo($finish)){  
                        $apt = new Appointment();  // We could create a Slot Model for that
                        $apt->date = $date;
                        $apt->start_time = $slot_start_time->toTimeString();
                        $apt->finish_time = $slot_finish_time->toTimeString();
                        $apt->psych()->associate($w->psych->id);
                        
                        

                        //check existing appointment (todo create function, done twice)
                        $slot_taken = Appointment::where('psych_id', $apt->psych->id) 
                                    ->where('date', $apt->date)
                                    ->where('start_time', $apt->start_time)
                                    ->where('finish_time', $apt->finish_time)
                                    ->where('status', 'confirmed')
                                    ->first();

                        if(!$slot_taken){
                            $available_slots[$slot_start_time->toTimeString()] = $apt;
                        }    
                    }
                }
                
            }
        }
        $collection = collect($available_slots);
        $sorted_collection = $collection->sortBy(function ($product, $key) {
            return $key;
        });
        return $sorted_collection;

    }


    static function getMinDate(){
        return Carbon::now()->addHours(config('booking.appointment.earliest_allowed', 12));
    }
    static function getMaxDate(){
        return  Carbon::now()->addMonths(config('booking.appointment.latest_allowed', 6));
    }


}
