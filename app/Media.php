<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use SoftDeletes;

    public function users(){
        return $this->hasMany(User::class, 'avatar_id', 'id');
    }
}
