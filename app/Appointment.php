<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Appointment extends Model
{
    

    public function psych()
    {
        return $this->belongsTo(User::class, 'psych_id');
    }


    public function member()
    {
        return $this->belongsTo(User::class, 'member_id');
    }

    public function getMethod(){
        if(isset(config('booking.appointment.methods')[$this->method])){
            return config('booking.appointment.methods')[$this->method];
        }
        return null;
    }


    public function getStatus(){
        if(isset(config('booking.appointment.status')[$this->status])){
            return config('booking.appointment.status')[$this->status];
        }
        return null;
    }

    public function setStatus($status){
        if(isset($this->getStatusOptions()[$status])){
            $this->status = $status;
            return $this->save();
        }
        return false;
    }

    public function setMethod($method, $save = true){
        if(isset($this->getMethodOptions()[$method])){
            $this->method = $method;
            if($save) $this->save();
            return true;
        }
        return false;
    }

    public function getStatusOptions(){

        //TODO: Check status available for this apointment specifically
        return config('booking.appointment.status');
    }

    static function getMethodOptions(){

        return config('booking.appointment.methods');
    }

    public function setStatusConfirmed(){
        if(isset(config('booking.appointment.status')['confirmed'])){
            $this->status = 'confirmed';
        }
    }

    public function isConfirmed(){
        return ($this->status == 'confirmed');
    }
    public function isCanceled(){
        return ($this->status == 'canceled');
    }
    public function isCompleted(){
        return ($this->status == 'completed');
    }

    public function hasNotStarted(){
        $start_time = $this->getCarbonStart();
        $now = Carbon::now();
        return $now->lessThan($start_time);
    }

    public function hasNotFinished(){
        $finish_time = $this->getCarbonFinish();
        $now = Carbon::now();
        return $now->lessThanOrEqualTo($finish_time);
    }

    //Show the link 2 hours before until the end of the appointment
    public function linkIsVisible(){
        $start_time = $this->getCarbonStart();
        $finish_time = $this->getCarbonFinish();
        $now = Carbon::now();
        $minutes_before_start = config('booking.appointment.waiting_room_link_visible', 60);
        return $now->greaterThanOrEqualTo($start_time->subMinutes($minutes_before_start)) && $now->lessThanOrEqualTo($finish_time);
    }


    public function setTime($start_time){
        $duration = config('booking.appointment.duration', 60);
        $start_time = Carbon::parse($start_time);
        $this->start_time = $start_time->toTimeString();

        //TODO fix bug for 2359pm
        $this->finish_time = $start_time->addMinutes(60)->toTimeString();
    }

    public function setDate($date){
        $date = Carbon::parse($date);
        $this->date = $date->toDateString();
    }

    public function getCarbonStart(){
        $date = Carbon::parse($this->date);
        return $date->setTimeFromTimeString($this->start_time);
    }

    public function getCarbonFinish(){
        $date = Carbon::parse($this->date);
        return $date->setTimeFromTimeString($this->finish_time);
    }

    public function getFullDateString(){
        return $this->getCarbonStart()->format('l, d/m/y h:i A');  
    }
    public function getDateTimeString(){
        return $this->getCarbonStart()->format('d/m/y h:i A');  
    }


    public function getWaitingRoomlUrl(){
        if($psych = $this->psych){
            return $psych->getMetaValue('doxyme_url');
        }
        return null;
    }

    public function isValid(){
        $slot_taken = Appointment::where('psych_id', $this->psych->id) 
                    ->where('date', $this->date)
                    ->where('start_time', $this->start_time)
                    ->where('finish_time', $this->finish_time)
                    ->where('status', 'confirmed')
                    ->first();

        if($slot_taken) return false;

        

        $psych = User::find($this->psych->id);
        if(!$psych->isPsych())  return false; // not really necessary, already in controller


        $psych_is_working = $psych->working_hours()
                                ->where('date', $this->date)
                                ->whereTime('start_time', '<=', $this->start_time)
                                ->whereTime('finish_time', '>=', $this->finish_time)
                                ->first();

        if(!$psych_is_working) return false;
        if(!$this->member) return false;

        return true;
    }

    public function psychIs($psych){
        return ($this->psych_id == $psych->id)? true : false;
    }

    public function MemberIs($member){
        return ($this->member_id == $member->id)? true : false;
    }

    // check if appointment is contained in a working hour
    public function isContainedIn($working_hour){
        
        
        $s1 = $this->getCarbonStart();
        $f1 = $this->getCarbonFinish();

        $s2 = $working_hour->getCarbonStart();
        $f2 = $working_hour->getCarbonFinish();

        return $s1->greaterThanOrEqualTo($s2) && $f1->lessThanOrEqualTo($f2);
    }
}
