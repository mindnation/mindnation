<?php

namespace App\Rules;

use App\User;
use App\Company;

use Illuminate\Contracts\Validation\Rule;

class IsAllowedToRegister implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where('email',$value)->first();

        if($user && $user->isActive()){
            return true;
        }
        
        $domain = substr($value, strpos($value, "@") + 1);
        $company = Company::where('email_domain', $domain)->first();
        if($company && $company->isActive()){
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This email is not authorised to register';
    }
}
