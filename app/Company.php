<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name', 'description',
    ];

    public function employees()
    {
        return $this->hasMany(User::class, 'company_id', 'id');
    }

    public function assigned_psychs()
    {
        return $this->belongsToMany(User::class, 'company_psych');
    }

    public function getAvailablePsychs()
    {
        return $this->assigned_psychs()
            ->whereNotNull('onboarding')
            ->get();
    }

    public function added_by()
    {
        return $this->belongsTo(User::class, 'added_by_id');
    }

    public function setAddedBy()
    {
        if (Auth::user() != null) {$user_id = Auth::user()->id;} else { $user_id = 1;}
        $this->added_by()->associate($user_id);
    }

    public function isActive(){
        if($this->blocked_at)
        {
            return false;
        }

        return true;
    }
}
