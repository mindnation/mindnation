<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use SoftDeletes;


    public function question_categories()
    {
        return $this->hasMany(QuestionCategory::class, 'survey_id');
    }

    public function questions()
    {
        return $this->hasManyThrough(Question::class, QuestionCategory::class);
    }

    public function answers()
    {
        return $this->hasManyThrough(Answer::class, QuestionCategory::class, Question::class);
    }

}
