<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class UserSurvey extends Model
{

    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class, 'survey_id');
    }

    public function user_answers()
	{
		return $this->hasMany(UserAnswer::class, 'user_survey_id');
    }

    public function answer_to_question($question_id){
        return $this->user_answers()->where('question_id', $question_id)->latest()->first();
    }

    public function getCarbonDate(){
        return Carbon::parse($this->created_at);
    }

}
