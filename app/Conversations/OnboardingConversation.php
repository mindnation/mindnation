<?php

namespace App\Conversations;


use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;

class OnboardingConversation extends Conversation
{
    public function askName()
    {
        $question = Question::create('What kind of Service you are looking for?')
        ->callbackId('select_service')
        ->addButtons([
            Button::create('Hair')->value('Hair'),
            Button::create('Spa')->value('Spa'),
            Button::create('Beauty')->value('Beauty'),
        ]);

    $this->bot->typesAndWaits(2);
    $this->ask($question, function(Answer $answer) {
        if ($answer->isInteractiveMessageReply()) {
            $this->bot->userStorage()->save([
                'service' => $answer->getValue(),
            ]);
        }
        else{
            $this->repeat('use a choice');
        }
    });
    }

    public function run()
    {
        $this->askName();
    }
}
