<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactEnquiry extends Model
{
    public $fillable = ['first_name','last_name','email','company_name','role','contact_number','number_of_employees','message'];
}
