<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMeta extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function isPsychAllowed()
    {
        if (isset(config('user_metas.allowed_metas.psych')[$this->key])) {
            return true;
        }
        return false;
    }

    public function isMemberAllowed()
    {
        if (isset(config('user_metas.allowed_metas.member')[$this->key])) {
            return true;
        }
        return false;
    }
}
