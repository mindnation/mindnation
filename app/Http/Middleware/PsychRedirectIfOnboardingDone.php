<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PsychRedirectIfOnboardingDone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->onboarding){
             //This will redirect the user to the onboarding area, if they haven't logged in before.
             return redirect()->route('psych');
        }

        return $next($request);
    }
}
