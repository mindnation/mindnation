<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MemberRedirectIfAgreementNotSigned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user->agreement){
             //This will redirect the user to the agreement, if they haven't agreed before.
             return redirect()->route('member.agreement');
        }

        return $next($request);
    }
}
