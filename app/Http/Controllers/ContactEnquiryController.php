<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactEnquiry;
use Validator;

use Illuminate\Support\Facades\Mail;
use App\Mail\ContactFormMail;

class ContactEnquiryController extends Controller
{
    public function post(Request $request){

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'company_name' => 'required',
            'role' => 'required',
            'contact_number' => 'required|digits_between:8,15',
            'number_of_employees' => 'required|numeric',
            'message' => 'required'
        ]);

        if ($validator->fails())
        {
            $error_array = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $error_array[$field_name] = $messages;
            }
            return response()->json(['errors'=>$error_array]);
        }

        $enquiry = ContactEnquiry::create($request->except('_token'));  

        
        Mail::to('hello@themindnation.com')->send(new ContactFormMail($enquiry));
        return response()->json(['success'=>'Thank you for your message!']);
        
    }
}
