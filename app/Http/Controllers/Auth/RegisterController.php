<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Company;
use App\UserMeta;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Rules\IsAllowedToRegister;
use App\Rules\IsAlreadyRegistered;
use App\Rules\isValidPassword;

use Carbon\Carbon;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    protected function redirectTo()
    {
        return auth()->user()->getDashboardRoute();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', new IsAlreadyRegistered, new IsAllowedToRegister],
            'password' => ['required', 'string', 'confirmed', new isValidPassword],
            'birth_date' => ['required', 'date_format:d/m/Y'],
            'gender' => 'required',
            'contact_number' => 'required|digits_between:8,15',
            'terms' => 'required',
        ],
        [
            'terms.required' => 'You must agree with the Terms and Conditions and Privacy Policy',
        ]
    );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // Check if user email exist as allowed member (in this case the uuser already exist)
        $user = User::where('email',$data['email'])->first();

        if(!$user){
            // check if the domain of the email is allowed to register (we create the user)
            $domain = substr($data['email'], strpos($data['email'], "@") + 1);
            $company = Company::where('email_domain', $domain)->first();
            if($company){
                $user = new User();
                $user->email = $data['email'];
                $user->company()->associate($company->id);
            }
        }

        if($user){
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->password = Hash::make($data['password']);
            $user->registration_date = Carbon::now()->toDateTimeString();
            $user->save();

            $user->makeMember();

            $user->setMeta('birth_date', $data['birth_date']);
            $user->setMeta('gender', $data['gender']);
            $user->setMeta('contact_number', $data['contact_number']);

            return $user;
        }

        


     
    }


    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        return response()->json(['success'=> true, 'redirect' => $this->redirectPath()]);
    }
}
