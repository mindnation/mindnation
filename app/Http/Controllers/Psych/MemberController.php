<?php

namespace App\Http\Controllers\Psych;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\User;

class MemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function get_data(Request $request){
        $html = '';
        $error = '';

        if ($request->input('id')) {
            $member = User::find($request->input('id'));
            if($member && $member->isPatientOf(Auth::user())){

                $appointments = $member->member_appointments()
                        ->orderBy('date', 'DESC')
                        ->orderBy('start_time', 'DESC')
                        ->get();

                $user_survey_onboarding = $member->user_surveys()->with('survey')
                                    ->whereHas('survey', function($query) {
                                        $query->where('onboarding',true);
                                    })
                                    ->latest()->first();

                $view_data = array(
                    'member' => $member,
                    'appointments' => $appointments,
                    'user_survey_onboarding' => $user_survey_onboarding,
                );
                $html = view('psych.member.details', $view_data)->render();
            }
        }

        if(!$html) $error = 'An error happened, please try again';

        $output = array(
            'error' => $error,
            'html' => $html,
        );
        return json_encode($output);
    }
}
