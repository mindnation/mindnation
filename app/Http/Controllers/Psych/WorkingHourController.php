<?php

namespace App\Http\Controllers\Psych;

use App\Http\Controllers\Controller;
use App\User;
use App\WorkingHour;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class WorkingHourController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $min_date = WorkingHour::getMinDate();
        $max_date = WorkingHour::getMaxDate();

        $view_data = array(
            'min_date' => $min_date,
            'max_date' => $max_date,
        );
        return view('psych.working_hours.index', $view_data);
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $user = Auth::user();
        $output = array();
        $error_array = array();
        $carbon_date = Carbon::parse($request->input('date'));
        $date = $carbon_date->toDateString();
        $working_hours_data = $request->input('working_hours');
        $repeat = $request->input('repeat');
        $repeat_days = $request->input('days');
        $repeat_until = $request->input('repeat_until_date');
        $dates_arr = array($date);
        $not_contained_appointments = array();

        $min_date = WorkingHour::getMinDate();
        $max_date = WorkingHour::getMaxDate();

        
        

        $value_error_message = "Invalid values. Please check and save again";
        $overlapping_error_message = "Periods are overlapping. Please check and save again";

        $working_hours = array();


        if(!($carbon_date->greaterThanOrEqualTo($min_date->startOfDay())&&$carbon_date->lessThanOrEqualTo($max_date))){
            $output['error'][] = 'Invalid dates';
            return json_encode($output);
        }

        if (isset($working_hours_data) && count($working_hours_data)) {
            // Parse each period -.> if valid we store in temp list
            foreach ($working_hours_data as $period) {
                if (!isset($period['start']) || !isset($period['finish'])) {
                    $output['error'][] = $value_error_message;
                    return json_encode($output);
                }
                $start_time = Carbon::createFromFormat('H:i:s', $period['start']);
                $finish_time = Carbon::createFromFormat('H:i:s', $period['finish']);

                $w = new WorkingHour();
                $w->date = $date;
                $w->start_time = $start_time->toTimeString();
                $w->finish_time = $finish_time->toTimeString();

                // validate
                if(!$w->isValid()){
                    $output['error'][] = $value_error_message;
                    return json_encode($output);
                }
                $working_hours[] = $w;
            }

            //check overlaping
            $i = 0; 
            foreach($working_hours as $w1){
                $j = 0;
                foreach($working_hours as $w2){
                    if($i != $j){
                        if($w1->isOverlapping($w2)){
                            $output['error'][] = $overlapping_error_message;
                            return json_encode($output);
                        }
                    }
                    $j++;
                }
                $i++;
            }
        }


        /// Check if there are appointments that are not covered by the new working hours
        /// We do that twice -> create a function maybe

        $appointments_on_this_date = $user->psych_appointments()
                                        ->where('date', $date)
                                        ->where('status', 'confirmed')
                                        ->get();
        if($appointments_on_this_date){
            foreach($appointments_on_this_date as $appointment){
                $contained = false;
                foreach($working_hours as $working_hour)
                {
                    if($appointment->isContainedIn($working_hour)) {$contained = true; break;}
                }
                if(!$contained) $not_contained_appointments[] = $appointment;
            }
        }

        /// Create Working hours for reapeat
        if($repeat){

            $repeat_daily_periods_template = $working_hours;
            if(!$repeat_days || !count($repeat_days)){
                $output['error'][] = 'Select at least one day to repeat';
                return json_encode($output);
            }
            if(!$repeat_until){
                $output['error'][] = 'Select an end date for the repeat period';
                return json_encode($output);
            }
            $carbon_until = Carbon::parse($repeat_until);

            if($carbon_until->lessThanOrEqualTo($carbon_date)){
                $output['error'][] = 'Invalid repeat period.';
                return json_encode($output);
            }

            $repeat_from_to_period = CarbonPeriod::create($carbon_date->add(1, 'day'), $carbon_until);

            foreach ($repeat_from_to_period as $repeat_date) {
                if(isset($repeat_days[$repeat_date->weekday()])){
                    $dates_arr[] = $repeat_date->toDateString();
                    $daily_working_hours = array(); //only store the hours of this day
                    foreach($repeat_daily_periods_template as $period){
                        $w = new WorkingHour();
                        $w->date = $repeat_date->toDateString();
                        $w->start_time = $period->start_time;
                        $w->finish_time = $period->finish_time;
                        $working_hours[] = $w;

                        $daily_working_hours[] = $w;
                    }

                    /// Check if there are appointments that are not covered by the new working hours
                    /// We do that twice -> create a function maybe

                    $appointments_on_this_date = $user->psych_appointments()
                                                    ->where('date', $repeat_date->toDateString())
                                                    ->where('status', 'confirmed')
                                                    ->get();
                    if($appointments_on_this_date){
                        foreach($appointments_on_this_date as $appointment){
                            $contained = false;
                            foreach($daily_working_hours as $working_hour)
                            {
                                if($appointment->isContainedIn($working_hour)) {$contained = true; break;}
                            }
                            if(!$contained) $not_contained_appointments[] = $appointment;
                        }
                    }
                }      
            }

        }
       
        //erase all the existing working hours for these date and save new

        $user->working_hours()
            //->where('date', $date)
            ->whereIn('date', $dates_arr)
            ->delete();

        if(count($working_hours)){
            $user->working_hours()->saveMany($working_hours);
        }

        $success_output = "New working hours saved.";
        if($count_not_contained = count($not_contained_appointments)){
            $text_plural = "There are ".$count_not_contained." consultations";
            if($count_not_contained<2) $text_plural = "There is ".$count_not_contained." consultation";

            $success_output .= $text_plural." not contained in your new working hours.";
        }

        

        $output = array(
            'error' => $error_array,
            'success' => $success_output,
        );
        return json_encode($output);
    }

    public function get_input_template(Request $request){
        $order = $request->get('order');

        $view_data = array(
            'order' => $order,
            'options' => WorkingHour::list_select_options(),
        );

        $data = array(
            'input_template' => view('psych.working_hours.input_period', $view_data)->render(),
        );
        return json_encode($data);
    }


    public function get_data(Request $request)
    {
        $date = Carbon::parse($request->get('date'))->toDateString();

        $working_hours = Auth::user()->working_hours()
            ->where('date', $date)
            ->orderBy('start_time', 'asc')
            ->get();

        $html = '';
        $order = 1;
        foreach ($working_hours as $working_hour) {
            $view_data = array(
                'order' => $order,
                'start_time' => Carbon::parse($working_hour->start_time)->toTimeString(),
                'finish_time' => Carbon::parse($working_hour->finish_time)->toTimeString(),
                'options' => WorkingHour::list_select_options(),
            );
            $html .= view('psych.working_hours.input_period', $view_data)->render();
            $order++;
        }

        $data = array(
            'working_hours' => $html,
        );
        return json_encode($data);
    }
}

