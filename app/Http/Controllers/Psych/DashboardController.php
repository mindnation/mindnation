<?php

namespace App\Http\Controllers\Psych;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\WorkingHour;
use Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = auth()->user();

        $min_time = WorkingHour::getCarbonConfigMinTime()->toTimeString();
        $max_time = WorkingHour::getCarbonConfigMaxTime()->toTimeString();

        $view_data= array(
            'assigned_companies'=>$user->psych_assigned_companies()->orderby('name')->get(),

            'events' => $this->get_events(),
            'min_time' => $min_time,
            'max_time' => $max_time,
        );
        return view('psych.dashboard',$view_data);
    }


    public function get_events(){

        //TODO ASAP: dynamic query load only the recents
        $events = array();

        $user = Auth::user();
        
        $appointments = $user->psych_appointments()
                        ->where('status','confirmed')
                        ->whereDate('date', '>=', Carbon::now())
                        ->get();

        foreach($appointments as $a){
            $temp = array(
                'title' => $a->member->name(),
                'description' => $a->member->company->name,
                'start' => $a->getCarbonStart()->toIso8601String() ,
                'end' => $a->getCarbonFinish()->toIso8601String(),
                'className' => ['appointment', $a->status],
            );
            $events[] = $temp;
        }


        return json_encode($events);
    }
}
