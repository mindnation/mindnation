<?php

namespace App\Http\Controllers\Psych;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Auth;
use App\WorkingHour;
use Carbon\Carbon;


class CalendarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $min_time = WorkingHour::getCarbonConfigMinTime()->toTimeString();
        $max_time = WorkingHour::getCarbonConfigMaxTime()->toTimeString();

        $view_data = array(
            'events' => $this->get_events(),
            'min_time' => $min_time,
            'max_time' => $max_time,

        );
        return view('psych.calendar.index', $view_data);
    }

    public function get_events(){

        //TODO ASAP: dynamic query load only the recents

        $events = array();

        $user = Auth::user();
        
        $working_hours = $user->working_hours()->get();
        $appointments = $user->psych_appointments()->whereIn('status',['confirmed','completed'])->get();

        foreach($working_hours as $w){
            $temp = array(
                'groupId' => 'workingHours',
                'start' => $w->getCarbonStart()->toIso8601String() ,
                'end' => $w->getCarbonFinish()->toIso8601String(),
                'rendering' => 'inverse-background',
                'className' => 'non-working-hours',
            );
            $events[] = $temp;
        }
        foreach($appointments as $a){
            $temp = array(
                'title' => $a->member->name(),
                'description' => $a->member->company->name,
                'start' => $a->getCarbonStart()->toIso8601String() ,
                'end' => $a->getCarbonFinish()->toIso8601String(),
                'className' => ['appointment', $a->status],
            );
            $events[] = $temp;
        }


        return json_encode($events);
    }
}
