<?php

namespace App\Http\Controllers\Psych;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use Carbon\Carbon;
use App\Appointment;


use App\Notifications\MemberAppointmentCanceled;
use App\Notifications\PsychAppointmentCanceled;
use App\Notifications\MemberAppointmentCompleted;




class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


    public function index()
    {
        $datatable_columns = array(
            //array('data' => 'datetime', 'name' => 'datetime', 'title' => 'Datetime', 'visible'=> false,'defaultOrder'=> true, 'sortOrder'=> 'desc'),
            array('data' => 'date', 'name' => 'date', 'title' => 'Date' ,'defaultOrder'=> true, 'sortOrder'=> 'desc'),
            array('data' => 'time', 'name' => 'time', 'title' => 'Time','orderable' => false),
            array('data' => 'patient', 'name' => 'patient', 'title' => 'Patient','orderable' => false),
            array('data' => 'company', 'name' => 'company', 'title' => 'Company','orderable' => false),
            array('data' => 'display_status', 'name' => 'status', 'title' => 'Status','orderable' => false),
            array('data' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false),
        );
        
        $view_data = array(
            'datatable_data_route' => route('psych.appointments.datatable'),
            'datatable_columns' => $datatable_columns,

        );
        return view('psych.appointment.index', $view_data);
    }

    public function datatable()
    {
        $data = Auth::user()->psych_appointments()->with(['member']);
        return Datatables::of($data)
            // ->addColumn('datetime', function (Appointment $appointment) {
            //     return $appointment->getCarbonStart()->toDateTimeString();
            // })
            ->addColumn('date', function (Appointment $appointment) {
                return $appointment->getCarbonStart()->format('d/m/Y');
            })
            ->orderColumn('date', function ($query, $order) {
                $query  ->orderBy('date', $order)
                        ->orderBy('start_time', $order); 
            })
            ->filterColumn('date', function($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(date, '%d/%m/%Y') like ?", ["%{$keyword}%"]);   
            })
            ->addColumn('time', function (Appointment $appointment) {
                return $appointment->getCarbonStart()->format('g:i A');
            })
            ->filterColumn('time', function($query, $keyword) {
                $query->whereRaw("TIME_FORMAT(start_time, '%h:%i %p') like ?", ["%{$keyword}%"]);   
            })
            ->addColumn('patient', function (Appointment $appointment) {
                return $appointment->member->name();
            })
            ->filterColumn('patient', function($query, $keyword) {
                $query->whereHas('member', function($query) use ($keyword) {
                    $query->whereRaw("CONCAT(first_name,' ',last_name) like ?", ["%{$keyword}%"]);
                });
            })
            ->addColumn('company', function (Appointment $appointment) {
                return $appointment->member->company->name;
            })
            ->addColumn('display_status', function (Appointment $appointment) {
                //return $appointment->getStatus();
                return view('partials.appointment-status',['appointment'=>$appointment])->render();
            })
            ->addColumn('action', function (Appointment $appointment) {
                $view_data = array(
                    'appointment' => $appointment,
                    'member' => $appointment->member,
                );
                return view('psych.appointment.action_btn', $view_data)->render();
            })
            // ->addColumn('action', 'admin.companies.action_btn')
            ->rawColumns(['action', 'added', 'display_status'])
            ->make(true);
    }


    public function get_data(Request $request){
        $html = '';
        $error = '';

        if ($request->input('id')) {
            $appointment = Appointment::find($request->input('id'));
            
            if($appointment && $appointment->psychIs(Auth::user())){
                $patient = $appointment->member;

                $view_data = array(
                    'appointment' => $appointment,
                    'patient' => $patient,

                );
                $html = view('psych.appointment.details', $view_data)->render();
            }
        }

        if(!$html) $error = 'An error happened, please try again';

        $output = array(
            'error' => $error,
            'html' => $html,
        );
        return json_encode($output);
    }

    public function edit(Request $request){
        $error = '';
        $success = '';
        $saved = null;

        if ($request->input('id') && $request->input('status')) {
            $appointment = Appointment::find($request->input('id'));
            if($appointment && $appointment->psychIs(Auth::user())){
                if ($appointment->status == $request->input('status'))
                {
                    return response()->json(['errors' => ['message' => 'Status not changed']], 422);
                }
                $saved = $appointment->setStatus($request->input('status'));
            }
        }
        if($saved) {
            $success = 'Status Updated';
            if($appointment->isCompleted()){
                //Check if it's the first appointmennt completed
                $completed_appointments_count = $appointment->member->member_appointments()
                        ->where('status','completed')
                        //->whereDate('date', '<', $appointment->date)
                        ->count();
                $firstCompletedAppointment = ($completed_appointments_count <= 1);

                //Queue mail notifications
                $appointment->member->notify(new MemberAppointmentCompleted($appointment, $firstCompletedAppointment));
                
            }
            else if($appointment->isCanceled()){
                //Queue mail notifications
                $appointment->member->notify(new MemberAppointmentCanceled($appointment));
                $appointment->psych->notify(new PsychAppointmentCanceled($appointment,true));
            }

        }
        else return response()->json(['errors' => ['message' => 'An error happened, please try again']], 422);

        $output = array(
            'error' => $error,
            'success' => $success,
        );
        return json_encode($output);
    }
}
