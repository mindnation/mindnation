<?php

namespace App\Http\Controllers\Psych;

use App\Http\Controllers\Controller;
use App\Rules\isValidPassword;
use App\Rules\MatchOldPassword;
use App\User;
use App\Media;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Image;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function onboarding()
    {
        $psych_avatars = Media::where('type', 'psych_avatar')->get();

        $view_data = array(
            'psych_avatars' => $psych_avatars,
        );
        return view('psych.account.onboarding', $view_data);
    }

    public function profile()
    {
        $psych_avatars = Media::where('type', 'psych_avatar')->get();

        $view_data = array(
            'psych_avatars' => $psych_avatars,
        );
        return view('psych.account.profile', $view_data);
    }

    public function store_onboarding(Request $request)
    {

        $request->validate([
            'full_name' => 'required|max:255',
            'contact_number' => 'required|numeric|digits_between:8,15',
            'short_background' => 'required',
            'timeslots' => 'required',
            'doxyme_url' => 'required|url',
            'expertises' => 'required',
            'avatar'    => 'nullable|mimes:png|dimensions:ratio=1/1|max:2048', //dimensions:min_width=500,min_height=500
        ],
            [
                'expertises.required' => 'Select at least one expertise.',
            ]
        );

        $user = Auth::user();


        $existing_avatar_selected = $request->input('avatar_selected');
        if($existing_avatar_selected){
            $media = Media::where('id', $existing_avatar_selected)
                            ->where('type', 'psych_avatar')
                            ->first();
            if($media){
                $user->avatar()->associate($media->id);
            }
        }
        /// if the user upload we use it.
        $avatar = $request->file('avatar');
        if($avatar){
            

            $image_name = time() . '.' . $avatar->getClientOriginalExtension();
            $avatar_path = 'psych_avatars/';
            $resized_path = $avatar_path.'resized';

            $resize_image = Image::make($avatar->getRealPath());

            $resize_image->resize(300, 300, function($constraint){
            $constraint->aspectRatio();
            });

            Storage::disk('public')->put($resized_path.'/'.$image_name,  (string) $resize_image->encode());

            $media = new Media();
            $media->mime = $avatar->getClientMimeType();
            $media->original_filename = $avatar->getClientOriginalName();
            $media->filename = $resized_path.'/'.$image_name;//'psych_avatars/'.$avatar->getFilename().'.'.$extension;
            $media->type = 'psych_avatar';
            $media->save();

            $user->avatar()->associate($media->id);
        }

        if(!$user->avatar && !$avatar && !$existing_avatar_selected){
            $request->validate([
               'avatar'    => 'required|mimes:png|dimensions:ratio=1/1|max:2048', //dimensions:min_width=500,min_height=500
            ],
                [
                    'avatar.required' => 'The avatar is required',
                ]
            );
        }

        

        

        

        $saved_expertises = array();
        foreach ($request->input('expertises') as $key => $value) {
            if (isset(config('issues')[$key])) {
                $saved_expertises[$key] = $value;
            }
        }
        $user->setMeta('expertises', serialize($saved_expertises), 'psych');


        // SET all other allowed metas
        $user_metas = $request->except(['_token', 'expertises', 'avatar','avatar_selected' ]);

        foreach ($user_metas as $key => $value) {
            $user->setMeta($key, $value, 'psych');
        }
        $user->onboarding = Carbon::now()->toDateTimeString();
        $user->save();

        return redirect()->route('psych');
    }

    public function store_profile(Request $request)
    {
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'full_name' => 'required|max:255',
            'contact_number' => 'required|numeric|digits_between:8,15',
            'short_background' => 'required',
            'timeslots' => 'required',
            'doxyme_url' => 'required|url',
            'expertises' => 'required',
            'avatar'    => 'mimes:png|dimensions:ratio=1/1|max:2048', //dimensions:min_width=500,min_height=500
        ],
        [
            'expertises.required' => 'Select at least one expertise.',
        ]
        );

        $user = Auth::user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');

        $existing_avatar_selected = $request->input('avatar_selected');
        if($existing_avatar_selected){
            $media = Media::where('id', $existing_avatar_selected)
                            ->where('type', 'psych_avatar')
                            ->first();
            if($media){
                $user->avatar()->associate($media->id);
            }
        }
        /// if the user upload we use it.
        $avatar = $request->file('avatar');
        if($avatar){


            $image_name = time() . '.' . $avatar->getClientOriginalExtension();
            $avatar_path = 'psych_avatars/';
            $resized_path = $avatar_path.'resized';

            $resize_image = Image::make($avatar->getRealPath());

            $resize_image->resize(300, 300, function($constraint){
            $constraint->aspectRatio();
            });

            Storage::disk('public')->put($resized_path.'/'.$image_name,  (string) $resize_image->encode());

            $media = new Media();
            $media->mime = $avatar->getClientMimeType();
            $media->original_filename = $avatar->getClientOriginalName();
            $media->filename = $resized_path.'/'.$image_name;
            $media->type = 'psych_avatar';
            $media->save();

            $user->avatar()->associate($media->id);
        }

        if(!$user->avatar && !$avatar && !$existing_avatar_selected){
            return back()->withErrors(['avatar'=>'The avatar is required']);
        }



        $saved_expertises = array();
        foreach ($request->input('expertises') as $key => $value) {
            if (isset(config('issues')[$key])) {
                $saved_expertises[$key] = $value;
            }
        }
        $user->setMeta('expertises', serialize($saved_expertises), 'psych');
        

        $user_metas = $request->except(['first_name', 'last_name', 'expertises', '_token']);

        foreach ($user_metas as $key => $value) {
            $user->setMeta($key, $value, 'psych');

        }
        $user->save();

        return redirect()->route('psych.account.profile')->with('success', 'Profile Updated');
    }

    public function password()
    {
        return view('psych.account.password');
    }

    public function store_password(Request $request)
    {

        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required', new isValidPassword],
            'confirm_new_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        return redirect()->route('psych.account.password')->with('success', 'Password successfully changed');
    }

}
