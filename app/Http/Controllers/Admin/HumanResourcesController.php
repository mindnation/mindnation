<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;
use DB;


class HumanResourcesController extends Controller
{
    public function registered()
    {
        $datatable_columns = array(
            array('data' => 'id', 'name' => 'id', 'title' => 'Id', 'visible' => false),
            array('data' => 'first_name', 'name' => 'first_name', 'title' => 'First Name', 'visible'=> false), /// for the search
            array('data' => 'last_name', 'name' => 'last_name', 'title' => 'Last Name', 'visible'=> false), /// for the search
            array('data' => 'email', 'name' => 'email', 'title' => 'Email'),
            array('data' => 'display_company', 'name' => 'display_company', 'title' => 'Company','orderable' => false),
            array('data' => 'company', 'name' => 'company.name', 'title' => 'Company', 'visible' => false), //for search
            array('data' => 'name', 'name' => 'name', 'title' => 'Name', 'orderData'=> 1),
            array('data' => 'added', 'name' => 'added', 'title' => 'Added by','orderable' => false),
            array('data' => 'created_at', 'name' => 'created_at', 'title' => 'Created at', 'defaultOrder' => true, 'sortOrder' => "desc"),
            array('data' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false),
        );

        $view_data = array(
            'entry_plural' => 'Human Resources',
            'entry_singular' => 'Human Resource',
            'description' => '',
            'datatable_data_route' => route('admin.humanresources.registered.datatable'),
            'datatable_columns' => $datatable_columns,
            'company_select_data' => Company::select(
                                     DB::raw("CONCAT(name, CONCAT(' [', CONCAT(email_domain, ']'))) as display_name"), 'id')
                                    ->pluck('display_name', 'id')
                                    ->all()
        );
        return view('admin.humanresources.registered', $view_data);
    }

    public function datatable_registered()
    {
        $data = User::whereHas(
            'roles', function ($query) {
                $query->where('name', 'human_resources');
            }
        )->whereNull('registration_date')
        ->with('company');

        return Datatables::of($data)
            ->addColumn('name', function (User $user) {return view('admin.partials.datatable.col-user-name', ['user' => $user])->render();})
            ->addColumn('display_company', function (User $user) {
                $company = $user->company;
                if(!$company) return null;
                $icon_blocked = '<div class="float-right"><i class="text-danger fas fa-ban mr-1" data-toggle="tooltip" data-placement="top"
                title="Company Suspended on: '.$company->blocked_at.'"></i></div>';
                return $company->name.' '.(($company->blocked_at)?$icon_blocked:'');
            })
            ->addColumn('added', function (User $user) {
                if (isset($user->added_by)) {
                    return $user->added_by->first_name; //short_name().' (Id: '.$user->added_by->id.')';
                }
            })
            ->addColumn('action', 'admin.humanresources.action_btn')
            ->addColumn('registered', function (User $user) {return ($user->registration_date) ? $user->registration_date : 'Not yet';})
            ->rawColumns(['action', 'display_company', 'name', 'added'])
            ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_data(Request $request)
    {

        $user = User::findOrFail($request->input('id'));

        $data = array(
            'user' => $user,
            //'company' => $user->company->id,
        );

        return json_encode($data);
    }


    public function delete(Request $request){
        $user = User::find($request->input('id'));
        if(!$user || !$user->isHR()){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }

        if($request->input('permanently')){
        
            try{ 
                $user->forceDelete();
            }catch(\Exception $e){
                return response()->json(['errors' => ['message'=> "This entity cannot be deleted permanently."]], 422);
                $this->info($e->getMessage());        
            }
        }
        else{
            return response()->json(['errors' => ['message'=> "Tick the checkbox."]], 422);
        }

        
        return response()->json(['success' => true, 'message' => 'Deleted successfully']);

    }

    public function block(Request $request){
        $user = User::find($request->input('id'));
        if(!$user && $user->isHR()){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }
        $user->blocked_at = Carbon::now()->toDateTimeString();
        $user->save();

        return response()->json(['success' => true, 'message' => 'Suspended successfully']);
    }

    public function unblock(Request $request){

        $user = User::find($request->input('id'));
        if(!$user && $user->isHR()){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }
        $user->blocked_at = null;
        $user->save();

        return response()->json(['success' => true, 'message' => 'Activated successfully']);

    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'company' => 'required',
        ]);

        $success_output = '';

        if ($validator->fails()) {
            $error_array = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $error_array[$field_name] = $messages;
            }
            return response()->json(['errors' => $error_array], 422);
        } else {

            $success_output = 'Data Saved';

            if ($request->input('id')) {
                $user = User::find($request->input('id'));

            }

            if (!isset($user)) {
                $validator = Validator::make($request->all(), [
                    'email' => ['required', 'email', 'unique:users,email'],
                ]);
                if ($validator->fails()) {
                    $error_array = array();
                    foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                        $error_array[$field_name] = $messages;
                    }
                    return response()->json(['errors' => $error_array], 422);
                }
                $user = new User();
                $user->setAddedBy();
                $user->email = $request->input('email');
            }

            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');

            $user->updated_at = Carbon::now()->toDateTimeString();

            $company = Company::find($request->input('company'));
            if ($company) {$user->company()->associate($company->id);}

            $user->save();

            $user->makeHumanResources();

        }

        $output = array(

            'success' => true,
            'message' => $success_output,
        );
        return json_encode($output);
    }
}
