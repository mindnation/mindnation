<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rules\isValidPassword;
use App\Rules\MatchOldPassword;
use App\User;
use App\UserMeta;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    
    public function details()
    {
        return view('admin.account.details');
    }

    public function store_details(Request $request)
    {

        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
        ]);

        $user = Auth::user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');

        
        $user->save();

        return redirect()->route('admin.account.details')->with('success', 'Details Updated');
    }

    public function password()
    {
        return view('admin.account.password');
    }

    public function store_password(Request $request)
    {

        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required', new isValidPassword],
            'confirm_new_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        return redirect()->route('admin.account.password')->with('success', 'Password successfully changed');
    }

   
}
