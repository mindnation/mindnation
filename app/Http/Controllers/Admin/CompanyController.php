<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;

use Carbon\Carbon;


use App\Company;


class CompanyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datatable_columns = array(
            array('data' => 'id', 'name' => 'id', 'title' => 'Id', 'visible'=> false),
            array('data' => 'display_name', 'name' => 'name', 'title' => 'Name'),
            array('data' => 'description', 'name' => 'description', 'title' => 'Description', 'searchable' => false),
            array('data' => 'email_domain', 'name' => 'email_domain', 'title' => 'Email Domain', 'searchable' => true),
            array('data' => 'start_date', 'name' => 'start_date', 'title' => 'Start Date', 'searchable' => false),
            array('data' => 'end_date', 'name' => 'end_date', 'title' => 'End Date', 'searchable' => false),
            array('data' => 'added', 'name' => 'added', 'title' => 'Added By','orderable' => false),
            // array('data' => 'created_at', 'name' => 'created_at', 'title' => 'Created at'), 
            // array('data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated at', 'defaultOrder'=> true, 'sortOrder'=> 'desc'),
            array('data' => 'action', 'title' => 'Actions', 'orderable' => false, 'searchable' => false),
        );

        $view_data = array(
            'entry_plural' => 'Companies',
            'entry_singular' => 'Company',
            'description' => '',
            'datatable_data_route' => route('admin.companies.datatable'),
            'datatable_columns' => $datatable_columns,

        );
        return view('admin.companies.index', $view_data);
    }

    public function datatable()
    {
        $data = Company::query();
        return Datatables::of($data)
            ->addColumn('display_name', function (Company $company) {
                $icon_blocked = '<div class="float-right"><i class="text-danger fas fa-ban mr-1" data-toggle="tooltip" data-placement="top"
                title="Blocked on: '.$company->blocked_at.'"></i></div>';
                return $company->name.' '.(($company->blocked_at)?$icon_blocked:'');
            })
            ->addColumn('added', function (Company $company) {
                if(isset($company->added_by)){
                    return $company->added_by->short_name().' (Id: '.$company->added_by->id.')';
                }
            })
            ->addColumn('action', 'admin.companies.action_btn')
            ->rawColumns(['display_name','action', 'added'])
            ->make(true);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = null;
        if ($request->input('id')) {
            $company = Company::find($request->input('id'));
            $id = $company->id;

        }
        //TODO create a proper rule for that
        Validator::extend('correct_domain_format', function($attribute, $value, $parameters)
        {
            $words = array('@');
            foreach ($words as $word)
            {
                if (stripos($value, $word) !== false) return false;
            }
            return true;
        });

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:20', Rule::unique('App\Company', 'name')->ignore($id)],
            'email_domain' => ['correct_domain_format', Rule::unique('App\Company', 'email_domain')->ignore($id)],
            //'description' => ['string', 'max:255'],
        ],[
            'email_domain.correct_domain_format' => 'The :attribute must not contain @',
        ]);

        
        $success_output = '';
            
      
        if ($validator->fails()) {
            $error_array = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $error_array[$field_name] = $messages;
            }
            return response()->json(['errors' => $error_array], 422);
        } else {

            $success_output = 'Data Saved';
 
            
            if (!isset($company)) {
                $company = new Company();
                $company->setAddedBy();
            }

            $company->name = $request->input('name');
            $company->description = $request->input('description');
            $company->email_domain = $request->input('email_domain');

            //$company->updated_at = Carbon::now()->toDateTimeString();
            $company->save();

        }

        $output = array(
            
            'success' => true,
            'message' => $success_output,
        );
        return json_encode($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_data(Request $request)
    {
        
        $company = Company::findOrFail($request->input('id'));

        $data = array(
            'company' => $company,
        );
        
        return json_encode($data);
    }
   

    public function autocomplete(Request $request){

        $data = Company::where('name', 'LIKE', "%{$request->get('q')}%")->get();

        return response()->json($data);
    }


    public function delete(Request $request){
        $company = Company::find($request->input('id'));
        if(!$company){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }

        //temporary fix to dev environment not checking foreign keys
        if($company->employees()->count() || $company->assigned_psychs()->count()){
            return response()->json(['errors' => ['message'=> "This entity cannot be deleted permanently."]], 422);
        }

        if($request->input('permanently')){
        
            try{ 
                $company->forceDelete();
            }catch(\PDOException $e){
                return response()->json(['errors' => ['message'=> "This entity cannot be deleted permanently."]], 422);
                $this->info($e->getMessage());        
            }
        }
        else{
            return response()->json(['errors' => ['message'=> "Tick the checkbox."]], 422);
        }

        
        return response()->json(['success' => true, 'message' => 'Deleted successfully']);

    }

    public function block(Request $request){
        $company = Company::find($request->input('id'));
        if(!$company){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }
        $company->blocked_at = Carbon::now()->toDateTimeString();
        $company->save();

        return response()->json(['success' => true, 'message' => 'Suspended successfully']);
    }

    public function unblock(Request $request){

        $company = Company::find($request->input('id'));
        if(!$company){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }
        $company->blocked_at = null;
        $company->save();

        return response()->json(['success' => true, 'message' => 'Activated successfully']);

    }

    
}
