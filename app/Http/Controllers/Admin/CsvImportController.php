<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\CsvImport;
use App\Company;
use App\User;
use Carbon\Carbon;

class CsvImportController extends Controller
{
    public function parse(Request $request){
        
        $validator = Validator::make($request->all(), [
            'csv_file'    => 'required|mimes:csv,txt',
            'company'   => 'required',
        ]);


        if ($validator->fails()) {
            $error_array = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $error_array[$field_name] = $messages;
            }
            return response()->json(['errors' => $error_array], 422);
        }
        else{
            $path = $request->file('csv_file')->getRealPath();

            $company = Company::find($request->input('company'));
            if(!$company){
                return response()->json(['error' => 'Something went wrong.<br>Reload the page and try again.'], 422);
            }
            
             
            $invalid_rows = array();
            $emails = array();
            $index = 0;
            $delimiter = ($request->input('delimiter')=='semicolon')?';':','; 
            
            $handle = fopen($path, "r");
            $header = $request->input('has_header');

            while ($csvLine = fgetcsv($handle, 1000, $delimiter)) {
                
                if ($header) {
                    $header = false;
                } else {
                    $index++;
                    $email_found = false;
                    //automatically looking for the email in the row (this is not optimal for large file. we should define a model)
                    foreach($csvLine as $cell){
                        $arr = ['cell' => $cell];
                        $validator = Validator::make($arr, [
                            'cell'    => 'email|unique:users,email',
                        ]);
                        if($validator->passes()){
                            // save row
                            $emails[$index] = $cell;
                            $email_found = true;
                            break;
                            
                        }
                    }
                    if(!$email_found){
                        //log ignored row
                        $invalid_rows[$index] = implode($delimiter, $csvLine);
                        
                    }
                    
                }
            }

            

            // Check for duplicates
            
            $unique_emails = array_unique($emails);
            //dd(count($unique_emails));
            $duplicates = array_diff_assoc($emails, $unique_emails);
            //dd($duplicates);  

            $view_data = [
                'total_rows' => $request->input('has_header')?($index-1):$index,
                'duplicates' => $duplicates,
                'invalid_rows' => $invalid_rows,
                'count_imports' => count($unique_emails),
            ];

            //Save file in storage

            $csv_import = new CsvImport();
            $csv_import->user()->associate(auth()->user()->id);
            $csv_import->company()->associate($company->id);
            $csv_import->data = serialize($unique_emails);
            $csv_import->comments = serialize($view_data);
            $csv_import->save();

            $view_data['company'] = $company;
            $view_data['csv_import_id'] = $csv_import->id;

            return response()->json([
                'success' => true,
                'html'=> view('admin.csv-imports.parse', $view_data)->render(),
                
            ]);


        }
    }

    public function process(Request $request){
        $csv_import = CsvImport::find($request->input('id'));
        
        if(!$csv_import || $csv_import->imported_at){
            return response()->json(['error' => 'Something went wrong.<br>Reload the page and try again.'], 422);
        }

        $company = $csv_import->company;

        $emails = unserialize($csv_import->data);
        $index = 0;
    //     $emails = collect($emails);
    //    // dd($emails->count());
    //     $emails->chunk(1000, function($chunk){
    //         dd($chunk);
            foreach($emails as $email){
                $user = new User();
                $user->setAddedBy();
                $user->email = $email;
                $user->company()->associate($company->id);
                $user->save();
                $user->makeMember();
                $index ++;
            }
        // });
        


        $csv_import->imported_at = Carbon::now()->toDateTimeString();
        $csv_import->save();


        $output = array(
            'success' => true,
            'html' => '<div class="text-center text-success my-5">Import Successful ('.$index.' Allowed Members Added)<br>Redirecting...</div>',
            'redirect' => route('admin.members.allowed'),
        );
        return json_encode($output);
    }
}
