<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;


use App\Appointment;


class AppointmentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datatable_columns = array(
            array('data' => 'professional', 'name' => 'professional', 'title' => 'Professional', 'orderable' => false),
            array('data' => 'display_date', 'name' => 'date', 'title' => 'Date','defaultOrder'=> true, 'sortOrder'=> 'desc' ),
            array('data' => 'patient', 'name' => 'patient', 'title' => 'Patient','orderable' => false),
            array('data' => 'company', 'name' => 'company', 'title' => 'Company','orderable' => false),
            array('data' => 'display_status', 'name' => 'status', 'title' => 'Status','orderable' => false),
           // array('data' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false),
        );

        $view_data = array(
            'entry_plural' => 'Consultations',
            'entry_singular' => 'Consultation',
            'description' => '',
            'datatable_data_route' => route('admin.appointments.datatable'),
            'datatable_columns' => $datatable_columns,

        );
        return view('admin.appointments.index', $view_data);
    }

    public function datatable()
    {
        $data = Appointment::query()->with(['psych','member']);
        return Datatables::of($data)
        ->filterColumn('professional', function($query, $keyword) {
            $query->whereHas('psych', function($query) use ($keyword) {
                $query->whereRaw("CONCAT(first_name,' ',last_name) like ?", ["%{$keyword}%"]);
            });
        })
        ->filterColumn('patient', function($query, $keyword) {
            $query->whereHas('member', function($query) use ($keyword) {
                $query->whereRaw("CONCAT(first_name,' ',last_name) like ?", ["%{$keyword}%"]);
            });
        })

        ->addColumn('display_date', function (Appointment $appointment) {
            return $appointment->getCarbonStart()->format('d/m/Y - g:i A');
        })
        ->orderColumn('display_date', function ($query, $order) {
            $query->orderBy('date', $order)
                    ->orderBy('start_time', $order);
        })
        ->filterColumn('date', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(date, '%d/%m/%Y') like ?", ["%{$keyword}%"])   
                ->orWhereRaw("TIME_FORMAT(start_time, '%h:%i %p') like ?", ["%{$keyword}%"]);   
        
        })

        ->addColumn('professional', function (Appointment $appointment) {
            return $appointment->psych->name();
        })
        
        ->addColumn('patient', function (Appointment $appointment) {
            return $appointment->member->name();
        })
        ->addColumn('company', function (Appointment $appointment) {
            return $appointment->member->company->name;
        })
        ->addColumn('display_status', function (Appointment $appointment) {
            //return $appointment->getStatus();
            return view('partials.appointment-status',['appointment'=>$appointment])->render();
        })
        // ->addColumn('action', function (Appointment $appointment) {
        //     $view_data = array(
        //         'appointment' => $appointment,
        //         'member' => $appointment->member,
        //     );
        //     return view('psych.appointment.action_btn', $view_data)->render();
        // })
        // ->addColumn('action', 'admin.companies.action_btn')
        ->rawColumns([ 'added', 'display_status'])
        ->make(true);
    }



    

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function get_data(Request $request)
    // {
        
    //     $company = Company::findOrFail($request->input('id'));

    //     $data = array(
    //         'company' => $company,
    //     );
        
    //     return json_encode($data);
    // }
   
}
