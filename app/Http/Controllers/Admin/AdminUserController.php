<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;

use App\User;
use App\Company;

use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;




class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datatable_columns = array(
            array('data' => 'id', 'name' => 'id', 'title' => 'Id', 'visible'=> false),
            array('data' => 'first_name', 'name' => 'first_name', 'title' => 'First Name', 'visible'=> false), /// for the search
            array('data' => 'last_name', 'name' => 'last_name', 'title' => 'Last Name', 'visible'=> false), /// for the search
            array('data' => 'name', 'name' => 'first_name', 'title' => 'Name', 'orderData'=> 1),
            array('data' => 'email', 'name' => 'email', 'title' => 'Email'),
            //array('data' => 'registered', 'name' => 'registered', 'title' => 'Registered'),
            array('data' => 'added', 'name' => 'added', 'title' => 'Added by','orderable' => false),
            array('data' => 'created_at', 'name' => 'created_at', 'title' => 'Created at'), 
            //array('data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated at', 'defaultOrder' => true, 'sortOrder'=> "desc"),
            array('data' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false),
        );

        $view_data = array(
            'entry_plural' => 'Admin Users',
            'entry_singular' => 'Admin User',
            'description' => '',
            'datatable_data_route' => route('admin.admin-users.datatable'),
            'datatable_columns' => $datatable_columns,

        );
        return view('admin.admin-user.index', $view_data);
    }

    public function datatable()
    {
        // $data = User::withTrashed()->whereHas(
        //     'roles', function ($query) {
        //         $query->where('name', 'psychologist');
        //     }
        // )->get();

        $data = User::whereHas(
            'roles', function ($query) {
                $query->where('name', 'admin')
                    ->where('name', '!=' , 'super_admin');
            }
        );
        
        return Datatables::of($data)
            ->addColumn('name', function (User $user) {return view('admin.partials.datatable.col-user-name', ['user'=> $user])->render();}) 
            
            ->addColumn('added', function (User $user) {
                if(isset($user->added_by)){
                    return $user->added_by->first_name;//short_name().' (Id: '.$user->added_by->id.')';
                }
            })
            ->addColumn('action', 'admin.admin-user.action_btn')
            ->addColumn('registered', function (User $user) {return ($user->registration_date)?$user->registration_date:'Not yet';})
            ->rawColumns(['action', 'name', 'added'])
            ->make(true);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            //'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $error_array = array();
        $success_output = '';

        if ($validator->fails()) {
            $error_array = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $error_array[$field_name] = $messages;
            }
            return response()->json(['errors' => $error_array], 422);
        } else {

            $success_output = 'Data Saved';
            

            $user = User::find($request->input('id'));
            
            if ($user && $user->isAdmin()) {

                $user->first_name = $request->input('first_name');
                $user->last_name = $request->input('last_name');
                $user->updated_at = Carbon::now()->toDateTimeString();
                $user->save();
            }
            else{
                $validator = Validator::make($request->all(), [
                    'email' => ['required', 'unique:users,email', 'string', 'email', 'max:255'],
                ]);
                if ($validator->fails()) {
                    $error_array = array();
                    foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                        $error_array[$field_name] = $messages;
                    }
                    return response()->json(['errors' => $error_array], 422);
                }

                $user = new User();
                $user->setAddedBy();
                $user->email = $request->input('email');


                // We register the user directly and send email with pass

                $user->registration_date = Carbon::now()->toDateTimeString();
                $user->markEmailAsVerified();
                $pw = User::generatePassword();
                $user->password = $pw;
                $user->first_name = $request->input('first_name');
                $user->last_name = $request->input('last_name');
                $user->updated_at = Carbon::now()->toDateTimeString();
                $user->save();
                $user->makeAdmin();

                $user->sendStaffWelcomeEmail();

                $success_output = 'New User Added. Welcome email sent';

            }

        }

        $output = array(
            'success' => true,
            'message' => $success_output,
        );
        return json_encode($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_data(Request $request)
    {
        
        $user = User::findOrFail($request->input('id'));
 


        $data = array(
            'user' => $user,
        );
        
        return json_encode($data);
    }


  

    public function delete(Request $request){
        $user = User::find($request->input('id'));
        if(!$user || !$user->isAdminOnly()){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }

        if($request->input('permanently')){
            $user->forceDelete();
            try{ 
                $user->forceDelete();
            }catch(\Exception $e){
                return response()->json(['errors' => ['message'=> "This entity cannot be deleted permanently."]], 422);
                $this->info($e->getMessage());        
            }
        }
        else{
            return response()->json(['errors' => ['message'=> "Tick the checkbox."]], 422);
        }

        
        return response()->json(['success' => true, 'message' => 'Deleted successfully']);

    }

    public function block(Request $request){
        $user = User::find($request->input('id'));
        if(!$user && $user-isAdminOnly()){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }
        $user->blocked_at = Carbon::now()->toDateTimeString();
        $user->save();

        return response()->json(['success' => true, 'message' => 'Suspended successfully']);
    }

    public function unblock(Request $request){

        $user = User::find($request->input('id'));
        if(!$user && $user-isAdminOnly()){
            return response()->json(['errors' => ['message'=> 'Entity not found']], 422);
        }
        $user->blocked_at = null;
        $user->save();

        return response()->json(['success' => true, 'message' => 'Activated successfully']);

    }


}
