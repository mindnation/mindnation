<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



class FrontendController extends Controller
{

    public function home()
    {
        return view('frontend.home');
    }
    public function aboutus()
    {
        return view('frontend.about-us');
    }
    public function formembers()
    {
        return view('frontend.for-members');
    }
    public function privacypolicy()
    {
        return view('frontend.privacy');
    }
    public function terms()
    {
        return view('frontend.terms');
    }
    public function videos()
    {
        return view('frontend.videos');
    }
}
