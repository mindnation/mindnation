<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Appointment;

use App\Notifications\MemberAppointmentCanceled;
use App\Notifications\PsychAppointmentCanceled;



class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $appointments = Auth::user()->member_appointments()
            ->orderBy('date', 'DESC')
            ->orderBy('start_time', 'DESC')
            ->latest('updated_at')
            ->get();

        $view_data = array(
            'appointments' => $appointments,
        );
        return view('member.appointment.index', $view_data);
    }

    public function cancel(Request $request)
    {

        if (!$request->input('id')) {
            return back()->withErrors(['Something went wrong. Please try again.']);
        }
        $appointment = Appointment::find($request->input('id'));

        //check appointment exists and user owns it
        if (!$appointment || !$appointment->MemberIs(Auth::user())) {
            return back()->withErrors(['Something went wrong. Please try again.']);
        }
        //Check if appointment can be canceled
        if (!$appointment->isConfirmed() || !$appointment->hasNotStarted()) {
            return back()->withErrors(['This consultations cannot be canceled anymore.']);     
        }

        $saved = $appointment->setStatus('canceled');

        if (!$saved) {
            return back()->withErrors(['Something went wrong. Please try again.']);
        } 

        //Queue mail notifications
        $appointment->member->notify(new MemberAppointmentCanceled($appointment,true));
        $appointment->psych->notify(new PsychAppointmentCanceled($appointment));
        

        return back()->with('success', 'Your consultation has been canceled.');
    }
}
