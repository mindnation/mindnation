<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Rules\isValidPassword;
use App\Rules\MatchOldPassword;
use App\User;
use App\UserMeta;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function agreement()
    {
        return view('member.account.agreement');
    }

    public function postagreement()
    {
        $user = Auth::user();
        $user->agreement = Carbon::now()->toDateTimeString();
        $user->save();

        return redirect()->route('member');
    }

    public function onboarding()
    {
        return view('member.account.onboarding');
    }

    public function details()
    {
        return view('member.account.details');
    }

    public function store_details(Request $request)
    {

        // dd($request->all());
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'birth_date' => 'required|date_format:d/m/Y',
            'gender' => 'required',
            'contact_number' => 'required|numeric|digits_between:8,15',
            'emergency_contact_name' => 'required|max:255',
            'emergency_contact_relationship' => 'required|max:255',
            'emergency_contact_number' => 'required|numeric|digits_between:8,15',
            

        ]);

        $user = Auth::user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');

        $user_metas = $request->except(['first_name', 'last_name', '_token']);

        foreach ($user_metas as $key => $value) {
            $user->setMeta($key, $value, 'member');
        }
        $user->save();

        return redirect()->route('member.account.details')->with('success', 'Details Updated');
    }

    public function password()
    {
        return view('member.account.password');
    }

    public function store_password(Request $request)
    {

        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required', new isValidPassword],
            'confirm_new_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        return redirect()->route('member.account.password')->with('success', 'Password successfully changed');
    }

    public function settings()
    {
        return view('member.account.settings');
    }

    public function store_settings(Request $request)
    {
        $user = Auth::user();
        $settings = ['settings_email_notifications'];

        foreach ($settings as $setting) {
            $input = $request->input($setting);

            $user->setMeta($setting, ($input)?true:false, 'member');  // only for checkboxes

            // $meta = $user->getMeta($setting);
            // if (!$meta) {
            //     $meta = new UserMeta();
            //     $meta->key = $setting;
            //     $meta->user()->associate($user->id);
            // }
            // $meta->value = ($input)?true:false;  // only for checkboxes

            // if ($meta->isMemberAllowed()) {
            //     $meta->save();
            // }

        }

        return redirect()->route('member.account.settings')->with('success', 'Settings Updated');
    }
}
