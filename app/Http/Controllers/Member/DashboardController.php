<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        
        $user = Auth::user();
        $company = $user->company;
        $assigned_psychs = array();
        if($company){
            $assigned_psychs = $company->getAvailablePsychs();
        }

        //improve : has manythrough  psychs

        $recommendation_score = array();
        foreach ($assigned_psychs as $psych) {
            $expertises = unserialize($psych->getMetaValue('expertises'));
            $issues = unserialize($user->getMetaValue('issues'));
            if (is_array($expertises) && is_array($issues)) {
                $score = array_intersect($expertises, $issues);
                $recommendation_score[$psych->id] = count($score);
            }
        }
        $recommendations = array();
        if (count($recommendation_score)) {
            $max = max($recommendation_score);
            if ($max && $max > 0) {
                $recommendations = array_keys($recommendation_score, $max);
            }
        }

        $view_data = array(
            'assigned_psychs' => $assigned_psychs,
            'recommendations' => $recommendations,
        );
        return view('member.dashboard', $view_data);
    }
}
