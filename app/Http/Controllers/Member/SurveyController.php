<?php

namespace App\Http\Controllers\Member;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Question;
use App\QuestionCategory;
use App\Survey;
use App\UserAnswer;
use App\UserSurvey;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use Auth;

class SurveyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function onboarding(Request $request)
    {

        
        $user_survey = $this->check_session_survey($request);
        $question = $this->get_next_unanswered_question($request);
        if (!$question) {
            $user = auth()->user();
            $user->onboarding = Carbon::now()->toDateTimeString();
            $user->save();
            return redirect()->route('member');
        }

        $view_data = array(
            'question' => $question,
            //'next' => $question->next(),
            'previous' => $question->previous(),
        );

        // dump($question->id);
        // dump($question->next()->id);
        // dump($question->previous());

        if ($question->type == 'radio') {
             $user_answer = $user_survey->answer_to_question($question->id);
             $view_data['user_answer'] = $user_answer;
        }

        return view('member.survey.onboarding', $view_data);
    }

    public function store(Request $request)
    {

        $user_survey = $this->check_session_survey($request);
        $survey = $user_survey->survey;
        $user = auth()->user();

        // Check if question of answer is part of the current survey
        $question = $survey->questions()->find($request->input('question'));
        if (!$question) {
            return response()->json(['error' => 'An error happened, please try again.'], 422);
        }

        if(!$request->input('previous')){ // handle the previous
        // We prepare the user_answer
        $user_answer = $user_survey->answer_to_question($question->id);
        if (!$user_answer) {
            $user_answer = new UserAnswer();
            $user_answer->user_survey()->associate($user_survey->id);
            $user_answer->question()->associate($question->id);
        }
        switch ($question->type) {
            case 'radio':

                $validator = Validator::make($request->all(), [
                    'answer' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json(['error' => 'Please, select an answer.'], 422);
                }

                // Check if answer ID exist
                $answer = Answer::find($request->input('answer'));
                if (!$answer) {
                    return response()->json(['error' => 'An error happened, please try again.'], 422);
                }
                

                
                $user_answer->answer()->associate($answer->id);
                break;

            case 'issues':

                $validator = Validator::make($request->all(), [
                    'answers' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json(['error' => 'Select an answer'], 422);
                }

                $answers = $request->input('answers');
                
                //can be improved (check if the issues exist)
                $saved_issues = array();
                if($answers){
                    foreach($answers as $key=>$value){
                        if(isset(config('issues')[$key])){
                            $saved_issues[$key] = $value;
                        }
                    }
                }
                $user->setMeta('issues', serialize($saved_issues), 'member');
                break;

            case 'emergency_contact':

                $validator = Validator::make($request->all(), [
                    'emergency_contact_name' => 'required|max:255',
                    'emergency_contact_relationship' => 'required|max:255',
                    'emergency_contact_number' => 'required|digits_between:8,15',
                ]);

                if ($validator->fails()) {
                    $error_array = array();
                    foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                        $error_array[$field_name] = $messages;
                    }
                    return response()->json(['errors' => $error_array], 422);
                }

                $user->setMeta('emergency_contact_name', $request->input('emergency_contact_name'), 'member');
                $user->setMeta('emergency_contact_relationship', $request->input('emergency_contact_relationship'), 'member');
                $user->setMeta('emergency_contact_number', $request->input('emergency_contact_number'), 'member');

                break;
            default:
        }
       
        $user_answer->save();

        
        
        $question_to_return  = $question->next();
            
        }
        else{
            
            $question_to_return = $question->previous();
        }
        
       // $question = $this->get_next_unanswered_question($request);

        if (!$question_to_return) { // No more question
            $user->onboarding = Carbon::now()->toDateTimeString();
            $user->save();
            $output_data = array(
                'success' => 'Answer Saved',
                'redirect' => route('member'),
            );
            return response()->json($output_data);
        }

        $view_data = array(
            'question' => $question_to_return,
            //'next' => $question->next(),
            'previous' => $question_to_return->previous(),
        );
        if ($question->type == 'radio') {
            $user_answer = $user_survey->answer_to_question($question_to_return->id);
            $view_data['user_answer'] = $user_answer;
        }

        $output_data = array(
            'success' => 'Answer Saved',
            'html' => view('member.survey.question', $view_data)->render(),
        );
        return response()->json($output_data);

    }


    public function list(){
        $user = auth()->user();
        $user_surveys = $user->user_surveys()->with('survey')
            ->latest()
            ->get();

        $view_data = array(
            'user' => $user,
            'user_surveys' => $user_surveys,
        );
        return view('member.survey.list', $view_data);
    }

    private function get_next_unanswered_question($request)
    {

        // Todo: not very clean, should be improved for perf
        $user_survey = $this->check_session_survey($request);
        $survey = $user_survey->survey;

        // We parse the questions in order and return the first not answered (we might be able to get that directly with a double joint)
        foreach ($categories = $survey->question_categories()->ordered() as $cat) {
            foreach ($questions = $cat->questions()->ordered() as $question) {
                if (!$user_survey->answer_to_question($question->id)) {
                    return $question;
                }

            }
        }
        //No more question
        
        return null;
    }


    private function get_onboarding_survey(Request $request)
    {
        $survey = Survey::where('onboarding', true)->latest()->first();
        if (!$survey) {
            $survey = $this->create_onboarding_survey_from_config();
        }
        return $survey;
    }

    private function check_session_survey(Request $request)
    {

        $user_survey = $request->session()->get('user_survey');
        if (!$user_survey) {

            $survey = $this->get_onboarding_survey($request);
            $user = auth()->user();
            $user_survey = $user->user_surveys()->where('survey_id', $survey->id)->latest()->first();
            if (!$user_survey) {
                $user_survey = new UserSurvey();
                $user_survey->user()->associate($user->id);
                $user_survey->survey()->associate($survey->id);
                $user_survey->save();
            }

            $request->session()->put('user_survey', $user_survey);
        }
        return $user_survey;
    }

    private function reset_onboarding_survey()
    {
        $survey = Survey::where('onboarding', true);
        if ($survey) {
            $survey->forcedelete();
            $request->session()->forget('survey');
        }
    }

    private function create_onboarding_survey_from_config()
    {
        $config_survey_onboarding = config('surveys.onboarding');

        $survey = new Survey();
        $survey->title = $config_survey_onboarding['title'];
        $survey->onboarding = true;
        $survey->save();

        $i = 0;
        foreach ($config_survey_onboarding['categories'] as $category) {
            $cat = new QuestionCategory();
            $cat->title = $category['title'];
            $cat->order = $i;
            $cat->survey()->associate($survey->id);
            $cat->save();

            $j = 0;
            foreach ($category['questions'] as $question) {
                $q = new Question();
                $q->title = $question['title'];
                $q->type = $question['type'];
                $q->order = $j;
                $q->question_category()->associate($cat->id);
                $q->save();

                $k = 0;
                foreach ($question['answers'] as $answer) {
                    $a = new Answer();
                    $a->title = $answer;
                    $a->order = $k;
                    $a->question()->associate($q->id);
                    $a->save();

                    $k++;
                }
                $j++;
            }
            $i++;
        }

        return $survey;
    }

}
