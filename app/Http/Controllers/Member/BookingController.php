<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\User;
use App\WorkingHour;
use App\Appointment;


use App\Notifications\MemberAppointmentConfirmation;
use App\Notifications\MemberAppointmentReminder;
use App\Notifications\PsychAppointmentConfirmation;
use App\Notifications\PsychAppointmentReminder;



class BookingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(){

    }
    
    public function select_pro(Request $request)
    {
        
        $user = auth()->user();
        $count_ongoing_booking = $user->member_appointments()
                                    ->where('status','confirmed')
                                    ->whereDate('date', '>=', Carbon::now())
                                    ->count();

        //dd($count_ongoing_booking);
        if($count_ongoing_booking >= config('booking.appointment.max_ongoing')){
            return redirect()->route('member.dashboard')->withErrors(['Sorry, you have reached the maximum number of scheduled consultations. <br> You will be able to book again after your next consultation is completed.']);
        }
        $company = $user->company;

        $assigned_psychs = $company->getAvailablePsychs();
        $appointment = $request->session()->get('appointment');
        $preference = null;

        if(isset($appointment->psych)){
            $preference = $appointment->psych->id;
        }

        $view_data = array(
            'assigned_psychs' => $assigned_psychs,
            'preference' => $preference,
        );
        return view('member.booking.select-pro',$view_data);
    }

    public function post_select_pro(Request $request){
        
        $appointment = new Appointment();
        $appointment->member()->associate(Auth::user()->id);
        if($request->input('professional')!= null || $request->input('professional')!='no-preference'){
            $company = Auth::user()->company;
            $psych = User::findPsych($request->input('professional'));
            if($psych && $psych->isAssignedToCompany($company->id)){
                $appointment->psych()->associate($psych->id);
            }
        }
        $request->session()->put('appointment', $appointment);
        return redirect()->route('member.booking.select_slot');
    }

    public function select_slot(Request $request){
        $appointment = $this->check_session_appointment($request);
        $min_date = Carbon::now()->addHours(config('booking.appointment.earliest_allowed', 12));
        $max_date = Carbon::now()->addMonths(config('booking.appointment.latest_allowed', 6));

        $view_data = array(
            'psych' => $appointment->psych,
            'min_date'=> $min_date->toDateString(),
            'max_date'=> $max_date->toDateString(),
        );
        return view('member.booking.select-slot',$view_data);
        //dd(WorkingHour::get_available_slots(Carbon::now()->toDatestring()));
    }

    public function get_slots(Request $request){
        $appointment = $this->check_session_appointment($request);
        $date = $request->get('date');
        //dump($appointment->psych->id);
        $available_slots = WorkingHour::get_available_slots($date, $appointment);
        
        $view_data = array(
            'available_slots' => $available_slots,
        );

        $data = array(
            'available_slots' => view('member.booking.available_slots', $view_data)->render(),
        );
        return json_encode($data); 
    }


    public function post_select_slot(Request $request){
        $appointment = $this->check_session_appointment($request);
        $psych = User::findPsych($request->input('professional'));
        
        // We could create a custom validator
        if(!$request->get('slot')){
            return back()->withErrors(['Please select a Date and a timeslot']);
        }
        if(!$psych || !$request->get('date')){
            return back()->withErrors(['Something went wrong. Please try again.']);
        }
        $appointment->psych()->associate($psych->id);
        $appointment->setTime($request->get('slot'));
        $appointment->setDate($request->get('date'));
     
       
        if(!$appointment->isValid()){
            return back()->withErrors(['Something went wrong. Please try again.']);
        }
        
        $request->session()->put('appointment', $appointment);
        return redirect()->route('member.booking.select_method');
        
    }

    public function select_method(Request $request){
        $appointment = $this->check_session_appointment($request);

        
        $methods = Appointment::getMethodOptions();
        
        if(!isset($appointment->date) || !isset($appointment->start_time)){
            return redirect()->route('member.booking');
        }

        $preference = null;
        if(isset($appointment->method)){
            $preference = $appointment->method;
        }
        $carbon_start_time = $appointment->getCarbonStart();
        $view_data = array(

            'methods' => $methods,
            'preference' => $preference,
        );
        return view('member.booking.select-method',$view_data);
    }

    public function post_select_method(Request $request){
        $appointment = $this->check_session_appointment($request);
        $methods = Appointment::getMethodOptions();
        if(!$request->input('method')){
            return back()->withErrors(['Please select a method.']);
        }
        $appointment->setMethod($request->input('method'),false);
        
        
        $request->session()->put('appointment', $appointment);
        return redirect()->route('member.booking.comment');
    }

    public function comment(Request $request){
        $appointment = $this->check_session_appointment($request);

        $preference = null;
        if(isset($appointment->method)){
            $preference = $appointment->method;
        }
        $carbon_start_time = $appointment->getCarbonStart();
        $view_data = array(
            'appointment' => $appointment,
            'psych_name' => $appointment->psych->name(),
            'date' => $carbon_start_time->toFormattedDateString(), 
            'start_time' => $carbon_start_time->format('g:i A'),

        );
        return view('member.booking.comment',$view_data);
    }

    public function post_comment(Request $request){
        $appointment = $this->check_session_appointment($request);
        
        $appointment->comments = $request->input('comments');
        if(!$appointment->isValid()){
         
            return redirect()->route('member.booking')->withErrors(['Something went wrong. Please try again.']);
        }
        $appointment->setStatusConfirmed();
        $saved = $appointment->save();

        if(!$saved){
            return redirect()->route('member.booking')->withErrors(['Something went wrong. Please try again.']);
        }

        
        // Queue notifications and reminders 
        $appointment->member->notify(new MemberAppointmentConfirmation($appointment));
        $appointment->member->notify(new MemberAppointmentReminder($appointment));
        $appointment->psych->notify(new PsychAppointmentConfirmation($appointment));
        $appointment->psych->notify(new PsychAppointmentReminder($appointment));
           

        $request->session()->forget('appointment');
        return redirect()->route('member.dashboard')->with('success', 'Your consultation has been scheduled. You will receive a confirmation email.');

  
    }


    private function check_session_appointment(Request $request){
        $appointment = $request->session()->get('appointment');
        
        if(!$appointment) {
            return redirect()->route('member.booking');
        }
        else return $appointment;
    }
}
