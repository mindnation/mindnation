<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Appointment;

class MemberAppointmentCompletedMail extends Mailable
{
    use Queueable, SerializesModels;


    public $appointment;

    public $firstCompletedAppointment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment, $firstCompletedAppointment = false)
    {
        $this->appointment = $appointment;
        $this->firstCompletedAppointment = $firstCompletedAppointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->firstCompletedAppointment){
            return $this->subject('First Consultation Completed')
                    ->markdown('emails.memberAppointmentCompletedFirst')
                    ->with('appointment', $this->appointment);
        }

        return $this->subject('Consultation Completed')
                    ->markdown('emails.memberAppointmentCompleted')
                    ->with('appointment', $this->appointment);
    }
}
