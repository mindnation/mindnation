<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Appointment;

class MemberAppointmentConfirmationMail extends Mailable 
{
    use Queueable, SerializesModels;


    public $appointment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Consultation Confirmation')
                    ->markdown('emails.memberAppointmentConfirmation')
                    ->with('appointment', $this->appointment);
    }
}

