<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MemberOneMonthSinceLastSeenMail extends Mailable
{
    use Queueable, SerializesModels;


    protected $notifiable;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notifiable)
    {
        $this->notifiable = $notifiable;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('It\'s been a while...')
                    ->markdown('emails.memberOneMonthSinceLastAppointment')
                    ->with('notifiable', $this->notifiable);
    }
}
