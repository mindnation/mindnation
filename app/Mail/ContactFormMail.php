<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\ContactEnquiry;

class ContactFormMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $enquiry;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactEnquiry $enquiry)
    {
        $this->enquiry = $enquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->markdown('emails.contact');
        //hello@themindnation.com
       // $this->to('hello@themindnation.com');

        return $this->subject('Contact Enquiry - '.$this->enquiry->first_name.' '.$this->enquiry->last_name)
                    ->markdown('emails.contact')
                    ->with('enquiry', $this->enquiry);
    }
}
