<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Appointment;

class MemberAppointmentCanceledMail extends Mailable 
{
    use Queueable, SerializesModels;


    public $appointment;
    public $isUserCanceling;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment, $isUserCanceling)
    {
        $this->appointment = $appointment;
        $this->isUserCanceling = $isUserCanceling;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $date = '['.$this->appointment->getDateTimeString().']';
        
        $subject = 'Consultation Canceled by Professional '.$date;
        if($this->isUserCanceling) $subject = 'Confirmation Canceled Consultation '.$date;
        
        return $this->subject($subject)
                    ->markdown('emails.memberAppointmentCanceled')
                    ->with('appointment', $this->appointment)
                    ->with('isUserCanceling', $this->isUserCanceling);
    }
}