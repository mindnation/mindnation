<?php

namespace App\Providers;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('svg', function($arguments) {
            // Funky madness to accept multiple arguments into the directive
     
            list($path, $class, $preserveAspectRatio) = array_pad(explode(',', trim($arguments, "() ")), 3, '');
            $path = trim($path, "' ");
            $class = trim($class, "' ");
            $preserveAspectRatio = trim($preserveAspectRatio, "' ");
    
            // Create the dom document as per the other answers
            $svg = new \DOMDocument();
            $svg->load(public_path($path));
            $svg->documentElement->setAttribute("class", $class);
            if($preserveAspectRatio){$svg->documentElement->setAttribute("preserveAspectRatio", $preserveAspectRatio);}
            $output = $svg->saveXML($svg->documentElement);

            
    
            return $output;
        });

       
    }
}
