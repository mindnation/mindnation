<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionCategory extends Model
{
    public function survey()
    {
        return $this->belongsTo(Survey::class, 'survey_id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'question_category_id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc')->get();
    }

    public function previous()
    {
        $previous = null;
        foreach ($this->survey->question_categories()->ordered() as $cat) {
            if (!empty($previous && $cat->id == $this->id)) {
                
                return $previous;
            }
            $previous = $cat;
        }
        return null;
    }

    public function next()
    {
        $current = null;
        foreach ($this->survey->question_categories()->ordered() as $cat)
        {
            if(!empty($current)){
                return $cat;
            }
            if($this->id == $cat->id){
                $current = $cat;
            }
        }
        return null;
    }
}
