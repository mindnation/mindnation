<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CsvImport extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
 