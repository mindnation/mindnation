<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = new Role();
        $role->id = 1;
        $role->name = 'super_admin';
        $role->display_name = 'Super Admin';
        $role->save();

        $role = new Role();
        $role->id = 2;
        $role->name = 'admin';
        $role->display_name = 'Admin';
        $role->save();

        $role = new Role();
        $role->id = 3;
        $role->name = 'psychologist';
        $role->display_name = 'Psychologist';
        $role->save();

        $role = new Role();
        $role->id = 4;
        $role->name = 'member';
        $role->display_name = 'Member';
        $role->save();

        
    }
}
