<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\Company;

class AddTestHRUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', 'human_resources')->first();
  
        
        $user = new User();
        $user->first_name = 'HR';
        $user->last_name = 'Me';
        $user->email = 'johndanielvalderama1@gmail.com';
        $user->password = bcrypt('Password01$');
        $user->setAddedBy();
        $user->save();
        $user->roles()->attach($role);
        
        
        
        // $user = new User();
        // $user->first_name = 'John';
        // $user->last_name = 'Psych';
        // $user->email = 'psych@mind.com';
        // $user->password = bcrypt('pass');
        // $user->company()->associate($company->id);
        // $user->setAddedBy();
        // $user->save();
        // $user->makePsych();

        

        // $user = new User();
        // $user->first_name = 'Sam';
        // $user->last_name = 'Member';
        // $user->email = 'member@mind.com';
        // $user->password = bcrypt('pass');
        // $user->company()->associate($company->id);
        // $user->setAddedBy();
        // $user->save();
        // $user->makeMember(); 
        
        // $company = Company::where('name', 'Neslé')->first();
        
        // $user = new User();
        // $user->first_name = 'Regirster';
        // $user->last_name = 'Authorized';
        // $user->email = 'test@stobz.com';
        // $user->company()->associate($company);
        // $user->setAddedBy();
        // $user->save();
        // $user->makeMember(); 
        
        

        // $user = new User();
        // $user->first_name = 'Regirster';
        // $user->last_name = 'Authorized 2';
        // $user->email = 'test2@stobz.com';
        // $user->company()->associate($company->id);
        // $user->setAddedBy();
        // $user->save();
        // $user->makePsych(); 
        
    }
}
