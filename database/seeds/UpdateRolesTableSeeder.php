<?php

use Illuminate\Database\Seeder;
use App\Role;

class UpdateRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = new Role();
        $role->id = 5;
        $role->name = 'human_resources';
        $role->display_name = 'Human Resources';
        $role->save();
        
    }
}
