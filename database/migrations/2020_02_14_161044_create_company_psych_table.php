<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyPsychTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_psych', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            
            $table->timestamps();
        });

        Schema::table('company_psych', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('company_psych', function (Blueprint $table) {
            $table->dropForeign('company_psych_user_id_foreign');
            $table->dropForeign('company_psych_company_id_foreign');
        });

        Schema::dropIfExists('company_psych');
    }
}
