<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAuthorizedGoogleSheetsTableWithExecuted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('authorized_google_sheets', function (Blueprint $table) {
            $table->boolean('is_executed');
            $table->string('integration_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('authorized_google_sheets', function (Blueprint $table) {
            $table->boolean('is_executed');
            $table->string('integration_type');
        });
    }
}
