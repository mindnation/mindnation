<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCsvImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_imports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('data');
            $table->string('filename')->nullable();
            $table->longText('comments')->nullable();
            $table->timestamp('imported_at')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('csv_imports', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('csv_imports', function (Blueprint $table) {
            $table->dropForeign('csv_imports_user_id_foreign');
            $table->dropForeign('csv_imports_company_id_foreign');
      });
        Schema::dropIfExists('csv_imports');
    }
}
