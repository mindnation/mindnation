<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_hours', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('psych_id')->unsigned()->nullable();
            $table->foreign('psych_id')->references('id')->on('users');
            $table->date('date')->nullable();
            $table->time('start_time');
            $table->time('finish_time')->nullable();
            $table->timestamps();
            //$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('working_hours', function (Blueprint $table) {
            $table->dropForeign('working_hours_psych_id_foreign');
        });
        Schema::dropIfExists('working_hours');
    }
}
