<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id')->unsigned()->nullable();
            $table->foreign('member_id')->references('id')->on('users');
            $table->bigInteger('psych_id')->unsigned()->nullable();
            $table->foreign('psych_id')->references('id')->on('users');
            $table->date('date')->nullable();
            $table->time('start_time');
            $table->time('finish_time')->nullable();
            $table->text('comments')->nullable();
            $table->string('method')->nullable();
            $table->string('status')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('appointments', function (Blueprint $table) {
            $table->dropForeign('appointments_psych_id_foreign');
            $table->dropForeign('appointments_member_id_foreign');
        });
        Schema::dropIfExists('appointments');
    }
}
