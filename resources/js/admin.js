
// require('./app');


$(document).ready(function () {

    var assignedCompaniesList = $('#assigned_companies_list');

   //var adminForm = $('.admin-form');
    $(document).on('submit', '.admin-form', function (event) {
        event.preventDefault();
        var parent_modal = $(this).parents('.modal');
        var form = $(this);
        var form_data = $(this).serialize();
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var response_div = $(this).find('.form-response');

        form.find('.invalid-feedback').remove();
        form.find('.is-invalid').removeClass('is-invalid');
        var submitBtn = form.find('input[type=submit]');
        submitBtn.prop('disabled', true);

        $.ajax({
            url: url,
            method: method,
            data: form_data,
            dataType: "json",
            success: function (data) {
                
                if (typeof (data.success) != "undefined" && data.success == true) {
                    if (typeof (data.message) != "undefined" && data.message !== null){
                        response_div.html('<div class="callout callout-success text-success border-success">' + data.message + '</div>');
                    }

                

                    setTimeout(function () {
                        parent_modal.modal('hide');
                        reloadDatatable();
                        resetForm(form);
                        submitBtn.prop('disabled', false);

                    }, 1000);
                }
                
            },
            error: function (xhr, status, error) {
                var errors = xhr.responseJSON;
                console.log(errors);
                $.each(errors.errors, function (key, value) {
                    var field = form.find('[name="' + key + '"]');
                    field.addClass('is-invalid');
                    field.parent('.form-group').append('<div class="invalid-feedback text-left">' + value + '</div>');
                });
                
                if (typeof (errors.exception) != "undefined" && errors.exception !== null) {
                    response_div.html('<div class="callout callout-danger text-danger border-danger">Something went wrong.<br>Reload the page and try again.</div>').show();
                }
                if (typeof (errors.errors.message) != "undefined" && errors.errors.message !== null) {
                    response_div.html('<div class="callout callout-danger text-danger border-danger">'+errors.errors.message+'</div>').show();
                }
                submitBtn.prop('disabled', false);
            },
        });
    });



    function reloadDatatable() {
        table = $('.dataTable');
        table.dataTable();
        table.fnDraw(false);
    }


    function resetForm(form) {
        form.trigger("reset");
        form.find('input[type=hidden]').not('input[name=_token]').val('');
        form.find('.form-response').empty();
        form.find('input[name=email]').prop('disabled',false);
        
        form.find('.invalid-feedback').remove();
        form.find('.is-invalid').removeClass('is-invalid');
        assignedCompaniesList.empty(); //for psychs

        $(this).parents('modal').find('.overlay').attr('style', 'display:flex !important');
    }

    $(document).on('hidden.bs.modal', '.admin-delete-modal, .admin-edit-form', function (e) {

        var form = $(e.target).find('.admin-form');
        resetForm(form);
    });

    // $(document).on('hidden.bs.modal', '#EditFormModal', function (e) {
    //     alert('closing');
    //     var form = $(e.target).find('.admin-form');
    //     resetForm(form);
    // });




    $(document).on('click', '.datatable-edit-button', function (e) {
        // e.preventDefault();

        var target = $(this).data('target');
        var modal = $(target);
        var action = $(this).data('action');
        modal.find('.modal-title').html($(this).data('modal-title'));
        var response_div = $('.response-div');

        if (action == 'add') {
            modal.find('.overlay').attr('style', 'display:none !important');
        }
        else if (action == 'edit') {
            var url = $(this).data('get-data-url');
            var id = $(this).data('entry-id');
            var form = modal.find('.admin-form');
            $.ajax({
                url: url,
                method: 'GET',
                data: { id: id },
                dataType: "json",
                success: function (data) {
                    if (typeof (data.user) != "undefined" && data.user != null ) {  
                        populate(form, data.user);
                        
                        form.find('input[name=email]').prop('disabled',true);

                        if (typeof (data.user.company_id) != "undefined" && data.user.company_id != null ) {  
                            form.find('select#company').val(data.user.company_id);
                        }
                    }

                    if (typeof (data.company) != "undefined" && data.company != null ) {  
                        populate(form, data.company);
                    }
                    
                    if (typeof (data.assigned_companies) != "undefined" && data.assigned_companies != null ) {   
                        assignedCompaniesList.html(data.assigned_companies);
                    }

                    $(target).modal('show');
                    $(target).find('.overlay').attr('style', 'display:none !important');


                },
                error: function (xhr, status, error) {
                    var errors = xhr.responseJSON;
                    
                    if (typeof (errors.exception) != "undefined" && errors.exception !== null) {
                        response_div.html('<div class="text-danger">Something went wrong.<br>Reload the page and try again.</div>').show();
                    }
                    if (typeof (errors.message) != "undefined" && errors.message !== null) {
                        response_div.html('<div class="text-danger">'+errors.message+'</div>').show();
                    }
                },
            });
        }

    });





    function populate(frm, data) {
        $.each(data, function (key, value) {
            var ctrl = $('[name=' + key + ']', frm);
            switch (ctrl.prop("type")) {
                case "radio": case "checkbox":
                    ctrl.each(function () {
                        if ($(this).attr('value') == value) $(this).attr("checked", value);
                    });
                    break;
                default:
                    ctrl.val(value);
            }
        });
    }




});

