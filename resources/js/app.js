/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
//global.$ = global.jQuery = require('jquery');
require('./bootstrap');
require('moment');


require('icheck');

require('../../public/bower_components/datatables.net/js/jquery.dataTables.min.js');
require('../../public/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
require('../../public/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js');
require('../../public/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');

require('eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js');

require('../../public/bower_components/admin-lte/dist/js/adminlte.min.js');

require('../../public/bower_components/typeahead.js/dist/typeahead.bundle.min.js');


import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import moment from 'moment';


global.Calendar = Calendar;
global.dayGridPlugin = dayGridPlugin;
global.bootstrapPlugin = bootstrapPlugin;
global.timeGridPlugin = timeGridPlugin;
global.listPlugin = listPlugin;
global.moment = moment;

moment.locale('en', {
    week: { dow: 1 }
});




$(document).ready(function () {
    // Check Auth
    setInterval(function () {
        checkLogin();
    }, 10000);

    loadICheck();



    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });




});





global.loadICheck = function () {


    $('input.radio-square, input.checkbox-square').each(function () {
        var self = $(this),
            dataColor = self.data('color'),
            color = '';
        if (typeof (dataColor) != "undefined" && dataColor !== null) {
            color = dataColor;
        }

        self.iCheck({


            checkboxClass: 'icheckbox_square-purple ' + color,
            radioClass: 'iradio_square-purple ' + color,
            //increaseArea: '20%' // optional
        });
    });

    $('input.radio-line, input.checkbox-line').each(function () {
        var self = $(this),
            label = self.next(),
            label_text = label.text(),
            dataColor = self.data('color'),
            color = '';
        if (typeof (dataColor) != "undefined" && dataColor !== null) {
            color = dataColor;
        }



        label.remove();
        self.iCheck({
            checkboxClass: 'icheckbox_line-purple  ' + color,
            radioClass: 'iradio_line-purple  ' + color,
            insert: '<div class="icheck_line-icon"></div>' + label_text
        });
    });
}
function checkLogin() {
    $.ajax({
        url: "/auth/check",
        method: 'GET',
        dataType: "json",
        success: function (response) {
            if (!response) {
                //alert('user Logged out');
                window.location.replace("/");
            }
        }
    });
}




//Account nav
$(document).ready(accountNavMinHeight);
$(window).on('resize',accountNavMinHeight);

function accountNavMinHeight() {
    var accountNav = $('#account_sidebar');
    if (accountNav) {
        $('.content-col').css('min-height', accountNav.find('aside').outerHeight() + 'px');
    }
}


$('.sidebar-col').click(function (e) {
    if (!$(e.target).hasClass('nav-link')) {
        $(this).toggleClass('opened');
    }

});
$('.content-col').click(function () {
    if ($('.sidebar-col').hasClass('opened')) {
        $('.sidebar-col').removeClass('opened');
    }
});

// $('.account-nav-toggler').click(function(){
//   $(this).parents('.sidebar-col').toggleClass('opened');
// });


