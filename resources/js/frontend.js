
global.$ = global.jQuery = require('jquery');
require('moment');
require('./bootstrap');
import moment from 'moment';
global.moment = moment;
require('eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js');



$(document).ready(function () {

    checkHash();
    
    $('select[name="gender"]').change(function(){
        if($(this).val() == ''){
            $(this).addClass('placeholder');
        }
        else{
            $(this).removeClass('placeholder');
        }
    });
    $('.contact-form').submit(function (e) {
        e.preventDefault();
        var parent_modal = $(this).parents('.modal');
        var form = $(this);
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var response_div = $(this).find('.form-response');
        var form_data = $(this).serialize();
        form.find('.invalid-feedback').remove();
        form.find('.is-invalid').removeClass('is-invalid');
        response_div.hide();

        var loading = form.find('.loading');
        loading.show();
        var submitBtn = form.find('input[type=submit]');
        submitBtn.prop('disabled', true);


        $.ajax({
            url: url,
            method: method,
            data: form_data,
            dataType: "json",
            success: function (data) {
                $.each(data.errors, function (key, value) {
                    var field = form.find('[name="' + key + '"]');
                    field.addClass('is-invalid');
                    field.parent('.form-group').append('<div class="invalid-feedback text-left">' + value + '</div>');
                });

                if (typeof (data.success) != "undefined" && data.success !== null) {
                    response_div.text(data.success).show();
                    form.trigger("reset");
                    setTimeout(function () {
                        parent_modal.modal('hide');
                        
                    }, 3000);

                }
                submitBtn.prop('disabled', false);
                loading.hide();

            }
        });
    });


    $('.auth-form').submit(function (e) {
        e.preventDefault();
        var parent_modal = $(this).parents('.modal');
        var form = $(this);
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var response_div = $(this).find('.form-response');
        var form_data = $(this).serialize();
        form.find('.invalid-feedback').remove();
        form.find('.is-invalid').removeClass('is-invalid');
        response_div.hide();
        var submitBtn = form.find('input[type=submit]');
        submitBtn.prop('disabled', true);


        $.ajax({
            url: url,
            method: method,
            data: form_data,
            dataType: "json",
            success: function (data) {
                $.each(data.errors, function (key, value) {
                    var field = form.find('[name="' + key + '"]');
                    field.addClass('is-invalid');
                    field.parent('.form-group').append('<div class="invalid-feedback text-left">' + value + '</div>');
                });
                if (typeof (data.error) != "undefined" && data.error !== null) {
                    response_div.html('<div class="text-danger">' + data.error + '</div>').show();
                }
                if (typeof (data.success) != "undefined" && data.success == true) {
                    
                    if (typeof (data.success) != "undefined" && data.message !== null) {
                        response_div.text(data.message).show();
                    }
                    if (data.redirect) {
                        window.location.href = data.redirect;
                    }

                    setTimeout(function () {

                        form.trigger("reset");
                    }, 3000);

                }
                submitBtn.prop('disabled', false);

            },
            error: function (xhr, status, error) {
                var errors = xhr.responseJSON;
                if (typeof (errors.errors) != "undefined" && errors.exception !== null){
                    $.each(errors.errors, function (key, value) {
                        var field = form.find('[name="' + key + '"]');
                        field.addClass('is-invalid');
                        field.parent('.form-group').append('<div class="invalid-feedback text-left">' + value + '</div>');
                    });
                }
                
                if (typeof (errors.exception) != "undefined" && errors.exception !== true) {
                    response_div.html('<div class="text-danger">Something went wrong.<br>Reload the page and try again.</div>').show();
                }
                submitBtn.prop('disabled', false);
            },

        });
    });


    $('#loginCollapse').on('hidden.bs.collapse', function () {
        var form = $('.auth-form');
        form.find('.invalid-feedback').remove();
        form.find('.is-invalid').removeClass('is-invalid');
      })


    $('[data-trigger="auth-div"]').click(function (e) {
        e.preventDefault();
        showAuthDiv($(this).data('target'));
    });

    function showAuthDiv(id) {
        $('.auth-div').hide();
        $(id).show();
    }

    $('[data-trigger="auth"]').click(function (e) {
        e.preventDefault();
        showAuthDiv($(this).data('target'));
        // $([document.documentElement, document.body]).animate({
        //     scrollTop: 0,
        // }, 1000);
        $('#mainCollapse').collapse('hide');
        $('#loginCollapse').collapse('show').on('shown.bs.collapse', function () {
            $(window).scrollTop(0);
        });


    });


    $('a.hash-link').click(function (e) {
        if ($(this).attr('href').indexOf('http') < 0) {
          e.preventDefault();
          var hash = $(this).attr('href');
          navigateToAnchor(hash);
        }

      });

});

function checkHash() {
    removeLocationHash();
    // var hash = window.location.hash;
    // navigateToAnchor(hash);
}


function navigateToAnchor(hash) {
    removeLocationHash();
    if ($(hash).length) {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(hash).offset().top,
        }, 1500);
        return false;
    }
    
}
function removeLocationHash() {
    var noHashURL = window.location.href.replace(/#.*$/, '');
    window.history.replaceState('', document.title, noHashURL)
}