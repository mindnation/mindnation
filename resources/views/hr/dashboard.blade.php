@extends('layouts.hr')

@php $page_title = "Dashboard"; @endphp

@section('title', $page_title)

@section('content')

<!-- Main content -->
<div class="content pt-4 pb-6 py-md-7">
    <div class="container">
        <div class="h2 text-purple text-center infront-of-bg-shape ">
            Dashboard for HR
        </div>
    </div>
</div><!-- /.container-fluid -->

<!-- /.content -->
@endsection