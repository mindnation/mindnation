<footer class="main-footer border-multicolor-bottom pt-4 pb-4 shadow-lg">
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-12 text-center font-weight-light font-size-sm">
                If you are in a crisis or any other person may be in danger - don’t use this site.<br>
These resources can provide you with immediate help.
            </div>
        </div>
        <ul class="nav justify-content-center font-size-sm">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('front.privacy-policy') }}" target="_blank">Privacy Policy</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('front.terms') }}" target="_blank">Terms of Use</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">© 2020 MindNation</a>
            </li>
        </ul>
    </div>
</footer>