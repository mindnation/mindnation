<nav class="main-header navbar navbar-expand navbar-light navbar-white">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('member')}}">
            <img src="{{ asset('images/logo/mindnation-logo.png') }}" class="logo" alt="MindNation Logo">
        </a>
        <a class="dashboard-name d-none d-md-block" href="{{ route('hr')}}">
            HR Dashboard
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
  
        </button>

        @if($company = Auth::user()->company()->first())
                <div class="company-name d-none d-sm-block">{{ $company->name }}</div>
        @endif

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                
                @include('partials.user-dropdown')
                
            </ul>
        </div>
    </div>
</nav>