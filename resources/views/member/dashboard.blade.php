@extends('layouts.member')

@php $page_title = "Dashboard"; @endphp

@section('title', $page_title)

@section('content')



<!-- Main content -->
<div class="content pt-4 pb-6 py-md-7">
    <div class="container">
        @if ($message = Session::get('success'))
        <div class="callout callout-success border-success text-success text-center infront-of-bg-shape bg-transparent">
            <div>{{ $message }}</div>
        </div>
        @endif
        @if ($errors->any())
        <div class="callout callout-danger text-danger mb-5 border-danger text-center infront-of-bg-shape bg-transparent">
            {{--  Something is missing. Please check and try again.  --}}
            @foreach ($errors->all() as $error)
                <div>{!! $error !!}</div>
            @endforeach 
        </div>
        @endif 

        <div class="h2 text-purple text-center infront-of-bg-shape ">
            Our
            @if($company = Auth::user()->company()->first())
            {{ $company->name }}
            @endif
            team is ready to help.
        </div>
        

           
                <div class="row justify-content-center eq-height psych-card-list mb-2">
                    @forelse($assigned_psychs as $psych)
                    <div class="col py-4 ">
                        <div class="card h-100 text-center shadow-lg">
                            
                            <div class="card-body px-2">
                                @if(in_array($psych->id, $recommendations))
                                    @php $recommended_text = "We recommend this psychologist based on the issues you mentioned during the onboarding quiz"; @endphp
                                    <span class="recommended-badge badge-pill badge-teal h6 text-white d-inline-block text-uppercase py-2 px-3" data-toggle="tooltip" data-placement="top" title="{{ $recommended_text }}">Recommended</span>
                                @endif
                                <div class="photo">
                                    @if($avatar = $psych->avatar)
                                        <img id="avatar" class="w-100 " src="{{asset('storage/'.$avatar->filename)}}">
                                    @endif
                                </div>
                                <div class="h6 mt-3 mb-2">{{$psych->full_name()}}</div>
                                @if($timeslots = $psych->getMetaValue('timeslots'))
                                <div class=" font-weight-light font-size-md">
                                    <div class="font-weight-bold">
                                        Consultation Hours:
                                    </div>
                                    {!! nl2br($timeslots) !!}
                                </div>
                                @endif


                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-yellow btn-sm btn-aboutme" data-toggle="modal"
                                    data-target="#ProInfoModal-{{ $psych->id }}">
                                    About Me
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="ProInfoModal-{{ $psych->id }}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true" >
                                    <div class="modal-dialog modal-dialog-centered " role="document">
                                        <div class="modal-content shadow-lg">
                                            <div class="modal-header border-0 p-4">
                                                <h5 class="modal-title" id="ProInfoModal-{{ $psych->id }}Label">
                                                    {{$psych->full_name()}}</h5>
                                                <button type="button" class="close p-2" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-left px-4 pt-0 font-weight-light font-size-md">
                                                <div class="mb-3">
                                                    {!! nl2br($psych->getMetaValue('short_background')) !!}
                                                </div>
                                                
                                                    @if($expertises = unserialize($psych->getMetaValue('expertises')))
                                                    <div class="mb-3">
                                                    <span class="h6 font-size-md">Expertise:</span> {!! implode(', ',$expertises) !!}
                                                    </div>
                                                    @endif
                                                    
                                                
                                                <div class="mb-3">
                                                    <span class="h6 font-size-md">Contact Info:</span> {{$psych->email}}
                                                </div>
                                                <div class="">
                                                    <span class="h6 font-size-md">Consultation Hours:</span> {{$psych->getMetaValue('timeslots')}}
                                                </div>
                                            </div>
                                            <div class="modal-footer text-center border-0 py-3">
                                                <button type="button" class="btn btn-blue btn-sm mx-auto" data-dismiss="modal">Go
                                                    Back</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>               
                    @empty
                    <div class="">
                        <div class="text-center h5 pt-4 pb-3">
                            No professional available. 
                        </div>
                    </div>
                    
                    @endforelse
                </div>
            

        <div class="text-center infront-of-bg-shape">
            <div class="h3 mb-3 text-purple">Tell us how your day is going.</div>
            <a href="{{route('member.booking')}}" class="btn btn-purple btn-lg btn-min-width-lg">Schedule
                Consultation</a>
        </div>

    </div>
</div><!-- /.container-fluid -->

<!-- /.content -->
@endsection