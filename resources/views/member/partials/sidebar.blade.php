<!-- Main Sidebar Container -->
<aside class="sidebar-light-primary">

    
    
    <div class="nav-header h5 py-3 border-bottom text-center text-secondary text-uppercase">My Account</div>

    <!-- Sidebar -->
    <div class="sidebar">

       

        <!-- Sidebar Menu -->
        <nav class="my-3 overflow-hidden">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    @php $route = 'member.account.details'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>{{ __('My Details') }}</p>
                    </a>
                </li>

                <li class="nav-item">
                    @php $route = 'member.account.password'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-key"></i>
                        <p>{{ __('Change Password') }}</p>
                    </a>
                </li>

                <li class="nav-item">
                    @php $route = 'member.account.appointments'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-comment-medical"></i>
                        <p>{{ __('My Consultations') }}</p>
                    </a>
                </li>

                <li class="nav-item">
                    @php $route = 'member.account.surveys'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-notes-medical"></i>
                        <p>{{ __('My Assessments') }}</p>
                    </a>
                </li>

                <li class="nav-item">
                    @php $route = 'member.account.settings'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>{{ __('Settings') }}</p>
                    </a>
                </li>

                


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

