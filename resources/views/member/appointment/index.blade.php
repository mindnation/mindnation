@extends('layouts.member_account')

@php $page_title = "My Consultations | Account"; @endphp

@section('title', $page_title)

@section('content')


<!-- Main content -->
<div class="content">
    <div class="container-fluid px-2 px-md-3">

        <div class="card m-0 shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">My Consultations</div>

                @if ($message = Session::get('success'))
                    <div class="callout callout-success text-success border-success py-2">
                        <div>{{ $message }}</div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif
                <div class="table-responsive-wrapper">
                <table class="table  table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Professional</th>
                            <th class="text-center">Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($appointments as $appointment)
                        @php
                        $carbon_start_time = $appointment->getCarbonStart();
                        @endphp
                        <tr>
                            <td>{{$carbon_start_time->format('d/m/Y')}}</td>
                            <td>{{$carbon_start_time->format('g:i A')}}</td>
                            <td>{{$appointment->psych->full_name()}}</td>
                            <td class="text-center">
                                @include('partials.appointment-status',['appointment'=>$appointment])
                            </td>
                            <td>
                                @if($appointment->isConfirmed() && $appointment->hasNotFinished() )
                                    @if($appointment->linkIsVisible())
                                        <a href="{{ $appointment->psych->getMetaValue('doxyme_url') }}" target="_blank" class="btn btn-xs btn-purple px-2 text-nowrap">Enter Waiting Room</a>
                                    @else
                                        <div target="_blank" class="btn btn-xs btn-purple px-2 disabled text-nowrap" data-toggle="tooltip" data-placement="top"
                                        title="The Waiting Room link will be available 2 hours before the consultation">Enter Waiting Room</div>
                                    @endif

                                    {{-- @if($appointment->hasNotStarted()) --}}
                                    <div class="btn btn-xs btn-orange px-2" data-toggle="modal" data-target="#cancelModal-{{ $appointment->id}}">Cancel</div>
                                    

                                    <div id="cancelModal-{{ $appointment->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                {{ Form::open(array('route' => 'member.account.appointments.cancel')) }}
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cancel Consultation</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure to cancel the following consultation?</p>
                                                    <div>
                                                        <label>Date:</label> {{$carbon_start_time->format('d/m/Y')}}
                                                    </div>
                                                    <div>
                                                        <label>Time:</label> {{$carbon_start_time->format('g:i A')}}
                                                    </div>
                                                    <div>
                                                        <label>Professional:</label> {{$appointment->psych->full_name()}}
                                                    </div>



                                                    {{ Form::hidden('id', $appointment->id) }}
                                                </div>
                                                <div class="modal-footer">
                                                    
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                    {!! Form::submit("Cancel Consultation", [ 'class' => 'btn btn-danger']) !!}
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @endif --}}
                                    
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">No consultations yet</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

            </div>

        </div>



    </div><!-- /.container -->
</div>
<!-- /.content -->
@endsection