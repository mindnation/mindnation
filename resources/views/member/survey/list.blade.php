@extends('layouts.member_account')

@php $page_title = "My Assessments | Account"; @endphp

@section('title', $page_title)

@section('content')


<!-- Main content -->
<div class="content">
    <div class="container-fluid px-2 px-md-3">

        <div class="card m-0 shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">My Assessments</div>

                {{--  @if ($message = Session::get('success'))
                    <div class="callout callout-success text-success border-success py-2">
                        <div>{{ $message }}
            </div>
        </div>
        @endif

        @if ($errors->any())
        <div class="callout callout-danger text-danger border-danger  py-2">
            @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
            @endforeach
        </div>
        @endif --}}
        <div class="table-responsive-wrapper">
            <table class="table  table-striped">
            <thead>
                <tr>
                    <th>Test taken</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
                @forelse ($user_surveys as $user_survey)
                @php
                $carbon_date = $user_survey->getCarbonDate();
                @endphp
                <tr>
                    <td>{{ $user_survey->survey->title }}</td>
                    <td>{{$carbon_date->format('d/m/Y')}}</td>
                    <td>{{$carbon_date->format('g:i A')}}</td>
                    <td>
                        <div class="btn btn-xs btn-info px-2" data-toggle="modal"
                            data-target="#UserSurveyModal-{{ $user_survey->id }}">View</div>

                        <div class="modal fade" id="UserSurveyModal-{{ $user_survey->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="UserSurveyModal-{{ $user_survey->id }}-Label" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header px-4 py-3">
                                        <h4 class="modal-title" id="UserSurveyModal-{{ $user_survey->id }}-Label">
                                            {{ $user_survey->survey->title }}</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body px-5">

                                        <div class="mb-5">
                                            <label>Taken on:</label> {{$carbon_date->format('d/m/Y')}}
                                        </div>

                                        @php
                                        $survey = $user_survey->survey;
                                        $user_answer = $user_survey->user_answers()->get();
                                        @endphp

                                        @foreach ($survey->question_categories()->ordered() as $category)
                                        <div class="mb-5">
                                            <div
                                                class="mb-3 badge-pill badge-yellow d-inline-block text-white py-2 px-4">
                                                <div class="h6 text-white m-0">{{$category->title}}</div>
                                            </div>
                                            @foreach ($category->questions()->ordered() as $question)
                                            @if($question)
                                            <div class="mb-3">

                                                @switch($question->type)
                                                @case('radio')
                                                <span class="h6">- {{$question->title}}</span>
                                                @php $answer = $user_survey->user_answers()->where('question_id',$question->id)->first()->answer;
                                                @endphp
                                                @if($answer)
                                                <span class="text-teal ml-1">{{ $answer->title }}</span>
                                                @endif
                                                @break
                                                @case('issues')
                                                <span class="h6">- {{$question->title}}</span>
                                                @php
                                                $user_issues = unserialize($user->getMetaValue('issues'));
                                                @endphp
                                                <span class="text-teal">{{ implode(", ",$user_issues) }}</span>
                                                @break
                                                @case('emergency_contact')
                                                <div class="row pb-3 pb-md-0">
                                                    <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Name:</div>
                                                    <div class="col-12 col-md-8 col-lg-9">{{$user->getMetaValue('emergency_contact_name')}}
                                                    </div>
                                                </div>
                                                <div class="row pb-3 pb-md-0">
                                                    <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Relationship:</div>
                                                    <div class="col-12 col-md-8 col-lg-9">
                                                        {{$user->getMetaValue('emergency_contact_relationship')}}</div>
                                                </div>
                                                <div class="row pb-3 pb-md-0">
                                                    <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Contact Number:</div>
                                                    <div class="col-12 col-md-8 col-lg-9">
                                                        {{$user->getMetaValue('emergency_contact_number')}}</div>
                                                </div>
                                                @break
                                                @default

                                                @endswitch
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5">No Assessments yet</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        </div>

    </div>

</div>



</div><!-- /.container -->
</div>
<!-- /.content -->
@endsection