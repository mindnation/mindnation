@extends('layouts.member', ['no_sidebar' => true])

@php $page_title = "Onboarding"; @endphp

@section('title', $page_title)


@section('content')
<div class="content">
    <div class="container px-1 px-md-3 py-5 py-md-7">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-11 ">
                {{ Form::open(array('route' => 'member.onboarding.survey.store', 'id' => 'onboarding_survey_form')) }}
                
                <div id="survey_question">
                    @include('member.survey.question', ['question' => $question, 'user_answer' => $user_answer ?? null])
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<!-- Main content -->

@endsection

@push('scripts')
<script>

    $(document).on('click', '.survey-previous-question', function(){
        
        $('<input>').attr({
            type: 'hidden',
            name: 'previous',
            value: '1',
        }).appendTo('#survey_question');
        $('#onboarding_survey_form').submit();
    });
    $('#onboarding_survey_form').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var response_div = $(this).find('.form-response');
        var form_data = $(this).serialize();
        var question_div = $(this).find('#survey_question');
 
         form.find('.invalid-feedback').remove();
         form.find('.is-invalid').removeClass('is-invalid');
        response_div.hide();


        $.ajax({
            url: url,
            method: method,
            data: form_data,
            dataType: "json",
            success: function (data) {
                
                
                if (typeof (data.success) != "undefined" && data.success) {
                    // response_div.text(data.success).show();
                    if (data.redirect) {
                        window.location.href = data.redirect;
                    }
                    if(data.html){
                        question_div.html(data.html);
                    }
                }
                loadICheck();

            },
            error: function (xhr, status, error) {
                var data = xhr.responseJSON;
                if (typeof (data.error) != "undefined" && data.error !== null) {
                    response_div.html('<div class="text-danger">' + data.error + '</div>').show();
                }
                $.each(data.errors, function (key, value) {
                    var field = form.find('[name="' + key + '"]');
                    field.addClass('is-invalid');
                    field.parent('.form-group').append('<div class="invalid-feedback text-left">' + value + '</div>');
                });
            },

        });
    });
</script>
@endpush