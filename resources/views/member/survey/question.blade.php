<div class="badge-pill badge-yellow h5 py-3 px-4 d-inline-block mb-3 text-black font-weight-light text-center text-md-left">
    {{ $question->question_category->title }}
</div>

<div class="card survey-question-card">
    <div class="card-body py-0">
        <div class="row justify-content-center align-items-lg-center">

            <div class="col-12 col-md-6 pt-5 px-4  pr-md-3 px-lg-5 pb-0 pb-md-5">
                <div class="text-purple h1 m-0 question-title text-center text-md-left">{{ $question->title }}</div>
            </div>
            <div class="col-12 col-md-6 py-4">
                <div class="form-response"></div>
                <div >

                    {{ Form::hidden('question', $question->id) }}
                    
                    @switch($question->type)
                    @case('radio')
                    <div class="pt-3 container-fluid px-0">
                        @foreach ($question->answers()->ordered() as $answer)
                        @php $value = null; @endphp
                        @if($user_answer && $user_answer->answer_id == $answer->id)
                            @php $value = true; @endphp
                        @endif
                        <div class="form-group row mb-2 mb-md-3">
                            <div class="col-2 pr-1 text-right">
                                {{ Form::radio('answer', $answer->id, $value, ['id' => 'answer-'.$answer->id, 'class' => 'radio-square']) }}
                            </div>
                            {{ Form::label('answer-'.$answer->id, $answer->title, ['class' => 'col-10 px-2']) }}
                        </div>
                        @endforeach
                    </div>
                        
                    @break

                    @case('issues')

                        @php 
                            $user_issues = unserialize(auth()->user()->getMetaValue('issues'));
                        @endphp
                        <div class="row px-3">   
                            @foreach (config('issues') as $key=>$value)
                            <div class="col-6 p-1">
                                {{ Form::checkbox('answers['.$key.']', $value, isset($user_issues[$key])?true:null, ['id' => $key, 'class' => 'checkbox-line']) }}
                                {{ Form::label($key, $value) }}
                            </div>
                            @endforeach
                        </div>
                        
                    @break

                    @case('emergency_contact')
                    
                        <div class="form-group">
                            @php $field = 'emergency_contact_name'; @endphp
                            {{ Form::label($field, 'Name') }}
                            {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                        </div>
                        <div class="form-group">
                            @php $field = 'emergency_contact_relationship'; @endphp
                            {{ Form::label($field, 'Relationship') }}
                            {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                        </div>
                        <div class="form-group">
                            @php $field = 'emergency_contact_number'; @endphp
                            {{ Form::label($field, 'Contact Number') }}
                            {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                        </div>
                        
                    @break

                    @default

                    @endswitch



                </div>
            </div>
        </div>

    </div>
</div>

@if($previous)
    <div class="btn btn-yellow survey-previous-question">Previous</div>
    
@endif
{!! Form::submit('Next', [ 'class' => 'btn btn-purple  float-right']) !!}