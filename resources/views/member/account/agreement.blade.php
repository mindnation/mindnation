@extends('layouts.member', ['no_sidebar' => true])

@php $page_title = "Onboarding"; @endphp

@section('title', $page_title)
@section('content')
<div class="content ">
    <div class="container px-1 px-md-3 py-4 py-md-7 ">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-lg-9">

                <div class="card shadow-lg m-0">
                    <div class="card-body pt-5 px-4">
                        {{ Form::open(array('route' => 'member.agreement')) }}
                        <div class="text-center">
                            <h1 class="h4 text-blue mb-5">Consent Agreement</h1>

                            <div class="text-center mb-4">To continue with the platform, you must agree to the terms of
                                the consent agreement.</div>

                            <div id="agreement_wrapper" class="p-3 bg-light border border-gray-light text-left">
                                <div class="font-size-sm" >
                                    <p>
                                        This Consent will provide you information about your online mental health
                                        session, which will help you understand what it is about and how to proceed.
                                        Your session is using the following Google Hangouts, Doxy or Viber / Whatsapp /
                                        Telegram. You will need access to internet service and technological tools
                                        needed to have your mental health session.
                                        You can stop the session at any time without prejudice.
                                        Please understand that you shall not proceed with the consultation if you are in
                                        a crisis and are in imminent danger to yourself or others.
                                        Please proceed to the nearest emergency room instead.
                                    </p>
                                    <p>
                                        Your therapist follows security best practices and legal standards in order to
                                        protect your health care information through notes of your sessions, but you
                                        will also need to participate in maintaining your own security and privacy.
                                        The personal information, sensitive personal information, and privileged
                                        information (collectively, “personal data”) that you have disclosed or will
                                        disclose to us or your therapist will be used and temporarily stored for the
                                        purpose of your consultation.
                                        Certain anonymous data may be retained longer for purely statistical and
                                        research purposes.
                                        Your personal data may be transferred to other therapists if the current
                                        therapist is not available for the schedule, vacation or on leave.
                                        Your data privacy is all protected and the full session is all confidential.
                                        Please note that a limited number of people in your company might know the basic
                                        information with your name and schedule.
                                        Please read our Data Privacy Policy at <a
                                            href="{{route('front.privacy-policy')}}"
                                            target="_blank">{{route('front.privacy-policy')}}</a>
                                </div>
                            </div>
                            <div class="text-center mt-5 mb-7">Please click AGREE if you agree to the foregoing and to
                                our <a href="{{route('front.privacy-policy')}}" target="_blank">Data Privacy Policy.</a>
                            </div>


                        </div>


                        {{--  {!! Form::submit('Agree', [ 'id' => 'agreement_accept_btn', 'class' => 'disabled btn btn-blue
                        float-right ml-3', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Please
                        scroll down the Consent Agreement first']) !!}  --}}
                        {!! Form::submit('Agree', [ 'id' => 'agreement_accept_btn', 'class' => 'disabled btn btn-blue
                        float-right ml-3']) !!}

                        <div class="btn btn-default float-right" data-toggle="modal" data-target="#LogoutModal">Disagree
                        </div>
                        {{ Form::close() }}

                        @push('scripts')

                        <script>
                            $('document').ready(function(){
                                
                                $('#agreement_wrapper').scroll( function() {
                                    if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight-30) {
                                        console.log($('#agreement_accept_btn'));
                                        $('#agreement_accept_btn').removeClass("disabled").tooltip("disable").blur();
                                    }
                                });
                                $('#agreement_wrapper').scroll();
                                $('#agreement_accept_btn').click(function(e){
                                    if($(this).hasClass('disabled')){
                                        e.preventDefault();
                                    }
                                });
                            });
                            
                        </script>
                        @endpush


                        <div class="modal fade" id="LogoutModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="EditFormModalLabel"></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body py-0 px-4">
                                        <div class="text-center">
                                            <p class="mb-4">By refusing this consent, you cannot proceed further.</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default float-right"
                                            data-dismiss="modal">Cancel</button>
                                        {{ Form::open(array('route' => 'logout')) }}
                                        {!! Form::submit('Logout', ['id'=> 'submit_button', 'class' => 'btn btn-orange
                                        float-right']) !!}
                                        {{ Form::close() }}


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->

@endsection