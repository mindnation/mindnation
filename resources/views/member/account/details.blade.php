@extends('layouts.member_account')

@php $page_title = "My Details | Account"; @endphp

@section('title', $page_title)

@section('content')


<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        {{ Form::open(array('route' => 'member.account.store_details')) }}
        <div class="card m-0 shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">My Details</div>

                @if ($message = Session::get('success'))
                <div class="callout callout-success text-success border-success py-2">
                    <div>{{ $message }}</div>
                </div>
                @endif

                {{--  @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif  --}}

                <div class="h5 border-bottom py-3 mt-3 mb-4">Personal Information</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0 font-size-sm">
                        Your personal information will be used in verifying your identity with your psychologist.
                    </div>
                    <div class="col-12 col-md-7 ">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'first_name'; @endphp
                                    {{ Form::label($field, 'First Name') }}
                                    {{ Form::text($field, auth()->user()->first_name ,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'last_name'; @endphp
                                    {{ Form::label($field, 'Last Name') }}
                                    {{ Form::text($field, auth()->user()->last_name ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6 ">
                                <div class="form-group">
                                    @php $field = 'birth_date'; @endphp
                                    {{ Form::label($field, 'Birth Date') }}
                                    <div class="input-group date" id='bithdatepicker'>
                                        {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                        
                                        <div class="input-group-addon input-group-append">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8 col-sm-4">
                                <div class="form-group">
                                    @php $field = 'gender'; @endphp
                                    {{ Form::label($field, 'Gender') }}
                                    {{ Form::select($field, [null=>'Select Gender ...'] + auth()->user()->getGenderOptions(), auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="h5 border-bottom py-3 mb-4">Contact Information</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0 font-size-sm">
                        Your contact information will provide the means of communication with your psychologist..
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'contact_number'; @endphp
                                    {{ Form::label($field, 'Contact Number') }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'email'; @endphp
                                    {{ Form::label($field, 'Email') }}
                                    {{ Form::text($field, auth()->user()->email ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' ), 'disabled' => 'disabled']) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="h5 border-bottom py-3 mb-4">Emergency Contact</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0 font-size-sm">
                        Your safety matters to us. Please be assured that this will only be used in an actual emergency.
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'emergency_contact_name'; @endphp
                                    {{ Form::label($field, 'Name') }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'emergency_contact_relationship'; @endphp
                                    {{ Form::label($field, 'Relationship') }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'emergency_contact_number'; @endphp
                                    {{ Form::label($field, 'Contact Number') }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

            <div class="card-footer bg-white">
                <a href="{{route('member.account.details')}}" class="btn btn-default">Cancel</a>
                {!! Form::submit('Save', [ 'class' => 'btn btn-info float-right']) !!}
            </div>
        </div>
        {{ Form::close() }}
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection


@push('scripts')
<script>
    $(document).ready(function(){
        //https://eonasdan.github.io/bootstrap-datetimepicker/
        var datePicker = $('#bithdatepicker');

        datePicker.datetimepicker({
           
        allowInputToggle: true,
        useCurrent: false,
        viewMode:'years',
        format: 'DD/MM/YYYY',
        minDate: moment().subtract(100, 'years'),
        maxDate: moment(),
        icons: {
            previous: 'fas fa-angle-left',
            next: 'fas fa-angle-right',       
        },

        });
    });

</script>
@endpush