@extends('layouts.member_account')

@php $page_title = "Settings | Account"; @endphp

@section('title', $page_title)

@section('content')


<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        {{ Form::open(['route' => 'member.account.store_settings', 'id' => 'account_settings_form']) }}
        <div class="card m-0 shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">Account Settings</div>

                @if ($message = Session::get('success'))
                <div class="callout callout-success text-success border-success py-2">
                    <div>{{ $message }}</div>
                </div>
                @endif

                @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif

                <div class="h5 border-bottom py-3 mt-5 mb-4">Notifications</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0 font-size-sm">
                        Get notified about booking confirmations and reminders as well as other helpful emails in taking care of your mental health and using our service.
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="form-group">
                            @php 
                                $field = 'settings_email_notifications'; 
                                $user_notif = auth()->user()->getMetaValue($field);
                                $user_notif = ($user_notif === null || $user_notif)?true:false;
                            @endphp
                            <div class="custom-control custom-switch custom-switch-off-default custom-switch-on-teal">
                                {{ Form::checkbox($field, true, $user_notif ,['id' => $field, 'class' => 'custom-control-input '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                {{ Form::label($field, 'Email Notifications', ['class' => 'custom-control-label']) }}

                              </div>
                        </div>
                    </div>
                </div>


                <div class="h5 border-bottom py-3 mt-5 mb-4">Terms & Conditions and Privacy Policy</div>
                <div class="row mb-4">
                    <div class="col-12 col-md-5 mb-4 mb-md-0 font-size-sm">
                        To ensure your safe and secure use of our platform, an understanding of our Terms of Use and Privacy Policy is of the essence.
                    </div>
                    <div class="col-12 col-md-7 text-center">
                        <div class="row pb-3 justify-content-center justify-content-md-left">
                            <div class="col-8 col-lg-5">
                                <a href="{{ route('front.privacy-policy') }}" target="_blank" class="btn btn-default" style="min-width: 100%;"> Read Terms of Use</a>
                            </div>
                        </div>
                        <div class="row justify-content-center justify-content-md-left">
                            <div class="col-8 col-lg-5 ">
                                <a href="{{ route('front.terms') }}" target="_blank" class="btn btn-default" style="min-width: 100%;"> Read Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

        </div>
        {{ Form::close() }}
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection


@push('scripts')
<script>
    $(document).ready(function(){
        var settingsForm = $('#account_settings_form');
        settingsForm.find('input[type=checkbox]').change(function(){
            settingsForm.submit();
        });
    });

</script>
@endpush