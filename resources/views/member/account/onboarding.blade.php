@extends('layouts.member', ['no_sidebar' => true])

@php $page_title = "Onboarding"; @endphp

@section('title', $page_title)
@section('content')
<div class="content " >
    <div class="container  px-1 px-md-3 py-5 py-md-7 ">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-9 col-lg-7">
                
                

                

           
                <div class="card shadow-lg m-0">
                    <div class="card-body py-5">
                        
                            <div class="text-center">
                                <h1 class="text-purple mb-5">Mental health is a journey.</h1>
                                
                                <p class="mb-2">We’re here to help you navigate through the ups and downs.</p>
                                <p>Let our psychologists know how best they can help you.</p>

                                <a href="{{route('member.onboarding.survey')}}" class="btn btn-purple btn-lg btn-min-width-lg mt-1 mx-auto mt-5">Take 2 minute quiz</a>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->

@endsection