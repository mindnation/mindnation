@extends('layouts.member_account')

@php $page_title = "Change Password | Account"; @endphp

@section('title', $page_title)

@section('content')


<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        {{ Form::open(array('route' => 'member.account.store_password')) }}
        <div class="card m-0 shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">Change Password</div>

                @if ($message = Session::get('success'))
                <div class="callout callout-success text-success border-success py-2">
                    <div>{{ $message }}</div>
                </div>
                @endif

                {{--  @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                            <div>{{ $error }}
                        </div>
                        @endforeach
                    </div>
                    @endif --}}

        <div class="border-bottom py-3 mt-3 mb-5 font-size-sm"></div>
        <div class="row mt-2 mb-5">

            <div class="col-12 col-lg-8 offset-lg-1">
                <div class="form-group row">
                    @php $field = 'current_password'; @endphp
                    {{ Form::label($field, 'Current Password', ['class' => 'col-12 col-md-4 col-form-label text-md-right']) }}
                    <div class="col-12 col-md-5">
                        {{ Form::password($field,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                        @error($field)
                        <div class="invalid-feedback text-left">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    @php $field = 'new_password'; @endphp
                    {{ Form::label($field, 'New Password', ['class' => 'col-12 col-md-4 col-form-label text-md-right']) }}
                    <div class="col-12 col-md-5">
                        {{ Form::password($field,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                        @error($field)
                        <div class="invalid-feedback text-left">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    @php $field = 'confirm_new_password'; @endphp
                    {{ Form::label($field, 'Confirm New Password', ['class' => 'col-12 col-md-4 col-form-label text-md-right']) }}
                    <div class="col-12 col-md-5">
                        {{ Form::password($field,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                        @error($field)
                        <div class="invalid-feedback text-left">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-8 offset-4">
                        {!! Form::submit('Save', [ 'class' => 'btn btn-info']) !!}
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
{{ Form::close() }}
</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection