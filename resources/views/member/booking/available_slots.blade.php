
@if(count($available_slots))

    @php $i = 0; @endphp
    <div class="row px-3 py-5 justify-content-center">
        @foreach ($available_slots as $appointment)
        <div class="col-4 p-1">
            {{ Form::radio('slot', $appointment->start_time, false, ['id' => 'start_time-'.$i, 'class' => 'radio-line']) }}
            {{ Form::label('start_time-'.$i, $appointment->getCarbonStart()->format('g:i A')) }}
            {{ Form::hidden('professional', $appointment->psych->id) }}
        </div>
        {{-- 
        <div>{{ $appointment->start_time }} {{ $appointment->psych->name() }} </div> --}}
        @php $i++; @endphp
        @endforeach
    </div>

    <script>
        loadICheck();
    </script>
@else

<div class="text-center pt-10 h6">No slots available at this date</div>

@endif