@extends('member.booking.index')


@section('booking-content')

<div class="col-12 col-md-8 col-lg-7">
    <div class="card border-multicolor-top shadow-lg">
        <div class="card-body pt-5">
            <div class="h5 text-center mb-5">Select a Mental Health Professional</div>
            @if ($errors->any())
                <div class="callout callout-danger text-danger text-center">
                    @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
            @if(isset($assigned_psychs) && count($assigned_psychs))
            {{ Form::open(array('route' => 'member.booking.select_pro')) }}
            <div class="d-flex mt-5">
                <div class="list-inline mx-auto justify-content-center">
                    @foreach($assigned_psychs as $psych)
                        <div class="form-radio ">
                            {{ Form::radio('professional', $psych->id, ($preference == $psych->id)?true:false, ['id' => 'professional-'.$psych->id, 'class' => 'radio-square', 'data-color' => 'teal']) }}
                            {{ Form::label('professional-'.$psych->id, $psych->name()) }}
                        </div>
                    @endforeach
                    <div class="form-radio ">
                        {{ Form::radio('professional', '', (!$preference)?true:false, ['id' => 'professional-nopref', 'class' => 'radio-square', 'data-color' => 'teal']) }}
                        {{ Form::label('professional-nopref', 'No preference') }}
                    </div>
                </div>
            </div>
            <div class="form-footer mt-5">

                <a href="{{route('member.dashboard')}}" class="btn btn-default ">Return</a>
                {!! Form::submit('Proceed', ['id'=> 'submit_button', 'class' => 'btn btn-info float-right']) !!}

            </div>
            {{ Form::close() }}
            @else
            <div class="text-center">
               <div class="mb-6">No professional available</div>
               <a href="{{route('member.dashboard')}}" class="btn btn-blue mx-auto">Back</a>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection