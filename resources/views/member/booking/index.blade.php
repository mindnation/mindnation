@extends('layouts.member')

@php $page_title = "Schedule a Consultation"; @endphp

@section('title', $page_title)
@section('body_class', 'booking')

@section('content')


<!-- Main content -->
<div class="content">
    <div class="container pt-4 pb-6 py-md-7 px-0 px-md-3">
        <h1 class="mb-4 text-center text-teal infront-of-bg-shape">{{ $page_title }}</h1>
        <div class="row justify-content-center ">
            @yield('booking-content')
        </div>
        

    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection