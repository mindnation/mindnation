@extends('member.booking.index')


@section('booking-content')

<div class="col-12 col-md-12 col-lg-10">
    <div class="card border-multicolor-top shadow-lg">
        <div class="card-body pt-5">
            <div class="h5 text-center mb-5 mb-md-7">Select Date and Timeslot</div>

            @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
            {{ Form::open(array('route' => 'member.booking.select_slot', 'class' => 'select-slot-form')) }}
                <div class="row mt-5">
                    <div class="col-12 mb-5 col-md-6  mb-md-0">
                        <input name="date" type='hidden' class="form-control" id='datetimepicker' />
                    </div>
                    <div class="col-12 col-md-6">
                        <div id="render-div">
                            <div class="text-center"><span class="date-title badge-pill badge-teal h2 font-weight-light"></span></div>
                            <div class="mt-5"><span class="h6">Professional:</span> {{($psych) ? $psych->name() : 'No preference'}}</div>

                            <div class="available-slots"></div>
                        </div>
                    </div>
                </div>
                <div class="form-footer mt-5">

                    <a href="{{route('member.booking.select_pro')}}" class="btn btn-default">Return</a>
                    {!! Form::submit('Proceed', ['id'=> 'submit_button', 'class' => 'btn btn-info float-right']) !!}
    
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection



@push('scripts')
<script>
    $(document).ready(function(){
        //https://eonasdan.github.io/bootstrap-datetimepicker/
        var datePicker = $('#datetimepicker'),
        selectSlotForm = $('.select-slot-form');

        datePicker.datetimepicker({
            inline: true,
            viewMode:'days',
            format: 'L',
            minDate: "{{ $min_date }}",
            maxDate: "{{ $max_date }}",
            icons: {
                previous: 'fas fa-angle-left',
                next: 'fas fa-angle-right',       
            },

        }).on('dp.change', function(e){ 
            getAppointmentSlots(e.date);
        });

        getAppointmentSlots();

        // Disable month and year change
        $('.picker-switch').click(function(e){
            e.stopPropagation();
        });

        $(document).on('ifChecked','input[name=slot]', function(){
            if ($(this).is(':checked')) {
                selectSlotForm.find('#submit_button').attr("disabled", false);
            }
        });

        

        function getAppointmentSlots(date = null)
        {
            var renderDiv = $('#render-div'),
            dateTitle = renderDiv.find('.date-title'),
            availableSlotsDiv = renderDiv.find('.available-slots');

            selectSlotForm.find('#submit_button').attr("disabled", true);

            if(!date){
                date = datePicker.data("DateTimePicker").viewDate();
            }
            //date = date.format('YYYY-MM-DD');
            dateTitle.text(date.format('ddd, MMM D'));
            
            $.ajax({
                url: "{{ route('member.booking.get_slots') }}",
                method: 'GET',
                data: { date: date.format('YYYY-MM-DD') },
                dataType: "json",
                success: function (data) {
                    if (typeof (data.available_slots) != "undefined" && data.available_slots != null ) {   
                        availableSlotsDiv.html(data.available_slots);
                    }
                }
            });
        }



    });
</script>
@endpush