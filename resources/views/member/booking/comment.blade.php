@extends('member.booking.index')


@section('booking-content')

<div class="col-12 col-md-8 col-lg-7">
    <div class="card border-multicolor-top shadow-lg">
        <div class="card-body pt-5">
            <div class="h5 text-center mb-5">Is there anything you would like the psychologist to know before the
                consultation?</div>
            {{ Form::open(array('route' => 'member.booking.comment')) }}

            <div class="form-group">
                @php $field = 'comments'; @endphp

                {{ Form::textarea($field, null ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' ), 'rows' => 5]) }}
                @error($field)
                <div class="invalid-feedback text-left">{{ $message }}</div>
                @enderror
            </div>

            
<div class="form-footer mt-5 ">

    <a href="{{route('member.booking.select_method')}}" class="btn btn-default">Return</a>
    <button type="button" class="btn btn-info float-right confirm-btn" data-toggle="modal" data-target="#confirmModal">
        Proceed
    </button>


</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Booking Details</h5>
            </div>
            <div class="modal-body ">

                <div class="form-group row">
                    <label class="col-5 col-form-label">Provider:</label>
                    <div class="col-7">
                        <input type="text" disabled class="form-control" value="{{ $psych_name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-5 col-form-label">Date:</label>
                    <div class="col-7">
                        <input type="text" disabled class="form-control" value="{{ $date }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-5 col-form-label">Timeslot:</label>
                    <div class="col-7">
                        <input type="text" disabled class="form-control" value="{{ $start_time }}">
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-5 col-form-label">Comms:</label>
                    <div class="col-7">
                        <input id="comms-recap" type="text" disabled class="form-control"
                            value="{{ ($method = $appointment->getMethod())? $method: 'No Preference' }}">
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <label class="col-5 col-form-label">Comment:</label>
                    <div class="col-7">
                        
                            {!! Form::textarea('comments-recap',$appointment->comment , ['id' => 'comments-recap', 'class' => 'form-control', 'rows' => 5, 'disabled']) !!}
                    </div>
                </div>


            </div>
            <div class="modal-footer modal-footer justify-content-between">
                <button type="button" class="btn btn-default " data-dismiss="modal">Edit</button>
                {!! Form::submit('Confirm', ['id'=> 'submit_button', 'class' => 'btn btn-info']) !!}
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

</div>
</div>
</div>
@endsection

@push('scripts')
<script>
    $('.confirm-btn').click(function(){
         var comment = $("textarea[name=comments]");
        // var id = checkedComms.attr('id');
        // labelText = $("label[for='"+id+"']").text();
         $('#comments-recap').val(comment.val());
        
    });
</script>
@endpush