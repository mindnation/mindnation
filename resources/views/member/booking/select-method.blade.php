@extends('member.booking.index')


@section('booking-content')

<div class="col-12 col-md-8 col-lg-7">
    <div class="card border-multicolor-top shadow-lg">
        <div class="card-body pt-5">
            <div class="h5 text-center mb-5">Choose your preferred communication method</div>
            @if ($errors->any())
                <div class="text-danger text-center mb-5">
                    @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
            {{ Form::open(array('route' => 'member.booking.select_method')) }}
            @if(isset($methods) && count($methods))
            <div class="d-flex">
                <div class="list-inline mx-auto justify-content-center">
                    @foreach($methods as $key=>$value)
                    <div class="form-radio ">
                        {{ Form::radio('method', $key, ($preference == $key)?true:false, ['id' => 'method-'.$key, 'class' => 'radio-square' , 'data-color' => 'teal']) }}
                        {{ Form::label('method-'.$key, $value) }}
                    </div>
                    @endforeach
                    {{--  <div class="form-radio ">
                        {{ Form::radio('method', '', (!$preference)?true:false, ['id' => 'method-nopref', 'class' => 'radio-square' , 'data-color' => 'teal']) }}
                        {{ Form::label('method-nopref', 'No preference') }}
                    </div>  --}}
                </div>
            </div>
            @else
            No Method Available
            @endif
            <div class="form-footer mt-5 ">

                <a href="{{route('member.booking.select_slot')}}" class="btn btn-default">Return</a>
                {!! Form::submit('Proceed', ['id'=> 'submit_button', 'class' => 'btn btn-info float-right']) !!}
                

            </div>

            

           
            {{ Form::close() }}

        </div>
    </div>
</div>
@endsection

