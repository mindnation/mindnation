@extends('layouts.psych')

@php $page_title = "Dashboard"; @endphp

@section('title', $page_title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ $page_title }}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-12 col-md-8 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <div class="h5">Assigned Companies</div>
                    </div>
                    <div class="card-body">
                        <ul>
                        @foreach ($assigned_companies as $company)
                            <li>
                                <div class="h6">{{ $company->name }}</div>
                            </li>
                        @endforeach
                    </ul>

                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <div class="h5">Upcoming Consultations</div>
                    </div>
                    <div class="card-body">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection


@push('scripts')
<script>
    $(document).ready(function () {

var calendarEl = document.getElementById('calendar');

var calendar = new Calendar(calendarEl, {
    plugins: [timeGridPlugin, dayGridPlugin, listPlugin],
    defaultView: 'listMonth',
    //themeSystem: 'bootstrap',
    nowIndicator: true,
    firstDay: 1,
    allDaySlot: false,
    slotDuration: '01:00:00',
    minTime: "{{ $min_time }}",
    maxTime: "{{ $max_time }}",
    displayEventEnd:false,
    header: {
        left: 'prev,next today',
        center: 'title',
        right: '', //dayGridMonth,
      },
    events:{!! $events !!},

    eventRender: function (info) {
        //console.log(info.event.extendedProps);
        // {description: "Lecture", department: "BioChemistry"}
        // if(info.event.title){

        // $(info.el).popover({html:true,title:info.event.title,placement:'top',container:'body',trigger: 'focus'});
       
        // }
    },
    eventClick: function(info) {
      
        
                   
    },
});

calendar.render();
});
</script>
@endpush