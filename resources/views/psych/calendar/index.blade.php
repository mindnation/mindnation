@extends('layouts.psych')

@php $page_title = "Calendar"; @endphp

@section('title', $page_title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ $page_title }}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid px-2 px-md-3">
        <div class="row">
            
            <div class="col-12 col-lg-9">
                <div class="card mb-4 ">
                    <div class="card-body p-0">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="h5 mb-5">Legend</div>
                        <div class="mb-2">
                            <span class="badge badge-purple border py-2 px-3">Consultation: Confirmed</span>
                        </div>
                        <div class="mb-2">
                            <span class="badge badge-blue border py-2 px-3">Consultation: Completed</span>
                        </div>
                        <div class="mb-2">
                            <span class="badge badge-teal border py-2 px-3" style="opacity:0.4">Non-working hours</span>
                        </div>
                        <div class="mb-2">
                            <span class="badge badge-white border border-gray-light py-2 px-3">Working Hours</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection



@push('scripts')
<script>
    $(document).ready(function () {

var calendarEl = document.getElementById('calendar');

var calendar = new Calendar(calendarEl, {
    plugins: [timeGridPlugin, dayGridPlugin, listPlugin],
    defaultView: 'timeGridWeek',
    //themeSystem: 'bootstrap',
    nowIndicator: true,
    firstDay: 1,
    allDaySlot: false,
    slotDuration: '01:00:00',
    minTime: "{{ $min_time }}",
    maxTime: "{{ $max_time }}",
    displayEventEnd:false,
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'listMonth,timeGridWeek,timeGridDay', //dayGridMonth,
      },
    events:{!! $events !!},

    eventRender: function (info) {
        //console.log(info.event.extendedProps);
        // {description: "Lecture", department: "BioChemistry"}
        // if(info.event.title){

        // $(info.el).popover({html:true,title:info.event.title,placement:'top',container:'body',trigger: 'focus'});
       
        // }
    },
    eventClick: function(info) {
      
        
                   
    },
});

calendar.render();
});
</script>
@endpush