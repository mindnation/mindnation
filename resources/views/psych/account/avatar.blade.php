@if($avatar = auth()->user()->avatar)
<div class="avatar mb-5 mx-auto">
    <img id="avatar" class="w-100 " src="{{asset('storage/'.$avatar->filename)}}"
        data-src="{{asset('storage/'.$avatar->filename)}}">
</div>
@else
<div class="avatar mb-5 bg-light mx-auto">
    <img id="avatar" class="w-100">
</div>
@endif


<div class="form-group">
    <label for="avatar" class="mr-3">Change Photo:</label>
    {!! Form::file('avatar',['id'=>'avatar_upload_input', 'class'=>'form-control-file']) !!}
</div>
@if(isset($psych_avatars))
<div class="h6 ">OR</div>
<div class="btn btn-teal" data-toggle="modal" data-target="#avatarSelectModal">Select an
    existing</div>
{{ Form::hidden('avatar_selected', null, ['id' => 'avatar_selected_input']) }}


<!-- Modal -->
<div class="modal fade" id="avatarSelectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Profile Photo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="existing_avatar_list" class="row px-4">

                    @foreach ($psych_avatars as $avatar)
                    <div class="col-3 p-2 avatar-select" data-media-id="{{$avatar->id}}"
                        data-src="{{asset('storage/'.$avatar->filename)}}">
                        <img class="w-100 " src="{{asset('storage/'.$avatar->filename)}}">
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endif



@push('scripts')
<script>
    $(document).ready(function(){
        var avatarList = $('#existing_avatar_list'),
            avatarSelect = avatarList.find('.avatar-select');
            avatar = $('#avatar'),
            avatarSelectedInput = $('#avatar_selected_input');

            avatarSelect.click(function(){

                if(!$(this).hasClass('selected')){
                    avatarSelect.removeClass('selected');
                    $(this).addClass('selected');
                    avatar.attr('src',$(this).data('src'));
                    avatarSelectedInput.val($(this).data('media-id'));
                }
                else{
                    avatarSelect.removeClass('selected');
                    avatar.attr('src',avatar.data('src'));
                }
            });
    });
</script>
@endpush