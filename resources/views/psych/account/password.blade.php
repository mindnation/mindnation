@extends('layouts.psych')

@php $page_title = "My Account"; @endphp

@section('title', $page_title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ $page_title }}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid pb-5">
        {{ Form::open(array('route' => 'psych.account.store_password')) }}
        <div class="card shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">Change Password</div>

                @if ($message = Session::get('success'))
                    <div class="callout callout-success text-success border-success py-2">
                        <div>{{ $message }}</div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif

                <div class="border-bottom py-3 mt-3 mb-5"></div>
                <div class="row mt-2 mb-5">
                    
                    <div class="col-12 col-lg-8 offset-lg-1">
                        <div class="form-group row">
                            @php $field = 'current_password'; @endphp
                            {{ Form::label($field, 'Current Password', ['class' => 'col-12 col-md-4 col-form-label text-md-right']) }}
                            <div class="col-12 col-md-5">
                                {{ Form::password($field,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            @php $field = 'new_password'; @endphp
                            {{ Form::label($field, 'New Password', ['class' => 'col-12 col-md-4 col-form-label text-md-right']) }}
                            <div class="col-12 col-md-5">
                                {{ Form::password($field,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            @php $field = 'confirm_new_password'; @endphp
                            {{ Form::label($field, 'Confirm New Password', ['class' => 'col-12 col-md-4 col-form-label text-md-right']) }}
                            <div class="col-12 col-md-5">
                                {{ Form::password($field,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-8 offset-4">
                                {!! Form::submit('Save', [ 'class' => 'btn btn-info']) !!}
                            </div>
                        </div>
                        
                    </div>
                </div>
   
            </div>
        </div>
        {{ Form::close() }}
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
