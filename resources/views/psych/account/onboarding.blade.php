@extends('layouts.psych', ['no_sidebar' => true])

@php $page_title = "Onboarding"; @endphp

@section('title', $page_title)
@section('content')
<div class="content">
    <div class="container px-1 px-md-3 py-4 py-md-7">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-10 col-lg-8">
                <div class="card shadow-lg m-0">
                    {{ Form::open(array('route' => 'psych.store_onboarding', 'files' => true)) }}
                    <div class="card-body py-5">

                        <div class="text-center mb-5">
                            <h1 class="text-purple">Welcome to MindNation</h1>
                            <p class="font-size-sm">Please fill out information sheet below before proceeding</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-8">
                                
                                @if ($message = Session::get('success'))
                                <div class="callout callout-success text-success border-success py-2">
                                    <div>{{ $message }}</div>
                                </div>
                                @endif

                                 @if ($errors->any())
                                <div class="callout callout-danger text-danger border-danger  py-2">
                                    Something is missing. Please check and try again.
                                    {{--  @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                    @endforeach  --}}
                                </div>
                                @endif  


                                <div class="form-group">
                                    @php $field = 'full_name'; @endphp
                                    {{ Form::label($field, 'Full Name',['class'=>'badge-yellow badge-pill h6']) }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    <div><small>(Full Name with suffix. This will be the name displayed to the members)</small></div>
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    @php $field = 'contact_number'; @endphp
                                    {{ Form::label($field, 'Contact Number',['class'=>'badge-yellow badge-pill h6']) }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    @php $field = 'doxyme_url'; @endphp
                                    {{ Form::label($field, 'Doxy.me URL',['class'=>'badge-yellow badge-pill h6']) }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    <div><small>(Example of valid URL: https://doxy.me/DrJohn)</small></div>
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    @php $field = 'short_background'; @endphp
                                    {{ Form::label($field, 'Short Background',['class'=>'badge-yellow badge-pill h6']) }}
                                    {{ Form::textarea($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' ), 'rows' => 6]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    @php $field = 'timeslots'; @endphp
                                    {{ Form::label($field, 'Timeslots', ['class'=>'badge-yellow badge-pill h6']) }}
                                    {{ Form::textarea($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' ), 'rows' => 2]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                                
                                <div class="form-group mt-5">
                                    
                                    <div class="badge-yellow badge-pill h6 d-inline-block">Expertise</div>
                                    
                                    @php 
                                        $field = 'expertises';
                                        $expertises =  unserialize(auth()->user()->getMetaValue($field));
                                    @endphp
                                    @error($field)
                                        <div class="d-block invalid-feedback text-left mb-3">{{ $message }}</div>
                                    @enderror
                                    <div class="row px-3">
                                        @foreach (config('issues') as $key=>$value)
                                        <div class="col-6 p-2">
                                            {{ Form::checkbox($field.'['.$key.']', $value, isset($expertises[$key])?true:null, ['id' => $key, 'class' => 'checkbox-line' ]) }}
                                            {{ Form::label($key, $value) }}
                                        </div>
                                        @endforeach
                                    </div>
                                    
                                </div>

                                <div class="form-group mt-5 ">
                                    <div class="badge-yellow badge-pill h6 d-inline-block">Avatar</div>
                                    <div class="text-center">
                                    <div class="mb-4"><small>The avatar should be a circle PNG format with dimension ratio 1:1. (Max size: 2Mb)</small></div>

                                    @error('avatar')
                                        <div class="d-block invalid-feedback text-left mb-3">{{ $message }}</div>
                                    @enderror
                                    <div>

                                        @include('psych.account.avatar')
                                    </div>
                                   
                                </div>






                                <div class="text-center mt-5">
                                    {!! Form::submit("Let's Begin", [ 'class' => 'btn btn-purple btn-lg btn-min-width-lg mx-auto']) !!}
                                </div>


                            </div>
                        </div>

                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->

@endsection