@extends('layouts.psych')

@php $page_title = "My Account"; @endphp

@section('title', $page_title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ $page_title }}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid pb-5 px-2 px-md-3">
        {{ Form::open(array('route' => 'psych.account.store_profile', 'files' => true)) }}
        <div class="card shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">Profile</div>

                @if ($message = Session::get('success'))
                <div class="callout callout-success text-success border-success py-2">
                    <div>{{ $message }}</div>
                </div>
                @endif

                @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach

                </div>
                @endif

                <div class="h5 border-bottom py-3 mt-5 mb-3">Avatar</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        The avatar should be a circle PNG format with dimension ratio 1:1. (Max size: 2Mb)
                    </div>
                    <div class="col-12 col-lg-4 offset-lg-3 text-center">
                        @include('psych.account.avatar')
                    </div>
                </div>

                <div class="h5 border-bottom py-3 mt-5 mb-3">Personal Information</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        Your personal information will be used in verifying your identity as a licensed psychologist.
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'first_name'; @endphp
                                    {{ Form::label($field, 'First Name') }}
                                    {{ Form::text($field, auth()->user()->first_name ,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                    <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'last_name'; @endphp
                                    {{ Form::label($field, 'Last Name') }}
                                    {{ Form::text($field, auth()->user()->last_name ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                    <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    @php $field = 'full_name'; @endphp
                                    {{ Form::label($field, 'Full Name') }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    <div><small>(Full Name with suffix. This will be the name displayed to the members)</small></div>
                                    @error($field)
                                    <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="h5 border-bottom py-3 mb-3">Contact Information</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        Your contact information will provide the means of communication with our service users.
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'contact_number'; @endphp
                                    {{ Form::label($field, 'Contact Number') }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                    <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'email'; @endphp
                                    {{ Form::label($field, 'Email') }}
                                    {{ Form::text($field, auth()->user()->email ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' ), 'disabled' => 'disabled']) }}
                                    @error($field)
                                    <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'doxyme_url'; @endphp
                                    {{ Form::label($field, 'Doxy.me URL') }}
                                    {{ Form::text($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    <div><small>(Example of valid URL: https://doxy.me/DrJohn)</small></div>
                                    @error($field)
                                    <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="h5 border-bottom py-3 mb-3">Background</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        Your background shall state relevant professional experiences and your unique approach in no more than 1000 characters. Make it professional yet personable.
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="form-group">
                            @php $field = 'short_background'; @endphp
                            {{ Form::label($field, 'Short Background') }}
                            {{ Form::textarea($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' ), 'rows' => 5]) }}
                            @error($field)
                            <div class="invalid-feedback text-left">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="h5 border-bottom py-3 mb-3">Timeslots</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        Provide a quick summary of your timeslots in this format: MTWThF 10AM-12PM, 1PM-5PM Sat 1PM-5PM
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="form-group">
                            @php $field = 'timeslots'; @endphp
                            {{ Form::label($field, 'Timeslots') }}
                            {{ Form::textarea($field, auth()->user()->getMetaValue($field) ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' ), 'rows' => 3]) }}
                            @error($field)
                            <div class="invalid-feedback text-left">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="h5 border-bottom py-3 mb-3">Expertise</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        Tick off the issues and concerns where your expertise and comfort lie.
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="form-group">
                            @php
                            $field = 'expertises';
                            $expertises = unserialize(auth()->user()->getMetaValue($field));
                            @endphp
                            @error($field)
                            <div class="d-block invalid-feedback text-left mb-3">{{ $message }}</div>
                            @enderror
                            <div class="row px-3">
                                @foreach (config('issues') as $key=>$value)
                                <div class="col-12 col-sm-6 p-2">
                                    {{ Form::checkbox($field.'['.$key.']', $value, isset($expertises[$key])?true:null, ['id' => $key, 'class' => 'checkbox-line' ]) }}
                                    {{ Form::label($key, $value) }}
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="card-footer bg-white">
                <a href="{{route('psych.account.profile')}}" class="btn btn-default">Cancel</a>
                {!! Form::submit('Save', [ 'class' => 'btn btn-info float-right']) !!}
            </div>
        </div>
        {{ Form::close() }}
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

