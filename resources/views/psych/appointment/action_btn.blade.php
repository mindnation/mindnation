
<a href="#" class="consultation-details-btn float-right px-1" data-toggle="modal" data-target="#ConsultationDetailsModal" data-id="{{$appointment->id}}">
    <i class="text-danger far fa-edit"></i>
</a>

<a href="#" class="patient-details-btn float-right px-1"  data-target="#PatientDetailsModal" data-id="{{$member->id}}">
    <i class="text-info far fa-user"></i>
</a>

