@extends('layouts.psych')

@php $page_title = "Consultations"; @endphp

@section('title', $page_title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ $page_title }}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->


<!-- Main content -->
<div class="content">
    <div class="container-fluid px-2 px-md-3">

        <div class="card">
            <div class="card-body px-2 px-md-3">
                
                <table class="w-100 table  table-bordered table-striped dataTable table-hover dt-responsive ">
                </table>
              

            </div>
        </div>
        

        <div class="modal fade" id="PatientDetailsModal" tabindex="-1" role="dialog" aria-labelledby="PatientDetailsModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header px-4 py-3">
                        <h4 class="modal-title" id="PatientDetailsModalLabel"><i class="far fa-user pr-2"></i>Patient Details</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body px-5">
                         
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue " data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ConsultationDetailsModal" tabindex="-1" role="dialog" aria-labelledby="ConsultationDetailsModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="ConsultationDetailsModalLabel"><i class="fas fa-notes-medical pr-2"></i>Consultation Details</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-0">
                         
                    </div>
                   
                </div>
            </div>
        </div>
        
        
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

@endsection


@push('scripts')
    @include('admin.partials.datatable.script')


    <script>    
        $(document).on('click', '.patient-details-btn', function (e) {
             e.preventDefault();

            var target = $(this).data('target');
            var modal = $(target);
            var id = $(this).data('id');

            $.ajax({
                url: "{{ route('psych.patients.get_data') }}",
                method: 'GET',
                data: { id: id },
                dataType: "json",
                success: function (data) {
                    if (typeof (data.html) != "undefined" && data.html != null ) {  
                        modal.find('.modal-body').html(data.html);
                        modal.modal('show');
                    }
                }
            });         
        });

        $(document).on('click', '.consultation-details-btn', function (e) {
             e.preventDefault();

            var target = $(this).data('target');
            var modal = $(target);
            var id = $(this).data('id');

            $.ajax({
                url: "{{ route('psych.appointments.get_data') }}",
                method: 'GET',
                data: { id: id },
                dataType: "json",
                success: function (data) {
                    if (typeof (data.html) != "undefined" && data.html != null ) {  
                        modal.find('.modal-body').html(data.html);
                        modal.modal('show');
                    }
                }
            });         
        });

        $(document).on('submit', '.appointment-edit-form', function(e){
            e.preventDefault();
            var form = $(this);
            var response_div = $(this).find('.form-response');
            var parent_modal = $(this).parents('.modal');
            var submitBtn = form.find('input[type=submit]');
            submitBtn.prop('disabled', true);
            response_div.empty();
            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                dataType: "json",
                success: function (data) {
                    if(data.error.length > 0){
                        response_div.html('<div class="callout callout-danger text-danger py-2">' + data.error + '</div>');
                    }
                    if(data.success.length > 0){
                        response_div.html('<div class="callout callout-success text-success py-2">' + data.success + '</div>');
                    }
                    reloadDatatable();

                    setTimeout(function() {
                        parent_modal.modal('hide');
                        submitBtn.prop('disabled', false);
                    }, 500)
                },
                error: function (xhr, status, error) {
                    var errors = xhr.responseJSON;
                    // console.log(errors);
                    $.each(errors.errors, function (key, value) {
                        var field = form.find('[name="' + key + '"]');
                        field.addClass('is-invalid');
                        field.parent('.form-group').append('<div class="invalid-feedback text-left">' + value + '</div>');
                    });
                    
                    if (typeof (errors.exception) != "undefined" && errors.exception !== null) {
                        response_div.html('<div class="callout callout-danger text-danger border-danger">Something went wrong.<br>Reload the page and try again.</div>').show();
                    }
                    if (typeof (errors.errors.message) != "undefined" && errors.errors.message !== null) {
                        response_div.html('<div class="callout callout-danger text-danger border-danger">'+errors.errors.message+'</div>').show();
                    }
                    submitBtn.prop('disabled', false);
                },
            }); 
        });

        function reloadDatatable() {
            table = $('.dataTable');
            table.dataTable();
            table.fnDraw(false);
        }

    </script>
@endpush