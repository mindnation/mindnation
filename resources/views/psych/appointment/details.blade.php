{{ Form::open(array('route' => 'psych.appointments.edit', 'class' => 'appointment-edit-form')) }}
{{ Form::hidden('id', $appointment->id) }}
<div class="p-3">
    <div class="form-response"></div>
    <div class="row mb-2">
        <div class="col-3 font-weight-bold">Date:</div>
        <div class="col-9">{{$appointment->getCarbonStart()->format('d/m/Y')}}</div>
    </div>
    <div class="row mb-2">
        <div class="col-3 font-weight-bold">Time:</div>
        <div class="col-9">{{$appointment->getCarbonStart()->format('g:i A')}}</div>
    </div>
    <div class="row mb-2">
        <div class="col-3 font-weight-bold">Patient:</div>
        <div class="col-9">{{$patient->name()}}</div>
    </div>

    <div class="row mb-2">
        <div class="col-3 font-weight-bold">Comment:</div>
        <div class="col-9">{{$appointment->comments}}</div>
    </div>
    <div class="row mb-2">
        <div class="col-3 font-weight-bold">Status:</div>
        <div class="col-9">
            {{--  {{ Form::select('status', $appointment->getStatusOptions(), $appointment->status ,['class' => 'form-control form-control-sm', ($appointment->isCompleted() || $appointment->isCanceled())?'disabled':'' ]) }}  --}}
            {{ Form::select('status', $appointment->getStatusOptions(), $appointment->status ,['class' => 'form-control form-control-sm' ]) }}
        </div>
    </div>
</div>

<div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    {{--  {!! Form::submit('Save', ['id'=> 'submit_button', 'class' => 'btn btn-info float-right', ($appointment->isCompleted() || $appointment->isCanceled())?'disabled':'']) !!}  --}}
    {!! Form::submit('Save', ['id'=> 'submit_button', 'class' => 'btn btn-info float-right']) !!}

</div {{ Form::close() }}