@extends('layouts.psych')

@php $page_title = "Working Hours"; @endphp

@section('title', $page_title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ $page_title }}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid m-0 px-2 px-md-3 ">
        <div class="row ">
            <div class="col-12 col-xl-10">
                <div class="card mb-4 shadow-lg">
                    <div class="card-body pt-5">
                        <div class="h5 text-center mb-7">Select Date and Period</div>

                        {{ Form::open(array('route' => 'psych.working-hours.store', 'id' => 'workinghours-form')) }}
                        <div class="form-response"></div>
                        <div class="row ">

                            <div class="col-12 col-md-6 pb-5 pb-md-0 px-2 px-sm-3">
                                <input name="date" type='hidden' class="form-control" id='datetimepicker' />
                            </div>
                            <div class="col-6 col-12 col-md-6">

                                <div id="render-div">
                                    <div class="text-center mb-4"><span
                                            class="date-title badge-pill badge-teal h2 font-weight-light"></span></div>

                                    <table class="table table-borderless">
                                        <thead>
                                            <tr class="table-headers">
                                                <th class="h6">Starts</th>
                                                <th class="h6">Ends</th>
                                                <th style="width: 10px"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="working-hours">
                                            <tr>
                                                <td colspan="3" class="no-working-hours text-center h6">No working
                                                    periods set for this date</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <a class="add-period-btn text-info btn"><i class="fas fa-plus"></i>
                                                        Add Period</a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div class="">
                                        <div class="custom-control custom-switch custom-switch-off-default custom-switch-on-teal" >
                                        @php 
                                            $field = 'repeat'; 
                                        @endphp
                                            {{ Form::checkbox($field, true, null ,['id' => $field, 'class' => 'custom-control-input', "data-toggle" => "collapse", "href" => "#repeatCollapse", "role"=>"button",
                                            "aria-expanded" => "false", "aria-controls" => "repeatCollapse"]) }}
                                            {{ Form::label($field, 'Repeat', ['class' => 'custom-control-label']) }}
                                        </div>
                                        
                                        <div class="collapse pt-5" id="repeatCollapse">

                                            @php 
                                                $days_list = [
                                                    1 => 'Mon',
                                                    2 => 'Tue',
                                                    3 => 'Wed',
                                                    4 => 'Thu',
                                                    5 => 'Fri',
                                                    6 => 'Sat',
                                                    0 => 'Sun',
                                                ];
                                            @endphp
                                            <div class="form-group">
                                                {{ Form::label('days', 'Week days:') }}
                                                <div class="row justify-content-center  days-list px-3">
                                                    
                                                    @foreach ($days_list as $key=>$day)
                                                    <div class="col p-1">
                                                        {{ Form::checkbox('days['.$key.']', $day, false, ['id' => 'days-'.$key, 'class' => 'repeat-days-checkbox checkbox-line']) }}
                                                        {{ Form::label('days-'.$key, $day) }}
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-6">
                                                    @php $field = 'repeat_from_date'; @endphp
                                                    {{ Form::label($field, 'From:') }}
                                                    {{ Form::text($field, null,['class' => 'form-control', 'id' => $field , 'disabled']) }}
                                                </div>

                                                <div class="form-group col-6">
                                                    @php $field = 'repeat_until_date'; @endphp
                                                    {{ Form::label($field, 'To:') }}
                                                    {{ Form::text($field, null,['class' => 'form-control', 'id' => $field ]) }}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-footer mt-5">
                                    <a href="{{route('psych.working-hours')}}" class="btn btn-default">Cancel</a>
                                    {!! Form::submit('Save', ['id'=> 'submit_button', 'class' => 'btn btn-info
                                    float-right']) !!}
                                </div>

                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>


    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection


@push('scripts')
<script>
    $(document).ready(function(){
        //https://eonasdan.github.io/bootstrap-datetimepicker/

        
        
        var datePicker = $('#datetimepicker'),
            workingHoursForm = $('#workinghours-form'),
            response_div = $('.form-response'),
            renderDiv = $('#render-div'),
            dateTitle = renderDiv.find('.date-title'),
            repeatFromDate = renderDiv.find('#repeat_from_date'),
            workingHoursRender = renderDiv.find('.working-hours'),
            noWorkingHoursNotice = renderDiv.find('.no-working-hours')
            tableHeader = $('.table-headers'),
            repeatDatePicker = $('#repeat_until_date');

        datePicker.datetimepicker({
            inline: true,
            viewMode:'days',
            format: 'L',
            minDate: "{{ $min_date }}",
            maxDate: "{{ $max_date }}",
            icons: {
                previous: 'fas fa-angle-left',
                next: 'fas fa-angle-right',       
            },

        }).on('dp.change', function(e){ 
            getWorkingHours(e.date);
        });

        getWorkingHours();

        // Disable month and year change
        $('.picker-switch').click(function(e){
            e.stopPropagation();
        });

        

        

        // Add Working period 
        $('.add-period-btn').click(function(){    
            var totalInputs = workingHoursRender.find('tr.period-row').length;
            if(totalInputs < 4){ // max periods per day
                var order = totalInputs+1;
                $.ajax({
                    url: "{{ route('psych.working-hours.get_input_template') }}",
                    method: 'GET',
                    data: { order: order },
                    dataType: "json",
                    success: function (data) {
                        if (typeof (data.input_template) != "undefined" && data.input_template != null ) { 
                            noWorkingHoursNotice.hide();  
                            workingHoursRender.append(data.input_template);
                            tableHeader.show();
                        }
                    }
                });
            }
        });
        // Remove a period
        $(document).on('click', '.remove-period-btn', function(e){
            $(e.target).parents('tr.period-row').remove();
            if(!workingHoursRender.find('tr.period-row').length){
                noWorkingHoursNotice.show();
                tableHeader.hide();
            }
        });

        

        workingHoursForm.submit(function(event){
            event.preventDefault();
            var form = $(this),
                form_data = $(this).serialize(),
                url = $(this).attr('action'),
                method = $(this).attr('method');

            response_div.empty();
            $.ajax({
                url: url,
                method: method,
                data: form_data,
                dataType: "json",
                success: function (data) {
                    if (data.error.length > 0) {
                        var error_html = '<div class="callout border-danger callout-danger text-danger">';
                        for (var count = 0; count < data.error.length; count++) {
                            error_html += '<div>' + data.error[count] + '</div>';
                        }
                        error_html += '</div>';
                        response_div.html(error_html);
                    }
                    else {
                        response_div.html('<div class="callout callout-success border-success text-success">' + data.success + '</div>');
                        resetRepeatSettings();
                        $('#repeat').prop('checked', false);
                        $('#repeatCollapse').collapse('hide')
                    }
                }
            });
        });
    

        function resetRepeatSettings(){
            
            repeatDatePicker.data("DateTimePicker").clear();
            $('.repeat-days-checkbox').iCheck('uncheck');
        }

        function setRepeatDatePicker(mindate){
            if(repeatDatePicker.data("DateTimePicker") != "undefined" && repeatDatePicker.data("DateTimePicker") != null ){
            repeatDatePicker.data("DateTimePicker").destroy();
            }
            repeatDatePicker.datetimepicker({
           
                allowInputToggle: true,
                useCurrent: false,
                viewMode:'days',
                
                format: 'DD-MM-YYYY',
                minDate: mindate,
                viewDate: mindate,
                maxDate: "{{ $max_date }}",
                icons: {
                    previous: 'fas fa-angle-left',
                    next: 'fas fa-angle-right',       
                },
        
            });
        }

        function getWorkingHours(date = null)
        {
            response_div.empty();
            if(!date){
                date = datePicker.data("DateTimePicker").viewDate();
            }
            dateTitle.text(date.format('ddd, MMM D'));
            repeatFromDate.val(date.format('DD-MM-YYYY'));
            
            setRepeatDatePicker(date.format());
            
            
            $.ajax({
                url: "{{ route('psych.working-hours.get_data') }}",
                method: 'GET',
                data: { date: date.format('YYYY-MM-DD') },
                dataType: "json",
                success: function (data) {
                    if (typeof (data.working_hours) != "undefined" && data.working_hours != null ) {   
                        
                        workingHoursRender.find('tr.period-row').remove();
                        if(data.working_hours.length > 0){
                            noWorkingHoursNotice.hide();
                            workingHoursRender.append(data.working_hours); 
                            tableHeader.show();
                        }else{
                            noWorkingHoursNotice.show();
                            tableHeader.hide();
                        }  
                    }
                }
            });
        }
    });
</script>
@endpush