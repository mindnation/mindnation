<tr class="period-row">
        <td class="py-1 align-middle">
            {{ Form::select('working_hours['.$order.'][start]',$options, isset($start_time)?$start_time:null, ['placeholder' => 'Start Time...', 'class' => 'form-control form-control-sm']) }}
        </td>
        <td class="py-1 align-middle">
            {{ Form::select('working_hours['.$order.'][finish]',$options, isset($finish_time)?$finish_time:null, ['placeholder' => 'Finish Time...', 'class' => 'form-control form-control-sm']) }}
        </div>
        <td class="px-0 py-1 align-middle" style="width: 10px">
            <i class="far fa-times-circle remove-period-btn btn text-danger px-0"></i>
        </td>
    </div>
</tr>