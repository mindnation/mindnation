<div class="px-5  mt-3 mb-7">
    <div class="row pb-3 pb-md-0">
        <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Name:</div>
        <div class="col-12 col-md-8 col-lg-9">{{$member->name()}}</div>
    </div>
    <div class="row pb-3 pb-md-0">
        <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Gender:</div>
        <div class="col-12 col-md-8 col-lg-9">{{$member->getGender()}}</div>
    </div>
    <div class="row pb-3 pb-md-0">
        <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Birthdate:</div>
        <div class="col-12 col-md-8 col-lg-9">{{$member->getMetaValue('birth_date')}}</div>
    </div>
    <div class="row pb-3 pb-md-0">
        <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Email:</div>
        <div class="col-12 col-md-8 col-lg-9">{{$member->email}}</div>
    </div>
    <div class="row pb-3 pb-md-0">
        <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Contact Number:</div>
        <div class="col-12 col-md-8 col-lg-9">{{$member->getMetaValue('contact_number')}}</div>
    </div>
</div>




<div class="h4">Onboarding Survey</div>

@php
$survey = $user_survey_onboarding->survey;
$user_answer = $user_survey_onboarding->user_answers()->get();
@endphp

@foreach ($survey->question_categories()->ordered() as $category)
<div class="mb-4">
    <div class="h5 mb-3 text-purple">{{$category->title}}</div>
    @foreach ($category->questions()->ordered() as $question)
    @if($question)
    <div class="mb-3">

        @switch($question->type)
        @case('radio')
        <span class="h6">- {{$question->title}}</span>
        @php $answer = $user_survey_onboarding->user_answers()->where('question_id',$question->id)->first()->answer
        @endphp
        @if($answer)
        <span class="text-orange ml-1">{{ $answer->title }}</span>
        @endif
        @break
        @case('issues')
        <span class="h6">- {{$question->title}}</span>
        @php
        $user_issues = unserialize($member->getMetaValue('issues'));
        @endphp
        <span class="text-orange">{{ implode(", ",$user_issues) }}</span>
        @break
        
        @case('emergency_contact')
        <div class="row pb-3 pb-md-0">
            <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Name:</div>
            <div class="col-12 col-md-8 col-lg-9">{{$member->getMetaValue('emergency_contact_name')}}
            </div>
        </div>
        <div class="row pb-3 pb-md-0">
            <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Relationship:</div>
            <div class="col-12 col-md-8 col-lg-9">
                {{$member->getMetaValue('emergency_contact_relationship')}}</div>
        </div>
        <div class="row pb-3 pb-md-0">
            <div class="col-12 col-md-4 col-lg-3 pb-0 pb-md-3 h6">Contact Number:</div>
            <div class="col-12 col-md-8 col-lg-9">
                {{$member->getMetaValue('emergency_contact_number')}}</div>
        </div>
        @break
        @default

        @endswitch
    </div>
    @endif
    @endforeach
</div>
@endforeach

<div class="card my-5">
    <div class="card-header h4  px-0">Consultation History</div>
    <div class="card-body p-0">
        <div class="table-responsive-wrapper">
            <table class="table table-striped table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                       <th>Time</th>
                        <th>Professional</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($appointments as $appointment)
                    @php
                    $carbon_start_time = $appointment->getCarbonStart();
                    @endphp
                    <tr>
                        <td>{{$carbon_start_time->format('d/m/Y')}}</td>
                        <td>{{$carbon_start_time->format('g:i A')}}</td>
                        <td>{{$appointment->psych->name()}}</td>
                        <td>{{$appointment->getStatus()}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>