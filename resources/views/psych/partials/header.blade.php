<nav class="main-header navbar navbar-expand navbar-light navbar-white">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('psych')}}">
            <img src="{{ asset('images/logo/mindnation-logo.png') }}" class="logo" alt="MindNation Logo">
        </a>
        <a class="dashboard-name" href="{{ route('psych')}}">
            Psych Dashboard
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
  
        </button>

  

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                
                @include('partials.user-dropdown')
                
            </ul>
        </div>
    </div>
</nav>