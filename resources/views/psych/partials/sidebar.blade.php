<!-- Main Sidebar Container -->
<aside class="sidebar-light-primary border-right">
   
    <div class="nav-header py-3"></div>
    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="my-3 overflow-hidden">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    @php $route = 'psych.dashboard'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>{{ __('Dashboard') }}</p>
                    </a>
                </li>

                <li class="nav-item">
                    @php $route = 'psych.appointments'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-notes-medical"></i>
                        <p>{{ __('Consultations') }}</p>
                    </a>
                </li>

                <li class="nav-item">
                    @php $route = 'psych.calendar'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-calendar-alt"></i>
                        <p>{{ __('Calendar') }}</p>
                    </a>
                </li>


                <li class="nav-item">
                    @php $route = 'psych.working-hours'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-business-time"></i>
                        <p>{{ __('Working Hours') }}</p>
                    </a>
                </li>

                <li class="nav-item has-treeview {{areActiveRoutes(['psych.account.profile', 'psych.account.password'], "menu-open")}}">
                    @php $route = 'psych.account'; @endphp
                    <a href="#" class="nav-link {{areActiveRoutes(['psych.account.profile', 'psych.account.password'])}}">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>{{ __('My Account') }}</p>
                        <i class="fas fa-angle-left right"></i>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            @php $route = 'psych.account.profile'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                <i class="nav-icon fas fa-user"></i>
                                <p>{{ __('Profile') }}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            @php $route = 'psych.account.password'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                <i class="nav-icon fas fa-key"></i>
                                <p>{{ __('Change Password') }}</p>
                            </a>
                        </li>

                    </ul>
                </li>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

