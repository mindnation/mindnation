<footer class="bg-dark text-white pt-9 pb-10 border-multicolor-bottom">
    <div class="container px-sm-1">
        <div class="row justify-content-center">
            <div class="col-logo">

                <div class="footer-logo mb-4 mb-xs-3">
                    <img src="{{ asset('images/logo/mindnation-logo-h.png') }}" class="logo w-100"
                        alt="MindNation Logo">
                </div>
                <div class="font-weight-light font-size-sm mb-4">
                    Copyright © 2020 MindNation.<br>
                    All rights reserved.
                </div>
                <ul class="list-inline social-links">
                    <li>
                        <a href="https://facebook.com/themindnation" target="_blank">@svg('images/icons/fb.svg', 'fb')</a>
                    </li>
                    <li>
                        <a href="https://twitter.com/themindnation" target="_blank">@svg('images/icons/tw.svg', 'twitter')</a>
                    </li>
                    <li>
                        <a href="https://instagram.com/themindnation" target="_blank">@svg('images/icons/ig.svg', 'ig')</a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/themindnation/" target="_blank">@svg('images/icons/in.svg', 'linkedin')</a>
                    </li>

                </ul>

            </div>
            <div class="col-address">
                <div class="font-weight-bold mb-2 no-mobile">Our Office</div>

                <div class="address-wrap">
                    <div class="pin-wrap">@svg('images/icons/pin.svg', 'pin')</div>

                    <div class="address-text font-size-sm">
                        <div class="mb-2">The MindNation HQ</div>
                        <div class="font-weight-light text-secondary pr-2">
                            MANILA, Philippines<br>
                            SINGAPORE, Singapore
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-footer-links">
                <div class="row footer-nav justify-content-center justify-content-lg-end">
                    {{-- <div class="col-3 col-links">
                        <div class="font-weight-bold mb-3">About</div>
                        <ul class="footer-links font-weight-light font-size-sm">
                            <li><a href="#">Prices</a></li>
                            <li><a href="#">Ressources</a></li>
                            <li><a href="#">Careers</a></li>
                        </ul>
                    </div> --}}
                    <div class="col-12 col-sm-3 col-links">
                        <div class="font-weight-bold mb-3">Partners</div>
                        <ul class="footer-links font-weight-light font-size-sm">
                            <li><a class="hash-link" href="{{(!isActiveRoute('front.home'))?route('front.home'):''}}#benefits">Benefits</a></li>
                            <li><a class="hash-link" href="{{(!isActiveRoute('front.home'))?route('front.home'):''}}#solutions">Solutions</a></li>
                            <li><a class="hash-link" href="http://blog.themindnation.com">Blog</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-3 col-links">
                        <div class="font-weight-bold mb-3">Members</div>
                        <ul class="footer-links font-weight-light font-size-sm">
                            <li><a href="#" data-trigger="auth" data-target="#registerDiv">Get Started</a></li>
                            <li><a href="#" data-trigger="auth" data-target="#loginDiv">Log in</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-3 col-links">
                        <div class="font-weight-bold mb-3">Support</div>
                        <ul class="footer-links font-weight-light font-size-sm">
                            <li><a href="{{ route('front.terms') }}">Terms</a></li>
                            <li><a href="{{ route('front.privacy-policy') }}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>