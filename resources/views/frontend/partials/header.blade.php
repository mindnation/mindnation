<header>
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container p-0">
            <a class="navbar-brand" href="{{ route('front.home')}}">
                <img src="{{ asset('images/logo/mindnation-logo.png') }}" class="logo" alt="MindNation Logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <div class="navbar-toggler-zone nav-collapsed-only" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"></div>

                <button class="navbar-toggler nav-collapsed-only" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                </button>
                <div class="nav-wrap">
                    <div class="wrap-bg-shape nav-collapsed-only">
                        @svg('images/svg/shape-9.svg', ' bg-shape  bg-shape-top')
                        @svg('images/svg/shape-8.svg', ' bg-shape fill-purple bg-shape-1')
                        @svg('images/svg/shape-8.svg', 'bg-shape fill-teal bg-shape-2')
                        @svg('images/svg/shape-7.svg', 'bg-shape fill-yellow bg-shape-3')
                    </div>
                    <a class="navbar-brand nav-collapsed-only infront-of-bg-shape" href="{{ route('front.home')}}">
                        <img src="{{ asset('images/logo/mindnation-logo.png') }}" class="logo" alt="MindNation Logo">
                    </a>


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav font-size-sm infront-of-bg-shape">
                        <li class="nav-item">
                            @php $route = 'front.about-us'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                <p>{{ __('About Us') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            @php $route = 'front.for-members'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                <p>{{ __('Benefits') }}</p>
                            </a>
                        </li>
                        <!-- Authentication Links -->
                        <li class="nav-item">
                            <div class="btn-contact btn btn-sm btn-teal btn-min-width-sm" data-toggle="modal"
                                data-target="#contactModal">{{ __('Talk to Us') }}</div>
                        </li>
                        @guest
                        <!-- <li class="nav-item">
                            <div class="btn btn-sm btn-yellow btn-login btn-min-width-sm mr-0 " data-trigger="auth"
                                data-target="#loginDiv">{{ __('Login') }}</div>
                        </li> -->
                        @if (Route::has('register'))
                        {{-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li> --}}
                        @endif
                        @else
                        @include('partials.user-dropdown')
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>