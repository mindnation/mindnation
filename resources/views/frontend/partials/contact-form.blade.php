<div class="contact-form-wrap text-center text-white">
    <div class="h2 text-white mb-5 mb-sm-8">Get in touch with us today!</div>
   
    
    <div class="row justify-content-center">
        <div class="col-form">
            <div class="">
                {{ Form::open(array('route' => 'contact-enquiry.post', 'class' => 'contact-form')) }}
                <div class="form-response mb-4 text-success" style="display:none;"></div>
                <div class="row px-2">
                    <div class="col-12 col-sm-4 px-2 form-group mb-3 mb-md-4">
                        {{ Form::text('first_name', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'First Name']) }}
                    </div>
                    <div class="col-12 col-sm-4 px-2 form-group mb-3 mb-md-4">
                        {{ Form::text('last_name', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Last Name']) }}
                    </div>
                    <div class="col-12 col-sm-4 px-2 form-group mb-3 mb-md-4">
                        {{ Form::email('email', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Email']) }}
                    </div>
                </div>
                <div class="row px-2">
                    <div class="col-12 col-sm-4 px-2 form-group mb-3 mb-md-4">
                        {{ Form::text('company_name', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Company Name']) }}
                    </div>
                    <div class="col-12 col-sm-4 px-2 form-group mb-3 mb-md-4">
                        {{ Form::text('role', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Your Role']) }}
                    </div>
                    <div class="col-12 col-sm-4 px-2 form-group mb-3 mb-md-4">
                        {{ Form::text('contact_number', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Contact Number']) }}
                    </div>
                </div>
                <div class="row px-2">
                    <div class="col px-2 form-group mb-3 mb-md-4">
                        {{ Form::text('number_of_employees', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'No. of Employees']) }}
                    </div>
                </div>
                <div class="row px-2">
                    <div class="col px-2 form-group mb-3 mb-md-4">
                        {{ Form::textarea('message', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'How can we help ?', 'rows' => 6]) }}
                    </div>
                </div>
                <div class="mt-3">
                    {!! Form::submit('Send', [ 'class' => 'btn btn-lg
                    btn-min-width-lg mx-auto '.(isset($btn_class)?$btn_class:'btn-purple')]) !!}
                </div>
                <div class="loading mt-3" style="display:none;">
                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                    <span class="sr-only">Loading...</span> 
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
