<div class="employees-calculator">
    <div class="row justify-content-center">
        <div class="col-card-slider">
            <div class="slider-card card border-multicolor-top shadow-lg infront-of-bg-shape">
                <div class="card-body  text-center">
                    <div class="row justify-content-center">
                        <div class="col-text-slider">
                            <h2 class="slider-title h5">How many employees are in your organization?</h2>
                        </div>
                    </div>

                    <div id="employees-slider" >
                        <button class="slider-cursor">
                            <div class="cursor-value h5 m-0"></div>
                        </button>
                        <div class="lower-fill"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-start align-items-end">
        <div class="col-image order-2 order-md-1 col-with-shape">
            <div class="wrap-bg-shape">
                @svg('images/svg/shape-6.svg', 'bg-shape fill-yellow', 'xMinYMin meet')
            </div>
            <div class="vector-image infront-of-bg-shape">
                <img src="{{ asset('images/frontend/stressed.png') }}" class="stressed">
            </div>
        </div>
        <div class="col-text order-1 order-md-2 infront-of-bg-shape">
            <div class="employees-stats">
                    <div class="mb-5">
                        <div class="h3 text-purple mb-3"><span id="stats-difficulties"></span></div>
                        <p>
                            Colleagues are currently struggling with some form of mental health issue.
                        </p>
                    </div>
                    <div class="mb-5">
                        <div class="h3 text-blue mb-3"><span id="stats-days"></span></div>
                        <p>
                            Days lost per year due to work - related stress, depression and anxiety.
                        </p>
                    </div>
                    <div class="">
                        <div class="h3 text-teal mb-3"><span>₱</span><span id="stats-cost"></span></div>
                        <p>
                            The annual cost of ill-mental health across absenteeism, loss of productivity and staff turnover.
                        </p>
                    </div>  
            </div>
            <div class="text-gray-light font-weight-light font-size-sm text-center text-md-right mb-3"><em>*</em> figures are estimated and based on Philippine research</div>
        </div>
        
    </div>

</div>




@push('scripts')
<script>
    var slider = $('#employees-slider'),
        cursor = slider.find('.slider-cursor'),
        cursorValue = cursor.find('.cursor-value'),
        lowerFill = slider.find('.lower-fill'),
        stats_difficulties = $('#stats-difficulties'),
        stats_days = $('#stats-days'),
        stats_cost = $('#stats-cost');
        cursorMoving = false;
    
    const multiplier_difficulties = 0.32;
    const multiplier_days = 4.5;
    const multiplier_cost = 20535.5;
    const initValue = 50.5;

    
    slider.bind('touchstart mousedown',function(e) {cursorMove(); onCursorMove(e)});
    $(document).bind('touchend mouseup',cursorStop);
    $(document).bind('mousemove', onCursorMove);

    slider.bind('touchmove', function(e) {
        e.preventDefault();

        onCursorMove(e.touches[0]);
    });

   $(window).resize(initSlider);
    
    initSlider();
    
    function cursorMove(){
        cursorMoving = true;
       
    }
    function cursorStop(){
        cursorMoving = false;
        
    }    

    function onCursorMove(e){
        if(cursorMoving){
            MoveCursor(e);
        } 
    }

    function initSlider(){
        var sliderWidth = slider.outerWidth(),
            cursorWidth = cursor.outerWidth();
        updateSlider(initValue, sliderWidth*(initValue/100) - cursorWidth*(initValue/100));
    }

    
    function MoveCursor(e){
        var sliderWidth = slider.outerWidth(),
            cursorWidth = cursor.outerWidth(),
            mousePosX = e.pageX - slider.offset().left - cursorWidth/2,
            minPos = 0,
            maxPos = sliderWidth-cursorWidth,
            cursorPosX = Math.max(minPos, Math.min(mousePosX,maxPos)),
            cursorPercent = cursorPosX * 100 / (maxPos-minPos);
        
            updateSlider(cursorPercent, cursorPosX);         
    }

    function updateSlider(cursorPercent, cursorPosX){
        
        var employees = numberOfEmployees(cursorPercent),
            cursorWidth = cursor.outerWidth();
        cursorValue.text(format(employees));
        updateStats(employees);

        cursor.css('left', cursorPosX+'px');
        lowerFill.css('width', (cursorPosX+cursorWidth/2)+'px');
    }

    function numberOfEmployees(percent) {
        const min = Math.log(10);
        const max = Math.log(100000);

        const scale = (max - min) / 99;
        return Math.max(Math.round(Math.exp(min + scale * (percent - 1))),10);
    }

    function updateStats (employees){
        var difficulties = format(employees*multiplier_difficulties);
        var days = format(employees*multiplier_days);
        var cost = format(employees*multiplier_cost);

        stats_difficulties.text(difficulties);
        stats_days.text(days);
        stats_cost.text(cost);  
    }

    function format(num){
        num = Math.round(num);
        var string = num.toString();
        return string.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }

</script>
@endpush