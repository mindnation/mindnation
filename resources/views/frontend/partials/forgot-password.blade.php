<div id="forgotPasswordDiv" class="auth-div">
    <div class="h4 mb-1">Forgot Password</div>
    <div class="form-response mb-4 text-success" style="display:none;"></div>

    
    <div class="login-card card border-multicolor-top shadow-lg mt-6">
        <div class="card-body pb-5">
            <a href="#" class="back-btn font-size-sm" data-trigger="auth-div" data-target="#loginDiv">&lt; Sign In</a>

            {{ Form::open(array('route' => 'password.email', 'class' => 'auth-form')) }}
            <div class="form-group mb-4">
                {{ Form::email('email', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Email']) }}
            </div>
            <div class="mt-4">
                {!! Form::submit('Send Password Reset Link', ['class' => 'submit-button btn btn-lg
                btn-min-width-lg mx-auto btn-teal']) !!}
            </div>
            <div class="font-size-sm mt-6">
                <a href="#" data-trigger="auth-div" data-target="#loginDiv">Sign in</a>
            </div>

            {{ Form::close() }}
        </div>
    </div>
</div>