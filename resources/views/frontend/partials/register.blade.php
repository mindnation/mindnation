<div id="registerDiv" class="auth-div">
    <div class="h4 mb-1">Create your account</div>
    <div class="h6 font-weight-light">Enter an authorised email to get started</div>
    <div class="login-card card border-multicolor-top shadow-lg mt-6">
        <div class="card-body">
            {{ Form::open(array('route' => 'register', 'class' => 'auth-form')) }}
            <div class="form-response mb-4 text-success" style="display:none;"></div>
            <div class="form-group mb-4">
                {{ Form::email('email', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Email']) }}
            </div>
            <div class="form-group mb-4">
                {{ Form::text('first_name', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'First Name']) }}
            </div>
            <div class="form-group mb-4">
                {{ Form::text('last_name', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Last Name']) }}
            </div>
            <div class="form-group mb-4">
                {{ Form::text('birth_date', null ,['class' => 'form-control form-control-lg', 'id' => 'bithdatepicker', 'placeholder' => 'Date of Birth']) }}
            </div>
            <div class="form-group mb-4">
                {{ Form::select('gender', App\User::getGenderOptions(), null ,['class' => 'form-control form-control-lg placeholder', 'placeholder' => 'Gender']) }}
            </div>
            <div class="form-group mb-4">
                {{ Form::text('contact_number', null,['class' => 'form-control form-control-lg', 'placeholder' => 'Contact Number']) }}
            </div>

            <div class="form-group mb-4">
                {{ Form::password('password',['class' => 'form-control form-control-lg', 'placeholder' => 'Password']) }}
            </div>
            <div class="form-group mb-3">
                {{ Form::password('password_confirmation',['class' => 'form-control form-control-lg', 'placeholder' => 'Confirm Password']) }}
            </div>
            <div class="mb-2 form-group form-check text-left">
                {{ Form::checkbox('terms', '1', false ,['id' => 'terms', 'class' => 'form-check-input mr-2']) }}
                <label class="form-check-label font-size-sm" for="terms">I agree to MindNation's <a href="{{ route('front.terms') }}" target="_blank">Terms & Conditions</a> and <a href="{{ route('front.privacy-policy') }}" target="_blank">Privacy Policy.</a></label>
            </div>
            
           
            <div class="mt-4">
                {!! Form::submit('Register', ['class' => 'submit-button btn btn-lg
                btn-min-width-lg mx-auto btn-teal']) !!}
            </div>
            <div class="font-size-sm mt-3">
                Already have an account? <a href="#" data-trigger="auth-div" data-target="#loginDiv">Sign in</a>
            </div>

            {{ Form::close() }}
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        //https://eonasdan.github.io/bootstrap-datetimepicker/
        var datePicker = $('#bithdatepicker');

        datePicker.datetimepicker({
        //inline:true,
        allowInputToggle: true,
        useCurrent: false,
        viewMode:'years',
        format: 'DD/MM/YYYY',
        minDate: moment().subtract(100, 'years'),
        maxDate: moment(),
        icons: {
            previous: 'fas fa-angle-left',
            next: 'fas fa-angle-right',       
        },

        });
    });

</script>
@endpush