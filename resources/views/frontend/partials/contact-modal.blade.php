<!-- Modal -->
<div class="modal fade" id="contactModal"  tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="wrap-bg-shape" >

        @svg('images/svg/shape-2.svg', 'fill-blue bg-shape contact-bg-shape-1')
        @svg('images/svg/shape-2.svg', 'fill-orange bg-shape contact-bg-shape-2')


        <div class="logo-wrap">
            <a href="#" data-dismiss="modal">
                <img src="{{ asset('images/logo/mindnation-logo.png') }}" class="logo" alt="MindNation Logo">
            </a>
        </div>
        <div class="modal-close" data-dismiss="modal"></div>


        <div class="modal-dialog  modal-xl" role="document">
            <div class="modal-content">
                <div class="wrap-bg-shape">
                    @svg('images/svg/shape-2.svg', 'fill-teal bg-shape contact-content-bg-shape-1')
                    @svg('images/svg/shape-2.svg', 'fill-yellow bg-shape contact-content-bg-shape-2')
                </div>
                <div class="modal-body">
                    @include('frontend.partials.contact-form', ['btn_class' => 'btn-orange'])
                </div>
            </div>
        </div>
    </div>
</div>

