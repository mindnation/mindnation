<div class="login-close" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false"
    aria-controls="loginCollapse mainCollapse"></div>

<div class="container px-4" style="position:relative;">
    <div class="wrap-bg-shape">
        @svg('images/svg/shape-2.svg', 'bg-shape login-bg-shape-1')
        @svg('images/svg/shape-2.svg', 'bg-shape login-bg-shape-2')
    </div>


    <div class="login-row row min-vh-100 justify-content-center align-items-center">
        <div class="col-login text-center infront-of-bg-shape">
            @include('frontend.partials.login')
            {{--  @include('frontend.partials.forgot-password')  --}}
            @include('frontend.partials.register')
        </div>
    </div>

</div>

@push('scripts')
<script>
    $(document).ready(function(){
        
    });
</script>
@endpush