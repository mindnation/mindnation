<div id="loginDiv" class="auth-div show">
    <div class="h4 mb-1">Login to your account</div>
    
    <div class="login-card card border-multicolor-top shadow-lg mt-6">
        <div class="card-body pb-3">
            {{ Form::open(array('route' => 'login', 'class' => 'auth-form')) }}
            <div class="form-response mb-4 text-success" style="display:none;"></div>
            <div class="form-group mb-4">
                {{ Form::email('email', null ,['class' => 'form-control form-control-lg', 'placeholder' => 'Email']) }}
            </div>
            <div class="form-group mb-2">
                {{ Form::password('password',['class' => 'form-control form-control-lg', 'placeholder' => 'Password']) }}
            </div>
            <div class="font-size-sm text-left px-3">
                <a href="{{ route('password.request') }}" >Forgot Password?</a>
            </div>

            <div class="mt-4">
                {!! Form::submit('Sign in', ['class' => 'submit-button btn btn-lg
                btn-min-width-lg mx-auto btn-teal']) !!}
            </div>
            <div class="font-size-sm mt-3">
                Need an account? <a href="#" data-trigger="auth-div" data-target="#registerDiv">Create now</a>
            </div>

            <div class="font-size-sm mt-6">
                <a href="{{ route('front.about-us') }}">Learn More</a>
            </div>

            {{ Form::close() }}
        </div>
    </div>
</div>