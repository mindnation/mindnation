{{-- GET THE SUPPORT --}}
<div class="row justify-content-center">
    <div class=" col-getsupport col-12 col-md-10 col-lg-9">
        <div class="get-support-card card bg-purple">
            <div class="card-body text-center text-white">
                <h2 class="support-title h2 text-white">Get the support you need today.</h2>
                <div class="font-weight-light">Our team is ready when you are.</div>
                @if(isset($route) && isset($btn_title))
                    <a href="{{ $route }}" class="btn btn-yellow btn-lg btn-min-width-lg">
                        {{ $btn_title }}
                    </a>

                @else
                    <div class="btn btn-yellow btn-lg btn-min-width-lg" data-toggle="modal" data-target="#contactModal">Get Started</div>
                @endif
            </div>
        </div>
    </div>
</div>
{{-- END GET THE SUPPORT --}}