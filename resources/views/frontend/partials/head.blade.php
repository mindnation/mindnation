
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">


    @php
        $description = 'Mind Nation is a mental wellness platform for the workplace. We provide an end-to-end solution for your company’s mental health needs.';
    @endphp

    <meta name="description" content="{{ $description }}">
    <meta name="keywords" content="mental wellness,mental solution,health platform">

    {{-- <link rel="canonical" href="{{ url()->current() }}"/> --}}

    <!-- Open Graph data -->
    <meta property="og:title" content="@yield('title')" /> 
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="{{ asset('images/logo/og-image.jpg') }}" />
    <meta property="og:description" content="{{ $description }}" />
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    {{-- <meta property="fb:admins" content="Facebook numberic ID" /> --}}


    <link href="{{ asset('css/frontend.css') }}" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

</head>