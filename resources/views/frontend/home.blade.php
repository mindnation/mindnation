@extends('layouts.frontend')

@section('title',config('app.name', 'Laravel'))
@section('body_class', 'home')


@section('content')
{{-- HOME BANNER --}}

@if ($message = Session::get('success'))
<div class="callout callout-success text-success border-success py-2">
    <div>{{ $message }}</div>
</div>
@endif

    @if ($errors->any())
<div class="callout callout-danger text-danger border-danger  py-2">
    {{--  Something is missing. Please check and try again.  --}}
    @foreach ($errors->all() as $error)
        <div>{{ $error }}</div>
    @endforeach 
</div>
@endif 
<div class="bg-light container-bg-shape">
    <div class="section section-1 section-what-keeps-them container">
        <div class="row align-items-center justify-content-end">

            <div class="col-text infront-of-bg-shape">
                <h1 class="h6 text-orange mb-2 font-weight-normal">We're ready to help.</h1>
                <h2 class="h2 mb-4">
                    What keeps your employees from seeking mental health support?
                </h2>
                <div class="h6 text-blue mb-4 carousel-wrap font-weight-normal">
                    <div id="heroBulletCarousel" class="carousel slide carousel-fade" data-ride="carousel"
                        data-interval="3000" data-keyboard="false" data-pause="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                Lack of knowledge with mental health
                            </div>
                            <div class="carousel-item">
                                Limited access to consultation clinics
                            </div>
                            <div class="carousel-item">
                                Expensive consultation fees
                            </div>
                            <div class="carousel-item">
                                Long waiting times
                            </div>
                            <div class="carousel-item">
                                Inconvenient process and takes too long
                            </div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('front.about-us') }}" class="btn btn-orange btn-lg btn-min-width-lg mt-1">
                    Learn More
                </a>
            </div>
            <div class="col-image col-with-shape">
                <div class="wrap-bg-shape">
                    @svg('images/svg/shape-1.svg', 'bg-shape fill-purple', 'xMaxYMax slice')
                </div>
                <div class="vector-image infront-of-bg-shape">
                    {{-- @svg('images/svg/hero.svg', 'hero') --}}
                    <img src="{{ asset('images/frontend/hero.png') }}" class="hero">
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END HOME BANNER --}}
{{-- FEATURED IN --}}
<div class="section-featuredin container pt-6">
    <div class="text-center">
        <h2 class="h2 mb-4">Featured in</h2>
        <ul class="list-inline logo-list">
            <li class="list-inline-item"><img src="{{ asset('images/frontend/featuredin/rappler.png') }}"
                    class="rappler"></li>
            <li class="list-inline-item"><img src="{{ asset('images/frontend/featuredin/8list.png') }}"
                    class="eightlist"></li>
            <li class="list-inline-item"><img src="{{ asset('images/frontend/featuredin/wonder.png') }}" class="wonder">
            </li>
            <li class="list-inline-item"><img src="{{ asset('images/frontend/featuredin/nolisoli.png') }}"
                    class="nolisoli"></li>
            <li class="list-inline-item"><img src="{{ asset('images/frontend/featuredin/wim.png') }}" class="wim"></li>
            <li class="list-inline-item"><img src="{{ asset('images/frontend/featuredin/cnn.png') }}" class="cnn"></li>
            <li class="list-inline-item"><img src="{{ asset('images/frontend/featuredin/tripzilla.png') }}"
                    class="tripzilla"></li>
        </ul>

    </div>
</div>

{{-- END FEATURED IN --}}
{{-- RANGE SLIDER EMPLOYEES --}}
<div class="container-bg-shape">
    <div class="section  section-employees-calculator container">
        @include('frontend.partials.slider')
    </div>
</div>
{{-- END RANGE SLIDER EMPLOYEES --}}
{{-- THRIVING EMPLOYEES --}}
<div class="container-bg-shape">
    <div class="section section-thriving-employees bg-light">
        <div class="container">
            <div class="row justify-content-end align-items-center pt-6">

                <div id="benefits" class="col-text infront-of-bg-shape">
                    <h2 class="section-title h2">
                        Thriving employees make for thriving businesses.
                    </h2>
                    <p class="sub-title font-weight-light">
                        Healthier and happier teams are more productive
                        and motivated to perform better. They are also equipped
                        with grit and resilience in facing challenges.
                    </p>

                    <div class="row align-items-center row-cards">
                        <div class="col-cards">
                            <div class="thriving-card card shadow-lg-purple mb-5">
                                <div class="card-body text-center ">
                                    <div class="icon-wrap mx-auto pt-1"><img
                                            src="{{ asset('images/icons/wellness.png') }}">
                                    </div>
                                    <h3 class="h6 text-purple my-3">Boosted Wellness</h3>
                                    <p class="font-weight-light font-size-sm ">
                                        Implementing a health program means that you are committed in the wellness of
                                        your
                                        employees.
                                    </p>
                                </div>
                            </div>
                            <div class="thriving-card card shadow-lg-orange">
                                <div class="card-body text-center ">
                                    <div class="icon-wrap mx-auto pt-1"><img
                                            src="{{ asset('images/icons/culture.png') }}">
                                    </div>
                                    <h3 class="h6 text-orange my-3">Improved Culture</h3>
                                    <p class="font-weight-light font-size-sm ">
                                        Creating a culture of openness provides your people safe spaces for sustainable
                                        growth.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-cards">
                            <div class="thriving-card card shadow-lg-blue">
                                <div class="card-body text-center ">
                                    <div class="icon-wrap mx-auto pt-1"><img src="{{ asset('images/icons/team.png') }}">
                                    </div>
                                    <h3 class="h6 text-blue my-3">Satisfied Teams</h3>
                                    <p class="font-weight-light font-size-sm ">
                                        Allowing your teams access to mental health support when they need it assures
                                        them
                                        that they are cared for.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-with-shape col-image">
                    <div class="wrap-bg-shape">
                        @svg('images/svg/shape-6.svg', 'bg-shape fill-teal', 'xMaxYMax meet')
                    </div>
                    <div class="vector-image infront-of-bg-shape">
                        <img src="{{ asset('images/frontend/employees.png') }}" class="stressed">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END THRIVING EMPLOYEES --}}
{{-- END TO END SOLUTION --}}
<div class="container pt-6 pb-7 pb-md-10 px-1">

    <div class="px-4 infront-of-bg-shape">
        @include('frontend.partials.get-support', ['route'=> route('front.about-us'), 'btn_title'=>'Learn More' ])
    </div>


    <div id="solutions" class="pt-6">
        <div class="text-center">
            <h2 class="title-solution h2 mb-1">Our End-to-End solution</h2>

        </div>
        <div class="row row-cards eq-height">
            <div class="col-cards">
                <div class="solution-card card shadow-blue h-100">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-2 pr-0">
                                <div class="icon-wrap bg-blue-light counseling"
                                    style="background-image:url('/images/icons/counseling.svg')"></div>
                            </div>
                            <div class="card-col-text">
                                <h3 class="h5 text-blue mt-1 mb-2">Counseling & Therapy</h3>
                                <p class="font-size-sm mb-2 pr-3">
                                    Give your employees access to online counseling and therapy services through our
                                    easy-to-use platform.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-cards">
                <div class="solution-card card shadow-orange h-100">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-2 pr-0">
                                <div class="icon-wrap bg-orange-light analytics"
                                    style="background-image:url('/images/icons/analytics.svg')"></div>
                            </div>
                            <div class="card-col-text">
                                <h3 class="h5 text-orange mt-1 mb-2">Analytics</h3>
                                <p class="font-size-sm mb-2 pr-3">
                                    Understand how investing in mental health impacts your business with a subscription
                                    to our analytics platform.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-cards">
                <div class="solution-card card shadow-teal h-100">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-2 pr-0">
                                <div class="icon-wrap bg-teal-light culture"
                                    style="background-image:url('/images/icons/culture-drives.svg')"></div>
                            </div>
                            <div class="card-col-text">
                                <h3 class="h5 text-teal mt-1 mb-2">Culture Drives</h3>
                                <p class="font-size-sm mb-2 pr-3">
                                    Equip your team with useful mental wellness practices through our regular seminars
                                    and training.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-cards">
                <div class="solution-card card shadow-purple h-100">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-2 pr-0">
                                <div class="icon-wrap bg-purple-light crisis"
                                    style="background-image:url('/images/icons/crisis.svg')"></div>
                            </div>
                            <div class="card-col-text">
                                <h3 class="h5 text-purple mt-1 mb-2">Crisis Helplines</h3>
                                <p class="font-size-sm mb-2 pr-3">
                                    Strengthen your mental health program with our chatbot and care hotline for more
                                    emergent member concerns.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center">
            <div class="btn btn-blue btn-lg btn-min-width-lg mt-2 mx-auto" data-toggle="modal"
                data-target="#contactModal">Talk to Us</div>
        </div>

    </div>
</div>
{{-- END END TO END SOLUTION --}}
{{-- TESTIMONY --}}
<div class="bg-light pt-6 pb-5 section-testimony">
    <div class="container px-md-2">
        <div class="row align-items-center">
            <div class="col-text">
                <h2 class="h2 mb-3">What people say about us</h2>
                <div class="font-weight-light">
                    See how our quality mental health services are benefitting our users.
                </div>


                <a href="{{ route('front.about-us') }}" class="btn btn-purple btn-lg btn-min-width-lg mt-5 mx-auto">
                    Learn More
                </a>

            </div>
            <div class="col-testimonies">
                <div class="row">
                    <div class="col-12 col-md-9">
                        <div class="testimony-card card shadow mt-3 mb-3">
                            <div class="card-header border-bottom py-3 px-4">
                                <div class="icon-wrap align-middle d-inline-block mr-3 facebook"></div>
                                <div class="align-middle d-inline-block">
                                    <div class="h5 m-0">Deidre Joyce C. Dalawampu</div>
                                </div>
                            </div>
                            <div class="card-body px-4">
                                <p class="font-weight-light font-size-sm font-italic mb-1">
                                    "This works, just tried it. Made me feel so much better. You're not alone, ask for
                                    help."
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-9 offset-md-3 pl-md-0">
                        <div class="testimony-card card shadow my-3">
                            <div class="card-header border-bottom py-3 px-4">
                                <div class="icon-wrap align-middle d-inline-block mr-3 twitter"></div>
                                <div class="align-middle d-inline-block">
                                    <div class="h5 m-0">Dayann</div>
                                    <div class="font-size-sm">@BabiiBabsiii</div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="font-weight-light font-size-sm font-italic mb-1">
                                    "okay, so I chatted mindnation and I felt better. But, I know I need a professional
                                    to
                                    help me. Imma sleep for now, the help I received ease the demons somehow. Thank the
                                    heavens for people who make the world a btter place to live with. Keep doing the
                                    good
                                    work."
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-9 p-md-0">
                        <div class="testimony-card card shadow mt-3">
                            <div class="card-header border-bottom py-3 px-4 ">
                                <div class="icon-wrap align-middle d-inline-block mr-3 twitter"></div>
                                <div class="align-middle d-inline-block">
                                    <div class="h5 m-0">Mari #CenterFlight</div>
                                    <div class="font-size-sm">@strxwmrie</div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="font-weight-light font-size-sm font-italic mb-1">
                                    "So guys, @theMindNation's Free Online Psychological Consultation is LEGIT. I'm
                                    really
                                    happy. To make things easier for you, all you have to do is to email them at
                                    hello@themindnation.com with your Employment/ID certificate. Slots are limited I
                                    hope I
                                    can cope one."
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END TESTIMONY --}}

{{-- GET IN TOUCH FORM --}}

<div class="section-getintouch bg-orange py-3">
    <div class="container" style="position:relative;">
        <div class="wrap-bg-shape">
            @svg('images/svg/shape-2.svg', ' bg-shape getintouch-bg-shape-1')
            @svg('images/svg/shape-2.svg', 'bg-shape getintouch-bg-shape-2')
        </div>
        <div class="infront-of-bg-shape">
            @include('frontend.partials.contact-form')
        </div>
    </div>
</div>

{{-- END GET IN TOUCH FORM --}}


@endsection