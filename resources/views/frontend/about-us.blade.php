@extends('layouts.frontend')
@section('title','About Us | '.config('app.name', 'Laravel'))
@section('body_class', 'about-us')

@section('content')


{{-- ABOUT SECTION 1 --}}
<div class="container-bg-shape">
    <div class="section section-1 section-empowered-workforce container">
        <div class="row  justify-content-end ">
            <div class="col-text infront-of-bg-shape order-2 order-md-1">
                <h1 class="h2 mt-0 mt-md-10">An empowered workforce where mental wellness is valued.</h1>
            </div>

            <div class="col-12 col-md-6 order-1 order-md-2 pb-8 pt-10 pt-md-6 px-md-0 col-with-shape">
                <div class="wrap-bg-shape">
                    @svg('images/svg/shape-1.svg', 'bg-shape fill-blue','xMaxYMax slice')
                </div>
                <div class="vector-image infront-of-bg-shape">
                    <img src="{{ asset('images/frontend/sotcks-01.png') }}" class="image">
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END ABOUT SECTION 1 --}}
{{-- ABOUT SECTION 2 --}}
<div class="container-bg-shape">
<div class="section section-2 section-mission container">
    <div class="row justify-content-start">
        <div class="col-12 col-md-6 col-with-shape">
            <div class="wrap-bg-shape">
                @svg('images/svg/shape-6.svg', 'bg-shape fill-orange', 'xMinYMin slice')
            </div>
            <div class="vector-image infront-of-bg-shape">
                <img src="{{ asset('images/frontend/sotcks-03.png') }}" class="image">
            </div>
        </div>

        <div class="col-text infront-of-bg-shape">
            <h2 class="h2 mt-5">Our Mission</h2>
            <div class="font-weight-light mt-3">
                To eliminate the barriers of seeking help by providing total mental healthcare support to employees.
                Here at MindNation, we're making technology work for your wellness.
            </div>
        </div>
    </div>
</div>
</div>
{{-- END ABOUT SECTION 2 --}}

{{-- ABOUT SECTION 3 --}}
<div class="container-bg-shape">
<div class="section section-3 section-story container">
    <div class="row justify-content-end">
        <div class="col-text order-2 order-md-1 infront-of-bg-shape">
            <h2 class="h2 ">Our Story</h2>
            <div class="font-weight-light mt-3">
                It all started when two friends - one an advocate who has actively lobbied for mental health law and
                another, a suicide survivor who's battling with Bipolar disorder - reconnected one evening and came up
                with the idea after feeling frustrated on how inaccessible and exclusive seeking mental health is for
                many Filipinos. They swore on that night to dedicate their life's work on making mental health support
                reachable for all. And the rest is history.
            </div>
        </div>
        <div class="col-12 col-md-6 order-1 order-md-2 pt-5 pt-md-3 pr-md-0 col-with-shape">
            <div class="wrap-bg-shape">
                @svg('images/svg/shape-6.svg', 'bg-shape fill-yellow', 'xMinYMax slice')
            </div>
            <div class="vector-image infront-of-bg-shape">
                <img src="{{ asset('images/frontend/sotcks-02.png') }}" class="image">
            </div>
        </div>
    </div>
</div>
</div>
{{-- END ABOUT SECTION 3 --}}


<div class="bg-light py-5 section-approach">
    <div class="container">
        <div class="row">
            <div class="col-text offset-1">
                <h2 class="h2 mt-2 mt-md-5">Our Approach</h2>
                <div class="font-weight-light mt-3">
                    A needs - based assessment and implementation cycle that is right for your team
                </div>
            </div>
        </div>

        <div class="row row-cards justify-content-center mt-6 pb-4">
            <div class="col-cards">
                <div class="our-approach-list row eq-height">
                    <div class="col-card">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body ">
                                <div class="icon  mt-2 mt-md-3">@svg('images/icons/analyze.svg', 'fill-blue')</div>
                                <h3 class="h6 text-blue my-3">Analyze</h3>
                                <p class="font-weight-light font-size-md px-1 mb-2 mb-md-4">
                                    What exactly is needed by my employees?
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-card">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body">
                                <div class="icon  mt-2 mt-md-3">@svg('images/icons/educate.svg', 'fill-orange')</div>
                                <h3 class="h6 text-orange my-3">Educate</h3>
                                <p class="font-weight-light font-size-md px-1 mb-2 mb-md-4">
                                    How do I promote the importance of mental health in the workplace?
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-card">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body">
                                <div class="icon mt-2 mt-md-3">@svg('images/icons/consult.svg', 'fill-teal')</div>
                                <h3 class="h6 text-teal my-3">Consult</h3>
                                <p class="font-weight-light font-size-md px-1 mb-2 mb-md-4">
                                    How can I address my employees' mental health concerns?
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-card">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body">
                                <div class="icon  mt-2 mt-md-3">@svg('images/icons/track.svg', 'fill-purple')</div>
                                <h3 class="h6 text-purple my-3">Track</h3>
                                <p class="font-weight-light font-size-md px-1 mb-2 mb-md-4">
                                    How can we be proactive and work towards a healthier workplace culture?
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <h2 class="h1 mt-3 mt-md-5 mb-5 mb-md-5 mb-sm-3 text-center">Our Team</h2>

        <div class="row justify-content-center pb-3 pb-sm-5">
            <div class="col-lg-10">
                <div class="our-team-list row eq-height">
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/kana.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Kana Takahashi</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Founder, CEO</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                    Vice Chair for External Affairs,<br>
                                    Youth for Mental Health Coalition
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/salma.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Salma Sakr</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Chief Growth Officer</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                    Building Happy, Healthy and Empowered organizations from 10 years global experience
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/cat.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Cat Triviño</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Chief Marketing Officer</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                    Mental Resilience, Advocate and Speaker, CORA Philippines
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/piril.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Piril Paker</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Chief Insights and Analytics Officer</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                    Mental Health Advocate and Consumer Voice in the Company
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/daph.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Daph Bajas</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Head of Partnerships and Sales</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                   Wellness, People and Culture Advocate
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/miguel.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Miguel Darrow</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Head of Technology</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                  Making Your Life Easier as a Business Analyst and Quality Assurance
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/ron.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Dr. Ron del Castillo</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Scientific Advisor</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                  Stanford and<br>
                                  UCLA-backed Expert of Mental Health
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/yves.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Yves Zuniga</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Scientific Advisor</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                  1st to bring Mental Health NGO in the Philippines
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-team-member">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body px-1">
                                <div class="photo mt-2"><img src="{{ asset('images/frontend/team/mon.png') }}"></div>
                                <div class="h6 mt-4 mb-0">Monique Ong</div>
                                <div class="role mb-4 px-3 pt-1 font-size-md font-italic">Chairman</div>
                                <p class="font-weight-light font-size-sm px-3 mb-2">
                                    Traumatic Brain Injury Survivor and founder of TBI Spark NGO,<br>
                                    ex-Facebook; ex-P&G
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="section-getsupport mt-5 mt-md-10 mb-4 mb-md-5">
            @include('frontend.partials.get-support')
        </div>


    </div>
</div>

@endsection