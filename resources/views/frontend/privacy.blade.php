@extends('layouts.frontend')
@section('title','Privacy Policy | '.config('app.name', 'Laravel'))
@section('body_class', 'simple-page terms')

@section('content')

<div class="container">
    <h1 class="h2">Privacy Policy</h1>

<p><strong>Last Updated: </strong>April 10, 2020</p>
<p>MindPH is a company engaged in the business of operating MindNation, an information and communication technology (ICT) facility that enables users to connect to mental healthcare providers. MindPH respects every person&rsquo;s right to privacy and aims to comply with the requirements of all relevant privacy and data protection laws, particularly the Data Privacy Act of 2012 (DPA). We seek to strike a balance between personal privacy and the free flow of information, especially when pursuing our legitimate interests and when necessary to carry out our responsibilities as a business, as an employer, and/or as a contracting party.</p>
<p>In this Policy, the terms, &ldquo;data&rdquo; and &ldquo;information&rdquo; are used interchangeably. When we speak of &ldquo;personal data,&rdquo; the term includes the concepts of personal information, sensitive personal information, and privileged information.</p>
<p><strong>How we collect, acquire, and generate personal data</strong></p>
<ol>
<li><em>General policy</em></li>
</ol>
<p>We generally collect, acquire, and generate personal data only with the data subject&rsquo;s consent. Such personal data may be in written, verbal, digital, electronic, photographic, or audio and/or video recording format. In exceptional situations when the communication of consent is not possible, practicable, or necessary, we collect, acquire, and generate personal data only when permitted by law. In all cases, we collect, acquire, and generate personal data only in accordance with the DPA, the Safe Spaces Act (SSA), and other relevant laws, rules, and regulations.</p>
<ol start="2">
<li><em>Services</em></li>
</ol>
<p>We collect, acquire, and generate certain personal data of users of MindNation. Such personal data include but are not limited to names, ages, sexes, gender identities, affiliations, job and organizational positions, contact numbers, e-mail addresses, health-related information, audio recordings of voices, and photographs and video recordings of persons. Such personal data are gathered and generated for the legitimate use of mental healthcare providers, for our research and statistical analysis (in which case, personal data are aggregated and anonymized), and for other legitimate purposes.</p>
<ol start="3">
<li><em>Contracts</em></li>
</ol>
<p>We collect, acquire, and generate certain personal data of individuals or representatives of entities with which we enter and execute contracts and instruments. Such personal data include but are not limited to names, ages, addresses, contact numbers, e-mail addresses, signatures, and other personal data the collection or generation of which is necessary and related to the fulfillment of our contractual obligations or in order to take steps at the request of the individual or representative prior to entering into a contract.</p>
<ol start="4">
<li><em>Personnel</em></li>
</ol>
<p>We collect, acquire, and generate certain personal data of our employees, workers, consultants, service providers, and personnel. Such personal data include but are not limited to those recorded or contained in the following documents:</p>
<ol>
<li>Personal data sheet, biodata, resume, and/or curriculum vitae</li>
<li>Certificate of employment</li>
<li>National Bureau of Investigation (NBI) Clearance or police clearance</li>
<li>Birth certificate and other records at the civil registry</li>
<li>Social security details, including identification numbers at the Social Security System (SSS), Philippine Health Insurance Corp. (PhilHealth), and Home Development Mutual Fund (HDMF)</li>
<li>Tax-related information, such as Tax Identification Number (TIN) and Bureau of Internal Revenue (BIR) Forms Nos. 1902 and 2316</li>
<li>Transcript of records and other education-related documents</li>
<li>Bank account details</li>
<li>Professional or other licenses</li>
<li>Identification photographs</li>
</ol>
<p>Such personal data will be collected, acquired, or generated for purposes of hiring, contracting, admission, compensation, remuneration, benefits, and other employment, work, and service-related purposes. We may need to collect, acquire, or generate additional personal data in the course of employment, work, or service by our personnel for purposes of performance evaluation, disciplinary measures, and other requirements.</p>
<ol start="5">
<li><em>Unsolicited information</em></li>
</ol>
<p>&nbsp;</p>
<p>There may be instances when personal data are sent to or received by us even without our prior request. In such cases, we will determine if we can legitimately keep such personal data. If it is not related to any of our legitimate interests, we will immediately dispose of the personal data in a way that will safeguard the data subject&rsquo;s privacy. Otherwise, it will be treated in the same manner as personal data legitimately collected, acquired, or generated by us.</p>
<p><strong>How we use personal data</strong></p>
<p>Generally, we use personal data only for purposes and in the manner agreed upon with the data subject. To the extent permitted or required by law, we use personal data to pursue our legitimate interests as a business, employer, and/or contracting party. For examples, we use personal data to:</p>
<ol>
<li>Enable users to register with and subscribe to MindNation;</li>
<li>Avail of the services of mental healthcare providers;</li>
<li>Provide users of MindNation with other services, including customer support;</li>
<li>Enhance customer experience and determine tailored content to meet user preferences and needs;</li>
<li>Communicate relevant services and advisories to users;</li>
<li>Abide by any safety, security, public service, or legal requirements and processes;</li>
<li>Efficiently and effectively manage our human resources;</li>
<li>Conduct research and studies relevant to our business; and</li>
<li>Intelligently evaluate our services and work;</li>
</ol>
<p>Apart from pursuing our legitimate interests, we use personal data as necessary in the performance of our contractual obligations to the data subject; for compliance with a legal obligation; to protect the data subject&rsquo;s vitally important interests, including life and health; and/or for the performance of tasks we carry out in the public interest (e.g. public order or public safety).</p>
<p>We understand that the DPA imposes stricter rules for the processing of sensitive personal information and privileged information, and we are fully committed to abiding by those rules. In particular, use of any privileged information shall be strictly ethical and shall be in accordance with the relevant laws, rules, and regulations.</p>
<p>Our use of personal data will never be excessive and will always be limited to the extent necessary to achieve the purpose of their collection or generation and/or to the extent permitted by law.</p>
<p><strong>How we share, disclose, or transfer personal data</strong></p>
<p>Generally, we share, disclose, and transfer personal data only for purposes and in the manner agreed upon with the data subject. We treat the personal data of MindNation&rsquo;s users confidentially. We aggregate and anonymize personal data in case any report, analysis, or recommendation is shared with the employer or company of any user of MindNation.</p>
<p>To the extent permitted or required by law, we share, disclose, and transfer personal data in the pursuit of our legitimate interests as a business, employer, and/or contracting party. For examples, we share, disclose, and transfer personal data to third parties in order to:</p>
<ol>
<li>Provide the mental healthcare provider assigned to a user of MindNation with the information necessary to perform mental healthcare services for the user;</li>
<li>Submit information to government agencies and private persons and entities for the provision of employment benefits mandated by law and our own policies; and</li>
<li>Conduct research and analysis with trusted partners.</li>
</ol>
<p>Apart from pursuing our legitimate interests, we may share, disclose, and transfer personal data to third parties as necessary in the performance of our contractual obligations to the data subject; for compliance with a legal obligation; to protect the data subject&rsquo;s vitally important interests, including life and health; and/or for the performance of tasks we carry out in the public interest (e.g. public order or public safety).</p>
<p>We understand that the DPA imposes stricter rules for the processing of sensitive personal information and privileged information, and we are fully committed to abiding by those rules. In particular, privileged information shall not be shared, disclosed, or transferred to third parties unless with the consent of the data subject and strictly in accordance with the relevant laws, rules, and regulations.</p>
<p>Sharing, disclosure, and transfer of personal data by us will never be excessive and will always be limited to the extent necessary to achieve the purpose of their collection or generation and/or to the extent permitted by law.</p>
<p><strong>How we store and retain personal data</strong></p>
<p>We ensure that personal data in our custody are protected against any accidental or unlawful destruction, alteration, disclosure, or processing. We implement appropriate security measures in storing personal data depending on their nature. Access to personal data is limited to authorized personnel. Unlawful processing and/or unauthorized access of stored personal data by our employees will be subjected to disciplinary and/or appropriate action.</p>
<p>We retain personal data only for as long as required by contractual and other obligations, laws, rules, regulations, and orders of competent authorities. When retention is not or no longer required, personal data may be retained by us for only a maximum of five years, except insofar as certain data are necessary for historical or statistical purposes. We will properly dispose of and/or destroy all physical and electronic copies of personal data in our custody upon determination that the same will not or no longer be retained.</p>
<p><strong>Updates</strong></p>
<p>We may, from time to time, make changes to this Policy. On such occasions, we will let data subjects know through our website. Any modification is effective immediately upon posting on our website, unless otherwise indicated.</p>
<p><strong>Miscellaneous</strong></p>
<p>Our other policies, rules, and regulations which are not inconsistent with this Policy will continue to apply. If any provision of this Policy is found to be unenforceable or invalid by any competent court, tribunal, agency, or office, the invalidity of such provision will not affect the validity of the other provisions, which shall remain in full force and effect.</p>
<p><strong>Contact us</strong></p>
<p>We recognize every individual&rsquo;s rights with respect to personal data as provided under the law. If a person wishes to exercise any data privacy rights, or has any concern or question regarding them, this Policy, or any matter involving us in relation to data privacy, you may contact us at hello@themindnation.com, at (+63) 915 721 7178, or at MindPH, Unit 4Q, 80 Maginhawa Street, Teachers Village East, Quezon City.</p>
<p><br /><br /></p>
</div>


@endsection