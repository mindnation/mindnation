@extends('layouts.frontend')
@section('title','For Members | '.config('app.name', 'Laravel'))
@section('body_class', 'for-members')

@section('content')

{{-- For Member SECTION 1 --}}
<div class="container-bg-shape">
    <div class="section section-1 section-struggles-are-valid container">
        <div class="row  justify-content-end ">

            <div class="col-text order-2 order-md-1 infront-of-bg-shape">
                <h1 class="h2 mt-9">Your struggles are valid and we're here to help.</h1>
                <div class="font-weight-light mt-4">
                    Whether you are feeling anxious, stressed at work, or just wanting to understand 
                    some of your thought patterns and habits, begin your path to wellness by sending an email to book a session.
                </div>
                <div class="btn btn-purple btn-lg mt-5 mt-md-4" data-toggle="modal"
                                data-target="#contactModal">{{ __('Send an email') }}
                </div>

            </div>

            <div class="col-12 col-md-6 order-1 order-md-2 mt-4 mt-md-0 pt-9 pb-6 pr-md-0 col-with-shape">
                <div class="wrap-bg-shape">
                    @svg('images/svg/shape-1.svg', 'bg-shape fill-orange','xMaxYMax slice')
                </div>
                <div class="vector-image infront-of-bg-shape">
                    <img src="{{ asset('images/frontend/sotcks-04.png') }}" class="image">
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END For Member SECTION 1 --}}


<div class="bg-light pt-5 pb-3 pb-md-5 section-howitworks">
    <div class="container">
        <div class="row">
            <div class="col-text-title">
                <h2 class="h2 mt-3 mt-md-4">How it works</h2>
                <div class="font-weight-light mt-3">
                    Consult with a psychologist in three easy steps.
                </div>
            </div>
        </div>

        <div class="row row-cards mt-10 mt-md-6 pb-0 pb-sm-4">
            <div class="col-cards">
                <div class="how-it-works-list row eq-height justify-content-center justify-content-md-start">
                    <div class="col-card">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body pt-5">
                                <div class="icon bg-blue"><span>1</span></div>
                                <h3 class="h6 text-blue my-3">Assess</h3>
                                <p class="font-weight-light font-size-md">
                                    Know where you currently stand with your mental health through our initial
                                    assessment.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-card">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body pt-5">
                                <div class="icon bg-orange"><span>2</span></div>
                                <h3 class="h6 text-orange my-3">Book</h3>
                                <p class="font-weight-light font-size-md">
                                    Schedule a consultation with your preferred care provider, date and time, and mode
                                    of communication.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-card">
                        <div class="card h-100 text-center shadow-lg">
                            <div class="card-body pt-5">
                                <div class="icon bg-teal"><span>3</span></div>
                                <h3 class="h6 text-teal my-3">Consult</h3>
                                <p class="font-weight-light font-size-md">
                                    Meet your care provider through a secure link given prior to your schedule.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5 mt-md-10 section-partnerwithemployer">
            <h2 class="col-text h2 text-center">
                We partner with your employers to provide you the best support you can get
            </h2>
            <div class="col-12 px-0">
                <div class="row row-cards eq-height">
                    <div class="col-cards">
                        <div class="solution-card card shadow-blue h-100">
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-2 pr-0">
                                        <div class="icon-wrap bg-blue-light cost-free"
                                            style="background-image:url('/images/icons/cost-free.svg')"></div>
                                    </div>
                                    <div class="card-col-text">
                                        <h3 class="h5 text-blue mt-1 mb-2">Cost-free</h3>
                                        <p class="font-size-sm mb-2">
                                            Our services are provided as a benefit for your company so you can worry
                                            less
                                            about
                                            expenses and focus on getting better.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-cards">
                        <div class="solution-card card shadow-orange h-100">
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-2 pr-0">
                                        <div class="icon-wrap bg-orange-light convenient"
                                            style="background-image:url('/images/icons/convenient.svg')"></div>
                                    </div>
                                    <div class="card-col-text">
                                        <h3 class="h5 text-orange mt-1 mb-2">Convenient</h3>
                                        <p class="font-size-sm mb-2 ">
                                            We have optimized our platform for your smart devices so you may access
                                            booking
                                            on-the-go and consult with our care providers whenever and however you want.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-cards">
                        <div class="solution-card card shadow-teal h-100">
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-2 pr-0">
                                        <div class="icon-wrap bg-teal-light competent"
                                            style="background-image:url('/images/icons/competent.svg')"></div>
                                    </div>
                                    <div class="card-col-text">
                                        <h3 class="h5 text-teal mt-1 mb-2">Competent</h3>
                                        <p class="font-size-sm mb-2">
                                            Our licensed and trusted care providers are experts in different mental
                                            health
                                            matters to offer you support in areas where you need it.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-cards">
                        <div class="solution-card card shadow-purple h-100">
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-2 pr-0">
                                        <div class="icon-wrap bg-purple-light confidential"
                                            style="background-image:url('/images/icons/confidential.svg')"></div>
                                    </div>
                                    <div class="card-col-text">
                                        <h3 class="h5 text-purple mt-1 mb-2">Confidential</h3>
                                        <p class="font-size-sm mb-2">
                                            No one - not even your supervisors - has to know about it. Meet our care
                                            providers
                                            discreetly without any needed notification to your managers.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container pb-4">
    <div class="mt-10 mb-5 section-getsupport">
        @include('frontend.partials.get-support')
    </div>
</div>


@endsection