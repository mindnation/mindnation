@extends('layouts.frontend')
@section('title','Mental Resilience Talk | '.config('app.name', 'Laravel'))
@section('body_class', 'simple-page videos')

@section('content')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-6FXV2CDG1S"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-6FXV2CDG1S');
</script>

<div class="container-bg-shape container-purple">
    <div class="section-recorded-videos container">
    
        <div class="row justify-content-end ">
            <div class="col-12 col-md-4 order-1 order-md-2 col-text infront-of-bg-shape order-2 order-md-1">
                <h2 class="section-title h2">
                    Mental Resilience
                </h2>
                <p class="sub-title font-weight-light">
                    The talk provides a broad overview of mental health and promoting mental wellbeing in the workplace with the tips to feel and become better.
                </p>
            </div>

            <div class="videos-container col-12 col-md-8 order-1 order-md-2 pb-8 pt-10 pt-md-10 pl-md-10 pr-md-0 col-with-shape">
                <video id="resilienceVideo" src="{{ asset('videos/mental-resilience-recorded-webinar.mp4') }}" type="video/mp4" controls controlsList="nodownload"/>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#resilienceVideo').bind('contextmenu', function() {
            return false;
        })
    })
</script>
@endsection