<!doctype html>
<html class="h-100" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('frontend.partials.head')

    <body class="@yield('body_class') h-100">

        <div class="collapse multi-collapse bg-light " id="loginCollapse">
            @include('frontend.partials.auth-content')
            <div class="login-footer" data-toggle="collapse" data-target=".multi-collapse"
                aria-expanded="false" aria-controls="loginCollapse mainCollapse">
                <div class="wrap-bg-shape">
                    @svg('images/svg/shape-1.svg', 'bg-shape fill-purple')
                </div>
                <div class="login-close-btn infront-of-bg-shape btn-teal">
                </div>
            </div>
        </div>
        <div class="collapse multi-collapse show " id="mainCollapse">

            @include('frontend.partials.header')

            <main class="main">
                @yield('content')
            </main>
            @include('frontend.partials.footer')
        </div>
        @include('frontend.partials.contact-modal')
        @include('partials.facebook-chat')
        @include('frontend.partials.scripts')
        
    </body>

</html>