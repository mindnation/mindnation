<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('psych.partials.head')

    <body class="@yield('body_class') layout-top-nav template-inner-sidebar">
        <div class="wrapper">
            @include('psych.partials.header')
            @if(isset($no_sidebar) && $no_sidebar) {{-- No Sidebar --}}
                <div class="content-wrapper bg-light" >
                    <div class="wrap-bg-shape">
                        @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-1')
                        @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-2')
                    </div>
                    @yield('content')
                </div>
             @else  {{-- Sidebar --}}
                <div class="content-wrapper flex-fill d-flex bg-light" style="flex-shrink: 0 !important">
                    <div class="row w-100" style="flex:1; margin:0;">
                        <div id="account_sidebar" class="col bg-white sidebar-col shadow-lg">
                            <div class="account-nav-toggler btn btn-yellow">
                                <i class="fas fa-chevron-right"></i>
                              </div>
                            @include('psych.partials.sidebar')
                        </div>
                        <div class="col content-col py-3">
                            @yield('content')
                        </div>
                    </div>
                </div>
            @endif
            <!-- /.content-wrapper -->
            {{--  @include('psych.partials.footer')  --}}
        </div>
        <!-- ./wrapper -->

        @include('partials.facebook-chat')
        @include('psych.partials.scripts')
    </body>

</html>