<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('member.partials.head')

    <body class="@yield('body_class') layout-top-nav template-inner-sidebar">
        <div class="wrapper">
            @include('member.partials.header')

            <div class="content-wrapper flex-fill d-flex bg-light" style="flex-shrink: 0 !important">
                <div class="row w-100" style="flex:1; margin:0;">
                    <div id="account_sidebar" class="col bg-white sidebar-col shadow-lg">
                        <div class="account-nav-toggler btn btn-yellow">
                            <i class="fas fa-chevron-right"></i>
                          </div>
                        @include('member.partials.sidebar')
                    </div>
                    <div class="col content-col py-5">
                        @yield('content')
                    </div>
                </div>
            </div>
            



            <!-- Content Wrapper. Contains page content -->

            <!-- /.content-wrapper -->
            @include('member.partials.footer')
        </div>
        <!-- ./wrapper -->
        @include('partials.facebook-chat')
        @include('member.partials.scripts')
    </body>

</html>