<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('hr.partials.head')

    <body class="@yield('body_class') layout-top-nav @yield('body_class')">
        <div class="wrapper">

            @include('hr.partials.header')

            
            
            @yield('sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper bg-light">

                <div class="wrap-bg-shape">
                    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-1')
                    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-2')
                </div>
                
                    @yield('content')
               
            </div>
            <!-- /.content-wrapper -->
            
                @include('hr.partials.footer')
            
        </div>
        <!-- ./wrapper -->
        @include('partials.facebook-chat')
        @include('hr.partials.scripts')
    </body>
</html>
