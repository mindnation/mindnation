<li class="nav-item dropdown user user-menu">
    <a href="#" class="btn btn-sm btn-yellow btn-login btn-min-width-sm mr-0 dropdown-toggle" data-toggle="dropdown">
        {{--  <img src="dist/img/user2-160x160.jpg" class="user-image img-circle elevation-2 alt="User Image">  --}}
        <span class="hidden-xs">Hi, {{ Auth::user()->first_name }}</span><i class="fas fa-angle-down ml-2"></i>


    </a>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right shadow mt-2">
        <!-- User image -->
        <div class="user-header bg-purple text-white text-center p-3">
            
            @if($avatar = auth()->user()->avatar)
                <div class="avatar my-3 mx-auto">
                    <img class="w-100 " src="{{asset('storage/'.$avatar->filename)}}"
                        data-src="{{asset('storage/'.$avatar->filename)}}">
                </div>
            @endif

            <div class="mb-2"> {{ Auth::user()->name() }}</div>
            <div class="font-size-sm">
                <div>{{ Auth::user()->roles()->first()->display_name }}</div>
                {{--  @if(Auth::user()->onboarding)
                    <div>since {{ Auth::user()->getRegistrationDate() }}</div>
                @endif  --}}
            </div>

        </div>
        
        
         @if(auth()->user()->isMember())
            <div class="mt-3">
                <ul class="list-unstyled"> 
                    
                @if(auth()->user()->agreement)
                
                    @if(auth()->user()->onboarding)
                        
                            
                            <li class="nav-item">
                                @php $route = 'member.dashboard'; @endphp
                                <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                    {{ __('My Dashboard') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                @php $route = 'member.booking'; @endphp
                                <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                    {{ __('Book a Consultation') }}
                                </a>
                            </li>

                            <li class="nav-item">
                                @php $route = 'member.account.appointments'; @endphp
                                <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                    <p>{{ __('My Consultations') }}</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                @php $route = 'member.account'; @endphp
                                <a href="{{ route($route) }}" class="nav-link {{areActiveRoutes([$route, 'member.account.details', 'member.account.settings', 'member.account.password'])}}">
                                    {{ __('My Account') }}
                                </a>
                            </li>
                    @else
                        <li class="nav-item">
                            @php $route = 'member.onboarding'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{areActiveRoutes([$route, 'member.onboarding.survey'])}}">
                                {{ __('Onboarding') }}
                            </a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        @php $route = 'member.dashboard'; @endphp
                        <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                            {{ __('My Dashboard') }}
                        </a>
                    </li>
                @endif
                </ul> 
            </div>
        @endif
        @if(Auth::user()->isPsych())
            <div class="mt-3">
                <ul class="list-unstyled"> 
                @if(Auth::user()->onboarding)
    
                        <li class="nav-item">
                            @php $route = 'psych.dashboard'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                {{ __('My Dashboard') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            @php $route = 'psych.account.profile'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveParentRoute($route)}}">
                                {{ __('My Account') }}
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            @php $route = 'psych.onboarding'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{areActiveRoutes([$route])}}">
                                {{ __('Onboarding') }}
                            </a>
                        </li>
                    @endif
                </ul> 
            </div>
        @endif

            

        @if(Auth::user()->isAdmin())
            <ul class="list-unstyled mt-3"> 
                <li class="nav-item">
                    @php $route = 'admin.dashboard'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        {{ __('Admin Dashboard') }}
                    </a>
                </li>
            </ul>
        @endif
        @if(auth()->user()->isHR())
            <div class="mt-3">
                <ul class="list-unstyled">        
                    <li class="nav-item">
                        @php $route = 'hr.dashboard'; @endphp
                        <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                            {{ __('My Dashboard') }}
                        </a>
                    </li>
                </ul>
            </div>
            @endif

        <!-- Menu Footer-->
        <div class="user-footer text-center p-3">


            <a class="btn btn-sm btn-yellow btn-login btn-min-width-sm" href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>



        </div>
    </div>
</li>