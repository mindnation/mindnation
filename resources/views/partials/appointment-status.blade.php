@switch($appointment->status)
    @case('confirmed')
        <span class="badge-pill badge-teal text-teal-light font-size-md">{{ $appointment->getStatus() }}</span>
        @break
    @case('canceled')
        <span class="badge-pill badge-orange text-orange-light font-size-md">{{ $appointment->getStatus() }}</span>
        @break
    @case('no-show')
        <span class="badge-pill badge-purple text-purple-light font-size-md">{{ $appointment->getStatus() }}</span>
        @break
    @case('completed')
        <span class="badge-pill badge-blue text-blue-light font-size-md">{{ $appointment->getStatus() }}</span>
        @break
    @default
    <span class="badge-pill badge-gray-light font-size-md">{{ $appointment->getStatus() }}</span>
        
@endswitch