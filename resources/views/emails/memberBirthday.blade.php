@component('mail::message')
# Hi {{ $notifiable->first_name }}!

It’s another year around the sun for you and we’re grateful to be a part of your journey! 🎂🥳

I highly recommend Birthday Sessions so you can check-in, reflect and work towards a greater, more wonderful year for you. ⏰

🎁 I’ve got a special treat, too! Once you’ve booked your Birthday Session, you get a FREE PERSONALIZED BIRTHDAY CARE PACKAGE from us.

Your best days are ahead. Have a wonderful birthday, {{ $notifiable->first_name }}!

@include('emails.partials.signature')
@endcomponent
