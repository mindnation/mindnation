@component('mail::message')
# Hi {{ $appointment->member->first_name }}!

@if($isUserCanceling)
This is to confirm your cancelation of the following session: 
<strong>{{ $appointment->getFullDateString() }}</strong>
with <strong>{{ $appointment->psych->full_name() }}</strong>.

@else

We regret to inform you that the following session has been canceled
<strong>{{ $appointment->getFullDateString() }}</strong>
with <strong>{{ $appointment->psych->full_name() }}</strong>.
@endif

@include('emails.partials.signature')
@endcomponent