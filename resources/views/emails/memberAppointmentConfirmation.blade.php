@component('mail::message')
# Hey there, {{ $appointment->member->first_name }}!

We’re so excited you’re taking the first step towards a better YOU. 🎉

This is to confirm your booking session this <strong>{{ $appointment->getFullDateString() }}</strong>{!! $appointment->getMethod()?' via <strong>'.$appointment->getMethod().'</strong>':'' !!}. Your booking is scheduled with <strong>{{ $appointment->psych->full_name() }}</strong>.

🔔 <strong>Here are some reminders for your session:</strong>
<ul>
<li>Be early: Make sure you are ready at least 10 minutes before call time.</li>

@if(in_array($appointment->status,['online_audio_call','online_video_call']))
<li>We value your privacy: Please ensure that you are also in a quiet, private place.</li> 
<li>We recommend that you’re also connected to fast, strong WiFi to maintain call quality</li>
@endif

<li>Be open: View your session as a collaboration with your therapist. We’re here to help you get through your most pressing concerns so you can be a better you!</li>
<li>Take time to also listen: It’s a two-way session, always.</li>
<li>Always seek action: Set goals and milestones with your therapist for your next session.</li>
</ul>

Please be informed that confirmed bookings that are cancelled within 12 hours or no-show will be rendered completed and it won’t be available to use by someone else. Should there be any sudden changes or concerns with your booking to cancel or edit your date/time session, please feel free to let me know by replying to this email or sending us a message on Messenger as soon as possible.

We are putting the link to a 2-minute survey that can help before your first session to start: <a href="https://bit.ly/MNOnboarding">https://bit.ly/MNOnboarding</a> Don’t forget to do this! :) Please do let us know if you have any questions or concerns. 


Thank you! Have a great session.
@include('emails.partials.signature')
@endcomponent