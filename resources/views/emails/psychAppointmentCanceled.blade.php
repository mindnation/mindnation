@component('mail::message')
# Hi {{ $appointment->psych->first_name }}!

@if($isUserCanceling)
This is to confirm your cancelation of the following session: 
<strong>{{ $appointment->getFullDateString() }}</strong>
with <strong>{{ $appointment->member->full_name() }}</strong>.

@else

Your patient has canceled the following session:
<strong>{{ $appointment->getFullDateString() }}</strong>
with <strong>{{ $appointment->member->full_name() }}</strong>.
@endif

@include('emails.partials.signature')
@endcomponent