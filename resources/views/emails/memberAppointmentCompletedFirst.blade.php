@component('mail::message')
# Hi {{ $appointment->member->first_name }}!

How was your session today? 🤗 I hope that it got to take a little bit of weight off your shoulders. 

I’m very proud of you for taking the necessary steps into self-improvement. It’s not always one-and-done, overnight magic. ✨ It’s a continuous process and we should be constantly in-tune with ourselves even when everything is going well. 

When can I schedule your next session? ⏰

PS. Just like you, we’re also looking to improve so we can help more people like you. We would also appreciate feedback regarding your session, Would you mind taking this quick feedback form? It won't take more than five minutes long, we promise! <a href="https://bit.ly/MNClientFeedback">https://bit.ly/MNClientFeedback</a>

@include('emails.partials.signature')
@endcomponent
