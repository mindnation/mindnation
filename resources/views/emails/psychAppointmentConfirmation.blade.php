@component('mail::message')

Hi {{ $appointment->psych->full_name() }}!

You have a new consultation : <strong>{{ $appointment->getFullDateString() }}</strong>{!! $appointment->getMethod()?' via <strong>'.$appointment->getMethod().'</strong>':'' !!}. 
Your booking is scheduled with <strong>{{ $appointment->member->full_name() }}</strong>.

Thank you! Have a great session.
@include('emails.partials.signature')
@endcomponent