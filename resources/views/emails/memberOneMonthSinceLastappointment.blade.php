@component('mail::message')
# Hi {{ $notifiable->first_name }}!

It’s been a month since we last talked. We miss you! 💔

Continuously booking sessions is key to the amazing, transformative process toward living a conscious life and a better YOU. ✨ Shall we pencil you in for a session soon? 

PS. If there’s anything that you feel I need to improve on or is stopping you from continuing our sessions, please let me know! 🤔 <a href="https://bit.ly/MNClientFeedback">https://bit.ly/MNClientFeedback</a>

@include('emails.partials.signature')
@endcomponent
