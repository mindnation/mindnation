@component('mail::message')
# Hi {{ $appointment->member->first_name }}!

How was your session today? Thank you for continuously trusting MindNation to help you put your mind and soul at ease! 💆🏽‍♀️

Can we schedule you for your next session? ⏰

PS. I took the chance to collect some new read recommendations for you, too: <a href="https://blog.themindnation.com/">https://blog.themindnation.com/</a>

<a href="https://bit.ly/MNClientFeedback">https://bit.ly/MNClientFeedback</a> Session Feedback Link

@include('emails.partials.signature')
@endcomponent
