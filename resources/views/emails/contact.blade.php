@component('mail::message')
Contact Enquiry:
- First Name: {{ $enquiry->first_name }}
- Last Name: {{ $enquiry->last_name }}
- Email: {{ $enquiry->email }}
- Company Name: {{ $enquiry->company_name }}
- Role: {{ $enquiry->role }}
- Contact Number: {{ $enquiry->contact_number }}
- Number of Employees: {{ $enquiry->number_of_employees }}
- Message: {{ $enquiry->message }}

@endcomponent
