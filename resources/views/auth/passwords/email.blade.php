@extends('layouts.frontend')
@section('body_class', 'simple-page auth-page')

@php $page_title = 'Reset Password' @endphp

@section('title', $page_title)
@section('content')
<div class="wrap-bg-shape">
    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-1')
    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-2')
</div>
<div class="container">
    <div class="row justify-content-center  pb-8">
        <div class="col-md-8">
            <div class="card border-multicolor-top shadow-lg text-center mt-6">
                <div class="card-body pt-5 pb-7 px-8">
               
                    <div class="h4 mb-5 text-center">{{ __('Reset Password') }}</div>
                    @if (session('status'))
                    <div class="callout callout-success border-success mb-3  text-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="POST" class="reset-form" action="{{ route('password.email') }}">
                        @csrf

                        
                                <div class="form-group mb-4">
                                    {{ Form::email('email', null ,['class' => ' form-control form-control-lg', 'placeholder' => 'Email']) }}
                                    @error('email')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                                @enderror
                                </div>

                                
                           
                        

                        <div class="mt-5">
                            <button type="submit" class="btn btn-teal btn-lg btn-min-width-lg">
                                {{ __('Send Password Reset Link') }}
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.reset-form').submit(function(){
        $(this).find('[type="submit"]').prop( "disabled", true );
    });

</script>
@endpush