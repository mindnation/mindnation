@extends('layouts.frontend')
@section('body_class', 'simple-page auth-page')
@php $page_title = 'Confirm Password' @endphp

@section('title', $page_title)
@section('content')
<div class="wrap-bg-shape">
    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-1')
    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-2')
</div>
<div class="container">
    
    <div class="row justify-content-center  pb-8">
        <div class="col-md-8">
            <div class="card border-multicolor-top shadow-lg text-center mt-6">
                <div class="card-body pt-5 pb-4">
                    <div class="h4 mb-5 text-center">{{ __('Confirm Password') }}</div>
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-5">
                                <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="mt-5">
                            
                                <button type="submit" class="btn btn-teal btn-lg btn-min-width-lg">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                <div>
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                                @endif
                          
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
