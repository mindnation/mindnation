@extends('layouts.frontend')
@section('body_class', 'simple-page auth-page')
@php $page_title = 'Reset Password' @endphp

@section('title', $page_title)
@section('content')
<div class="wrap-bg-shape">
    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-1')
    @svg('images/svg/shape-2.svg', 'bg-shape app-bg-shape-2')
</div>
<div class="container">
    
    <div class="row justify-content-center  pb-8">
        <div class="col-md-8">
            <div class="card border-multicolor-top shadow-lg text-center mt-6">
                <div class="card-body pt-5 pb-4">

                    <div class="h4 mb-5 text-center">{{ __('Reset Password') }}</div>
                    
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-5">
                                <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-5">
                                <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-5 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-5">
                                <input id="password-confirm" type="password" class="form-control form-control-lg" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        
                            <div class="mt-5">
                                <button type="submit" class="btn btn-teal btn-lg btn-min-width-lg ">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
