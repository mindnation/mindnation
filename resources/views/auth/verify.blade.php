@extends('layouts.member')
@section('title','Email Verification | '.config('app.name', 'Laravel'))
@section('body_class', 'simple-page ')
@php $page_title = 'Verify Your Email Address' @endphp

@section('title', $page_title)

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 py-10">
            <div class="card shadow-lg pt-5 pb-4 border-multicolor-top">
                <div class="card-header h5 text-center border-0 mb-4">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body text-center ">
                    @if (session('resent'))
                        <div class="callout callout-success border-success text-left text-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
