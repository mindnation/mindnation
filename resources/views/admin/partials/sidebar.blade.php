<!-- Main Sidebar Container -->
<aside class="sidebar-light-primary border-right ">

<div class="nav-header py-3"></div>
    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="my-3 overflow-hidden">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    @php $route = 'admin.dashboard'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    @php $route = 'admin.appointments.index'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">

                        <i class="nav-icon fas fa-notes-medical"></i>
                        <p>Consultations</p>
                    </a>
                </li>
                <li class="nav-item">

                    @php $route = 'admin.companies.index'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-building"></i>
                        <p>Companies</p>
                    </a>
                </li>
                <li class="nav-item">
                    @php $route = 'admin.psych.index'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">

                        <i class="nav-icon fas fa-user-md"></i>
                        <p>Psychologists</p>
                    </a>
                </li>
                <li class="nav-item">
                    @php $route = 'admin.humanresources.registered'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">

                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>Human Resources</p>
                    </a>
                </li>
                <li class="nav-item">
                    @php $route = 'admin.members.registered'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">

                        <i class="nav-icon fas fa-users"></i>
                        <p>Registered Members</p>
                    </a>
                </li>
                <li class="nav-item">
                    @php $route = 'admin.members.allowed'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">

                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>Allowed Members</p>
                    </a>
                </li>
                <li class="nav-item">

                    @php $route = 'admin.admin-users.index'; @endphp
                    <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>Admin Users</p>
                    </a>
                </li>

                <li class="nav-item has-treeview {{areActiveRoutes(['admin.account.details', 'admin.account.password'], "menu-open")}}">
                    @php $route = 'psych.account'; @endphp
                    <a href="#" class="nav-link {{areActiveRoutes(['admin.account.details', 'admin.account.password'])}}">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>{{ __('My Account') }}</p>
                        <i class="fas fa-angle-left right"></i>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            @php $route = 'admin.account.details'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                <i class="nav-icon fas fa-user"></i>
                                <p>{{ __('Profile') }}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            @php $route = 'admin.account.password'; @endphp
                            <a href="{{ route($route) }}" class="nav-link {{isActiveRoute($route)}}">
                                <i class="nav-icon fas fa-key"></i>
                                <p>{{ __('Change Password') }}</p>
                            </a>
                        </li>

                    </ul>
                </li>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>