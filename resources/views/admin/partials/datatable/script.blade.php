<script>
    var colOrder = 0,
        sortOrder = "desc";
    @php $index = 0; @endphp
    @foreach($datatable_columns as $col )
        @if(isset($col['defaultOrder']) && isset($col['sortOrder']))
            colOrder = {{ $index }};
            sortOrder = "{!! $col['sortOrder'] !!}";
        @endif
        @php $index++; @endphp
    @endforeach

    $(document).ready(function() {
        $('.dataTable').DataTable({
            order: [[ colOrder, sortOrder ]],
            processing: true,
            serverSide: true,
            responsive: true,
            columnDefs: [
                { responsivePriority: 1, targets: 0 },
                // { responsivePriority: 2, targets: -1 }
            ],
            ajax: '{!! $datatable_data_route !!}',
            columns: {!! json_encode($datatable_columns) !!}
        });
    });
</script>