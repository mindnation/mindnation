{{ $user->name() }}
<div class="float-right ">

    @if($user->blocked_at)
    <i class="text-danger fas fa-ban mr-1" data-toggle="tooltip" data-placement="top"
        title="Suspended on: {{ $user->blocked_at }}"></i>
    @endif
    
    @if($user->registration_date)
        @if($user->isMember())
            <i class="text-success far fa-check-circle mr-1" data-toggle="tooltip" data-placement="top"
            title="Registered on: {{ $user->registration_date }}"></i>

        @endif

        @if($user->isAdmin())
            @if($user->last_login)
                <i class="text-success far fa-check-circle mr-1" data-toggle="tooltip" data-placement="top"
                title="Last Login: {{ $user->last_login }}"></i>
            @else
                <i class="text-warning far fa-hourglass mr-1" data-toggle="tooltip" data-placement="top"
        title="Not Logged In Yet"></i>
            @endif
        @else
            @if($user->onboarding)
                <i class="text-success fa fa-plane mr-1" data-toggle="tooltip" data-placement="top"
                title="Onboarded on: {{ $user->onboarding }}"></i>
            @else
                <i class="text-yellow fa fa fa-plane mr-1" data-toggle="tooltip" data-placement="top"
            title="Not onboarded"></i>
            @endif
        @endif

    @else
    <i class="text-warning far fa-hourglass mr-1" data-toggle="tooltip" data-placement="top"
        title="Not Registered"></i>

    @endif

</div>