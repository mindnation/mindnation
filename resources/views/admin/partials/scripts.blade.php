
<script src="{{ asset ("/js/app.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/admin.js") }}" type="text/javascript"></script>

<script src="{{ asset ("/bower_components/typeahead.js/dist/typeahead.bundle.min.js") }}"></script>

@stack('scripts')