<head>
    <meta charset="UTF-8">
    <title>@yield('title') | Admin Dashboard | {{ config('app.name', 'Laravel') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    {{--  <link href="{{ asset('css/admin.css') }}" rel="stylesheet">  --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    

</head>