<a href="#" class="datatable-edit-button float-right" data-target="#EditFormModal" data-action="edit" data-modal-title="Edit Psychologist (ID:{{ $id}})" data-entry-id="{{$id}}" data-get-data-url="{!!route('admin.psych.get_data')!!}">
    <i class="text-info far fa-edit" data-toggle="tooltip" data-placement="top"
    title="Edit"></i> 
</a>



{{--  Delete  --}}
<div class="float-right mr-2" data-toggle="modal" data-target="#deleteModal-{{ $id }}">
    <i class="text-danger far fa-trash-alt" data-toggle="tooltip" data-placement="top"
    title="Delete Permanently"></i>
</div>

<div id="deleteModal-{{ $id }}" class="modal admin-delete-modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(array('route' => 'admin.psych.delete', 'class' => 'admin-form')) }}
            <div class="modal-header">
                <h5 class="modal-title">Delete Permanently User (ID: {{ $id }})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-response"></div>
                <p>Are you sure to delete permanently the following entry?</p>
        
                <div>
                    <label>Email:</label> {{$email}}
                </div>
                
                <div class="text-center mt-5">
                    {{ Form::checkbox('permanently', true, null, ['id'=> 'permanently-'.$id, 'class' => 'checkbox-square' ]) }}
                    {{ Form::label('permanently-'.$id, 'Delete Permanently') }}
                </div>
                <div class="text-center"><small>Permanent deletion will fail if other entries depend on this one.</small></div>

                {{ Form::hidden('id', $id) }}
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                {!! Form::submit("Delete", [ 'class' => 'btn btn-danger']) !!}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

{{--  Suspend  --}}

@if(!$blocked_at)
<div class="float-right mr-2" data-toggle="modal" data-target="#blockModal-{{ $id }}">
    <i class="text-danger fas fa-lock" data-toggle="tooltip" data-placement="top"
    title="Suspend"></i>
</div>

<div id="blockModal-{{ $id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(array('route' => 'admin.psych.block', 'class' => 'admin-form')) }}
            <div class="modal-header">
                <h5 class="modal-title">Suspend User (ID: {{ $id }})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-response"></div>
                <p>Are you sure to block the following entry?</p>
        
                <div>
                    <label>Email:</label> {{$email}}
                </div>
                

                {{ Form::hidden('id', $id) }}
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                {!! Form::submit("Suspend", [ 'class' => 'btn btn-danger']) !!}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@else
<div class="float-right mr-2" data-toggle="modal" data-target="#blockModal-{{ $id }}">
    <i class="text-success fas fa-lock-open" data-toggle="tooltip" data-placement="top"
    title="Activate"></i>
</div>

<div id="blockModal-{{ $id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(array('route' => 'admin.psych.unblock', 'class' => 'admin-form')) }}
            <div class="modal-header">
                <h5 class="modal-title">Activate User (ID: {{ $id }})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-response"></div>
                <p>Are you sure to unblock the following entry?</p>
        
                <div>
                    <label>Email:</label> {{$email}}
                </div>
                

                {{ Form::hidden('id', $id) }}
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                {!! Form::submit("Activate", [ 'class' => 'btn btn-success']) !!}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


@endif









