<div class="card">
    <div class="card-header">
        <h3 class="card-title">Assigned Companies</h3>
        <div class="card-tools">
            <input id='search_company_input' class="typeahead form-control form-control-sm" type="text"
                placeholder="Add Company">
        </div>
    </div>
    <div class="card-body">

        <div class="assigned_companies">
            <ul class="todo-list" id="assigned_companies_list">
                
            </ul>
        </div>
    </div>
</div>


@push('scripts')
<script type="text/javascript">
    $(document).ready(function($) {
        // Set the Options for "Bloodhound" suggestion engine
        var engine = new Bloodhound({
            remote: {
                url: "{{ route('admin.companies.autocomplete') }}?q=%QUERY%",
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });
    
        $("#search_company_input").typeahead({
            hint: true,
            highlight: true,
            minLength: 2,
            classNames: {  
            },
        }, {
           
            name: 'companies',
            source: engine.ttAdapter(),
    
            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
            display: '',
            limit: 5,
    
            // the key from the array we want to display (name,id,email,etc...)
            templates: {
                empty: [
                    '<div class="list-group search-results-dropdown "><div class="list-group-item">Nothing found.</div></div>'
                ],
                suggestion: function (data) {
                    return '<div class="list-group-item">'+data.name + '</div>'
                }
            }
        })
        .bind('typeahead:select', function(ev, suggestion) {
            $(this).typeahead('val', '');
            var assigned_companies_list = $('#assigned_companies_list');
            
            if (!assigned_companies_list.find('li[data-company-id="'+suggestion.id+'"]').length){
                $.ajax({
                    type: "get",
                    url:"{{route('admin.psych.assignedcompanies')}}",
                    dataType:'json',
                    data:{company_id:suggestion.id},
                    success: function (data) {
                        // console.log( data);
                        // console.log( assigned_companies_list);
                        if(data.companies.length){
                            $.each( data.companies, function( key, value ) {
                                assigned_companies_list.append(value);
                            });
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }

            

            //console.log( suggestion);
        });

        $(document).on('click', '.remove-assigned-company', function () {
            $(this).closest('li.assigned_companies_item').fadeOut( "fast", function() {
                $(this).remove();
            });

        });
    });



</script>

@endpush