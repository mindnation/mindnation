<li class="assigned_companies_item" data-company-id="{{ $company->id }}">

    <span class="text">{{ $company->name }}</span>
    <a href="#" class=" btn btn-danger btn-xs remove-assigned-company float-right">
        <i class="fa fa-times"></i>
    </a>
    <input type="hidden" name="assigned_companies[]" value="{{ $company->id }}">
</li>