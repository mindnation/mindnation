@extends('layouts.admin')

@php $page_title = "My Details | Account"; @endphp

@section('title', $page_title)

@section('content')


<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        {{ Form::open(array('route' => 'admin.account.store_details')) }}
        <div class="card m-0 shadow-lg">
            <div class="card-body">
                <div class="h3 mb-3">My Details</div>

                @if ($message = Session::get('success'))
                <div class="callout callout-success text-success border-success py-2">
                    <div>{{ $message }}</div>
                </div>
                @endif

                {{--  @if ($errors->any())
                <div class="callout callout-danger text-danger border-danger  py-2">
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif  --}}

                <div class="h5 border-bottom py-3 mt-3 mb-4">Personal Information</div>
                <div class="row">
                    <div class="col-12 col-md-5 mb-4 mb-md-0 font-size-sm">
                        Your personal information will be used in verifying your identity as an admin of the app.
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'first_name'; @endphp
                                    {{ Form::label($field, 'First Name') }}
                                    {{ Form::text($field, auth()->user()->first_name ,['class' => 'form-control '.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    @php $field = 'last_name'; @endphp
                                    {{ Form::label($field, 'Last Name') }}
                                    {{ Form::text($field, auth()->user()->last_name ,['class' => 'form-control'.( $errors->has($field) ? ' is-invalid' : '' )]) }}
                                    @error($field)
                                        <div class="invalid-feedback text-left">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>

                

            </div>

            <div class="card-footer bg-white">
                <a href="{{route('member.account.details')}}" class="btn btn-default">Cancel</a>
                {!! Form::submit('Save', [ 'class' => 'btn btn-info float-right']) !!}
            </div>
        </div>
        {{ Form::close() }}
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection


@push('scripts')

@endpush