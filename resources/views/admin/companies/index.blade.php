@extends('layouts.admin')

@php $page_title = $entry_plural ; @endphp

@section('title', $page_title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-12">
                <h1 class="m-0 text-dark">{{ $entry_plural }}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->




<!-- Main content -->
<div class="content">
    <div class="container-fluid px-2 px-md-3">

        <div class="card">
            <div class="card-body px-3">
                <div class="mb-3">
                    <button type="button" class="btn btn-sm btn-info datatable-edit-button" data-toggle="modal"
                        data-target="#EditFormModal" data-action="add" data-modal-title="Add New {{ $entry_singular }}">
                        Add New {{ $entry_singular }}
                    </button>
                </div>

                <div class="response-div"></div>
                <div class="table-responsive-wrapper">
                    <table class="w-100 table  table-bordered table-striped dataTable table-hover dt-responsive ">
                    </table>
                </div>

            </div>
        </div>
        
        @include('admin.companies.modal-form')
        



    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

@endsection


@push('scripts')
    @include('admin.partials.datatable.script')
@endpush