<div class="modal fade admin-edit-form" id="EditFormModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(array('route' => 'admin.companies.store', 'class' => 'admin-form')) }}
            <div class="overlay  d-flex justify-content-center align-items-center">
                <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
            <div class="modal-header">
            <h5 class="modal-title" id="EditFormModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-response"></div>
             
                    

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null ,['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('email_domain', 'Allowed Email Domain') }}
                            {{ Form::text('email_domain', null ,['class' => 'form-control']) }}
                            <small>This will allow all emails from this domain to register as Member.<br>
                            Ex: To allow all emails from @themindnation.com enter themindnation.com</small>
                        </div>
                        <div class="form-group">
                            {{ Form::label('description', 'Description') }}
                            {{ Form::textarea('description', null ,['class' => 'form-control']) }}
                        </div>
                        
                        {{ Form::hidden('id', null, array('id' => 'id')) }}

                    
                

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                {!! Form::submit('Save', ['id'=> 'submit_button', 'class' => 'btn btn-info float-right']) !!}

            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>