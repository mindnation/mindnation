
<div class="h5">Parsing Results</div>
<div><label>Total rows:</label> {{ $total_rows }}</div>
<div><label>Importing:</label> {{ $count_imports }} emails</div>
<div><label>Company:</label> {{ $company->name }} (ID:{{ $company->id }})</div>
<div>
    <label class="mb-0">Invalid Rows :</label> {{ $count_invalid = count($invalid_rows) }}
    <div class="mb-2"><small>(invalid email or email already in the database)</small></div>
    {{--  @if($count_invalid)
    <div>
        <table class="table table-sm table-bordered">
            <tr>
                <th>Row</th>
                <th>Value</th>
            </tr>
            @foreach($invalid_rows as $key=>$value)
                <tr>
                    <td>{{ $key }}</td>
                    <td>{{ $value }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    @endif  --}}
</div>

<div>
    <label class="mb-0">Duplicate emails:</label> {{ $count_duplicates = count($duplicates) }}
    <div class="mb-2"><small>(already present in the list)</small></div>
    {{--  @if($count_duplicates)
    <div>
        <table class="table table-sm table-bordered">
            <tr>
                <th>Row</th>
                <th>Value</th>
            </tr>
            @foreach($duplicates as $key=>$value)
                <tr>
                    <td>{{ $key }}</td>
                    <td>{{ $value }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    @endif  --}}
</div>
<div class="text-center mt-5">
    {{ Form::open(array('route' => 'admin.members.import.process', 'files' => true, 'class'=>'csv_import_form')) }}
    {{ Form::hidden('id', $csv_import_id) }}
    {!! Form::submit('Confirm & Import', [ 'class' => 'btn btn-lg btn-info']) !!}
    {{ Form::close() }}
    
</div>