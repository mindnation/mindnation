
<!-- Button trigger modal -->
<button type="button" class="btn btn-sm btn-purple float-md-right mb-3 mb-md-0" data-toggle="modal" data-target="#csvImportModal">
    Import CSV
</button>
  
  <!-- Modal -->
  <div class="modal fade" id="csvImportModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="csvImportModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="loading overlay justify-content-center align-items-center" style="opacity:1; display:none;">
            <div class="text-center mt-10">
                <div><i class="fas fa-2x fa-sync fa-spin"></i></div>
            
               <div>Loading...</div>
               <div>This might take a while. Do not close this page.</div>
            </div>
            
        </div>
        
        <div class="modal-header">
          <h5 class="modal-title" id="csvImportModalLabel">CSV Import</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body px-4 pt-4 form-container">
            {{ Form::open(array('route' => 'admin.members.import.parse', 'files' => true, 'class'=>'csv_import_form')) }}
            <div class="form-response text-center"></div>
            <div class="text-center text-danger mb-5"><small>For large import, 10k csv rows/users, contact DEV team (temporary)</small></div>
            
            <div class="form-group">
                {{ Form::label('company', 'Company') }}
                {{ Form::select('company',$company_select_data, null, ['placeholder' => 'Select Company...', 'class' => 'form-control']) }}
            </div>
            <div class="form-group mb-4">
                <label for="csv_file" class="mr-3">CSV File:</label>
                {!! Form::file('csv_file',['id'=>'csv_file', 'class'=>'form-control-file']) !!}
            </div>
            <div class="mb-4">
                <div class="h6">Header:</div>
                <div class="mb-2 form-group form-check text-left">
                    {{ Form::checkbox('has_header', '1', false ,['id' => 'has_header', 'class' => 'form-check-input mr-2']) }}
                    <label class="form-check-label" for="has_header">File contains header row? (ignore first row)</label>
                </div>
            </div>
            <div class="mb-5">
                <div class="h6">Delimiter:</div>
                
                <div class="mb-2 form-group form-check text-left">
                    
                    {{ Form::radio('delimiter', 'semicolon', true ,[ 'id' =>'delimiter_semicolon', 'class' => 'form-check-input mr-2']) }}
                    <label class="form-check-label" for="delimiter_semicolon">Semicolon (;)</label>
                    
                </div>
                <div class="mb-2 form-group form-check text-left">
                    
                    {{ Form::radio('delimiter', 'commma', false ,[ 'id' =>'delimiter_comma', 'class' => 'form-check-input mr-2']) }}
                    <label class="form-check-label" for="delimiter_comma">Comma (,)</label>

                </div>
            </div>
            <div class="text-center">
                {!! Form::submit('Parse', [ 'class' => 'btn btn-info']) !!}
            </div>
        
            {{ Form::close() }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          
        </div>
        
      </div>
    </div>
  </div>

  @push('scripts')
  <script>
    $('document').ready(function(){

        $(document).on('submit', '.csv_import_form', function(e){
            e.preventDefault();
            var form = $(this);
            var url = $(this).attr('action');
            var method = $(this).attr('method');
            var response_div = $(this).find('.form-response');
            var form_data = $(this).serialize();
            var formData = new FormData(this);
            var parent_modal = $(this).parents('.modal');
            var form_container = parent_modal.find('.form-container');
            var loading = parent_modal.find('.loading');

    
            form.find('.invalid-feedback').remove();
            form.find('.is-invalid').removeClass('is-invalid');
            response_div.hide();
            loading.show();


            $.ajax({
                url: url,
                method: method,
                data: formData,
                dataType: "json",
                success: function (data) {
                    
                    if (typeof (data.success) != "undefined" && data.success) {
                        // response_div.text(data.success).show();
                        
                        if(data.html){
                            form_container.html(data.html);
                        }
                        if (data.redirect) {
                            setTimeout(function () {
                                window.location.href = data.redirect;
                            }, 3000);
                            
                        }
                    }
                    loading.hide();
                },
                error: function (xhr, status, error) {
                    var errors = xhr.responseJSON;
                    if (typeof (errors.error) != "undefined" && errors.error !== null) {
                        response_div.html('<div class="text-danger">' + errors.error + '</div>').show();
                    }
                    $.each(errors.errors, function (key, value) {
                        var field = form.find('[name="' + key + '"]');
                        field.addClass('is-invalid');
                        field.parent('.form-group').append('<div class="invalid-feedback text-left">' + value + '</div>');
                    });
                    if (typeof (errors.exception) != "undefined" && errors.exception !== null) {
                        response_div.html('<div class="text-danger">Something went wrong.<br>Reload the page and try again.</div>').show();
                    }
                    loading.hide();
                },
                cache: false,
                contentType: false,
                processData: false,

            });
        });


    });
  </script>
    
  @endpush